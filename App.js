/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, { useState ,useEffect } from 'react';
import AuthNavigation from './src/Navigators/AuthNavigation'; 
import Splash from './src/Splash';

import { createStore, combineReducers, applyMiddleware } from 'redux'
import thunk from 'redux-thunk';
import { Provider } from 'react-redux';
import common_reducer from './src/redux/reducers/common_reducer'; 
import user_reducer from './src/redux/reducers/user_reducer';
import { NavigationContainer, DefaultTheme } from '@react-navigation/native'
import cartReducer from './src/redux/reducers/cart_reducer';



console.disableYellowBox = true;

const reducer = combineReducers({
  common : common_reducer,
  user : user_reducer,
  cart:cartReducer
});

const store =  createStore(reducer,applyMiddleware(thunk))

const App: () => React$Node = () => {


  return (
    <Provider store={store}>
     <NavigationContainer>
        <AuthNavigation/>
      </NavigationContainer>
    </Provider>
  );
};


/**
const App = (props) => {

  const [isLoading ,setLoading ] = useState(true);

  useEffect(() => {
    const splashInterval = setTimeout(()=> {

      setLoading(false);

  },3000);
    return () => {
      clearTimeout(splashInterval);
    }
  }, [])

  if(isLoading){
    return <Splash />
  }

  return (
    <>

    <Provider store={store}>
      <AuthNavigation /> 
    </Provider>
    </>
  )};

   */

export default App;
