import React,{useState} from 'react';
import { Text, Image, View ,StyleSheet,TouchableOpacity, SafeAreaView, Alert} from 'react-native';
import CustomTextInput from '../CustomUI/CustomTextInput';
import CustomButton from '../CustomUI/CustomButton';
import {COLORS} from '../Utils/Utils';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import AuthContext from '../AuthContext/AuthContext';
import AsyncStorage from '@react-native-community/async-storage';
import AddPicture from '../CustomUI/AddPicture';
import * as userActions from  '../redux/actions/user_action'
import ImagePicker from 'react-native-image-picker';
import {useDispatch,useSelector} from 'react-redux';
import ProgressBar from '../Utils/ProgressBar';

import {showMessage} from '../Utils/ShowMessage';
import { TextInput } from 'react-native';
import countryList from '../Utils/countries';
import { Dropdown } from 'react-native-material-dropdown';


const Registration =(props) => {

   // const { signUp } = React.useContext(AuthContext);

    const dispatch = useDispatch();
    const common = useSelector(state => state.common);

    const options = {
        title: 'Select Avatar',
        //customButtons: [{ name: 'fb', title: 'Choose Photo from Facebook' }],
        storageOptions: {
          skipBackup: true,
          path: 'images',
        },
      };

 
    const [email,setEmail] = useState("");
    const [password ,setPassword ] = useState("");
    const [phone ,setPhone] = useState("");
    const [username ,setUsername ] = useState("");
    const [address, setAddress] = useState("");
    const [city, setCity] = useState("");
    const [countryCode, setCountryCode] = useState("");
    const [confirm_password , setConfirmPassword] = useState("");

    const [errorTrue ,setErrorTrue] = useState(false);
    const [errorUserNameMsg ,setErrorUserNameMsg ] = useState("");
    const [errorCountryCodeMsg ,setCountryCodeMsg ] = useState("");
    const [errorEmailMsg ,setErrorEmailMsg ] = useState("");
    const [errorPhoneMsg ,setErrorPhonesg ] = useState("");
    const [errorPasswordMsg ,setErrorPasswordMsg ] = useState("");
    const [errorAddressMsg ,setErrorAddressMsg ] = useState(""); 
    const [errorCityMsg ,setErrorCityMsg ] = useState(""); 
    const [errorConfirmPasswordMsg ,setErrorConfirmPasswordMsg] = useState("");
    const [source , setSource ]  =  useState("");
    const [imageSource ,setImageSource] = useState("");
    
  //  const {signUp} = React.useContext(AuthContext);

  function validateSpecialChars(value){
    return ((/[ `!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?~]/).test(value))
    
  }
  function isEmoji(str) {
    var ranges = [
        '(?:[\u2700-\u27bf]|(?:\ud83c[\udde6-\uddff]){2}|[\ud800-\udbff][\udc00-\udfff]|[\u0023-\u0039]\ufe0f?\u20e3|\u3299|\u3297|\u303d|\u3030|\u24c2|\ud83c[\udd70-\udd71]|\ud83c[\udd7e-\udd7f]|\ud83c\udd8e|\ud83c[\udd91-\udd9a]|\ud83c[\udde6-\uddff]|[\ud83c[\ude01-\ude02]|\ud83c\ude1a|\ud83c\ude2f|[\ud83c[\ude32-\ude3a]|[\ud83c[\ude50-\ude51]|\u203c|\u2049|[\u25aa-\u25ab]|\u25b6|\u25c0|[\u25fb-\u25fe]|\u00a9|\u00ae|\u2122|\u2139|\ud83c\udc04|[\u2600-\u26FF]|\u2b05|\u2b06|\u2b07|\u2b1b|\u2b1c|\u2b50|\u2b55|\u231a|\u231b|\u2328|\u23cf|[\u23e9-\u23f3]|[\u23f8-\u23fa]|\ud83c\udccf|\u2934|\u2935|[\u2190-\u21ff])' // U+1F680 to U+1F6FF
    ];
    if (str.match(ranges.join('|'))) {
        return true;
    } else {
        return false;
    }
}

    const validator = (type,value) => {

        console.log("type",type);

        switch(type){

            case 'user_name':

                setUsername(value)
                if(!value){
                    setErrorUserNameMsg("Please Enter User Name");
                    setErrorTrue(true);
                    return false
                }
                else if(validateSpecialChars(value)){
                    setErrorUserNameMsg("Username Should Not Contain Any Special Characters");
                    setErrorTrue(true);
                    return false
                }
                else if(isEmoji(value)){
                    setErrorUserNameMsg("Username Should Not Contain Any Special Characters");
                    setErrorTrue(true);
                    return false
                }
                else if(value.length < 4){

                    setErrorUserNameMsg("Username Must Be Of Greater Than 4 Characters ");
                    setErrorTrue(true);
                    return false
                    
                }
                
                setErrorUserNameMsg("");
                setErrorTrue(false);
                
                return true
            
             case 'email' :{
                setEmail(value)
               if(!value){
                    setErrorEmailMsg("Please Enter Email");
                    setErrorTrue(true);
                   return false

                }
                else if(!(/[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/.test(
                value))){
                    setErrorEmailMsg("Please Enter Valid Email");
                    setErrorTrue(true);
                    return false
                }
                setErrorEmailMsg("");
                setErrorTrue(false);
                return true
            }
            case 'country_code' :{
                setCountryCode(value)
               if(!value){
                    setCountryCodeMsg("Please Select Country Code");
                    setErrorTrue(true);
                   return false

                }
                
                setCountryCodeMsg("");
                setErrorTrue(false);
                return true
            }
            case 'password' :{

                setPassword(value);
                if(!value){

                    setErrorPasswordMsg("Please Enter Password");
                    setErrorTrue(true);
                    return false

                }else if(value.length < 8 || value.length > 16){

                    setErrorPasswordMsg("Password Must Be Of Greater Than 8 Characters And Less Than 16.");
                    setErrorTrue(true);
                    return false
                    
                }
                setErrorPasswordMsg("");
                setErrorTrue(false);
                return true
            }
            case 'mobile' :{

                setPhone(value)
               if(!value){
                setErrorPhonesg("Please Enter Mobile Number");
                setErrorTrue(true);
                   return false

               }

               else if(validateSpecialChars(value)){
                setErrorPhonesg("Mobile Number Should Not Contain Any Special Characters");
                setErrorTrue(true);
                return false
              }

              else if(value.length < 7 || value.length >15){
                setErrorPhonesg("Mobile Number Should Be 7-15 Digits");
                setErrorTrue(true);
                return false
              }
              else if(value == '0000000'){
                setErrorPhonesg("Mobile Number Should Be valid");
                setErrorTrue(true);
                return false
            }

              else if(value == '00000000'){
                setErrorPhonesg("Mobile Number Should Be valid");
                setErrorTrue(true);
                return false
                
            }
            else if(value == '000000000'){
                setErrorPhonesg("Mobile Number Should Be valid");
                setErrorTrue(true);
                return false
                
            }
            else if(value == '0000000000'){
                setErrorPhonesg("Mobile Number Should Be valid");
                setErrorTrue(true);
                return false
                
            }
            else if(value == '00000000000'){
                setErrorPhonesg("Mobile Number Should Be valid");
                setErrorTrue(true);
                return false
                
            }
            else if(value == '000000000000'){
                setErrorPhonesg("Mobile Number Should Be valid");
                setErrorTrue(true);
                return false
                
            }
            


               setErrorPhonesg("");
               setErrorTrue(false);
               return true
            }
            case 'address' :{

                setAddress(value)
               if(!value){
                setErrorAddressMsg("Please Enter Address");
                setErrorTrue(true);
                   return false

               }
               else if(isEmoji(value)){
                setErrorAddressMsg("Address Should Not Contain Any Special Characters");
                setErrorTrue(true);
                return false
               }
               setErrorAddressMsg("");
               setErrorTrue(false);
               return true
            }

            case 'city' :{

                setCity(value)
               if(!value){
                setErrorCityMsg("Please Enter City");
                setErrorTrue(true);
                   return false

               }
               else if(isEmoji(value)){
                setErrorCityMsg("City Should Not Contain Any Special Characters");
                setErrorTrue(true);
                return false
               }
               setErrorCityMsg("");
               setErrorTrue(false);
               return true
            }
            case 'confirm_password' :{

                setConfirmPassword(value)
               if(!value){
                setErrorConfirmPasswordMsg("Please Enter Confirm Password");
                setErrorTrue(true);
                   return false

               }else if(value !== password){
                    setErrorConfirmPasswordMsg("Password And Confirm Password Must Be Same");
                    setErrorTrue(true);
                    return false
               }
               setErrorConfirmPasswordMsg("");
               setErrorTrue(false);
               return true
            }

            default :
                return true;
        }

    }

    const submitRegistration = async() => {

        console.log("bdhfhj");
       // await AsyncStorage.setItem("email",email);
       // props.navigation.navigate('OtpVerification');

        // if(!source){

        //     Alert.alert(
        //         "Alert",
        //         "Choose profile pic",
        //         [
        //           {
        //             text: "Cancel",
        //             onPress: () => console.log("Cancel Pressed"),
        //             style: "cancel"
        //           },
        //           { text: "OK", onPress: () => console.log("OK Pressed") }
        //         ],
        //         { cancelable: false }
        //       );

        //       return false;
        // }

        if(validator('user_name',username)
            && validator('email',email) 
            && validator('mobile',phone) 
            &&  validator('country_code',countryCode) 
            &&  validator('city',city) 
            && validator('address',address)
            && validator('password',password) 
            && validator('confirm_password',password) 
          
           
        ){
           // name,email,mobile,country_id,password,confirm_password,device_type,device_token

            let formdata = new FormData();
            formdata.append("name",username);
            formdata.append("email",email);
            formdata.append("mobile",phone);
            formdata.append("city",city);
            formdata.append("country_code",countryCode);
            formdata.append("password",password);
            formdata.append("confirm_password",confirm_password);
            formdata.append("device_type",Platform.OS == 'android' ? 1 : 2);
            formdata.append("agree_terms","1");
            formdata.append("address",address);
            formdata.append("profile_img",imageSource);
            formdata.append("device_token","dfhfbgfjgnfkgcfsafcsvhdks");

            console.log(formdata)

            dispatch(userActions.submitAccount(formdata))
                .then(response => {

                    if(!response.data.error){
                         showMessage('Account Created Successfully',false)

                          props.navigation.navigate('OtpVerification',
                          {"otp" : response.data.result.otp,
                          "id":response.data.result.id,
                          'email':email,
                          'path':'register'});
            
                    }else{
                      showMessage(response.data.message)
                    }

                }).catch(error => {
                  showMessage( "Something went wrong ! Please try again later .")
                    console.log("errr",error)

                })

          
        }

    }

   

    const onImagePicker = () => {

        console.log("here");

        ImagePicker.showImagePicker(options, (response) => {
            //console.log('Response = ', response);
          
            if (response.didCancel) {
              console.log('User cancelled image picker');
            } else if (response.error) {
              console.log('ImagePicker Error: ', response.error);
            } else if (response.customButton) {
              //console.log('User tapped custom button: ', response.customButton);
            } else {
              let source = { uri: response.uri };
          
              //console.log(source);
              setSource(source);
              setImageSource(response.uri);
              // You can also display the image using data:
              // const source = { uri: 'data:image/jpeg;base64,' + response.data };
          
            //   this.setState({
            //     avatarSource: source,
            //   });
            }
          });
    }


    return(
        <SafeAreaView  style={{backgroundColor:'white',flex:1}}>
        <KeyboardAwareScrollView>

            <View style={styles.container}>
                <Image source={require('../assets/logo2.png')} style={{marginTop:10,alignSelf:"center",width:'70%',height:100}} />
                <View style={styles.mainView}>
                    {/** onImagePicker */}
                    {/* <TouchableOpacity onPress={() => {onImagePicker()}}>
                        <AddPicture  source={source}/>
                    </TouchableOpacity> */}
                  
                    <CustomTextInput 
                            labelText="User Name"
                            placeholder="Enter User Name"
                            isSecureText={false}
                            value={username}
                            //errorTrue={errorTrue}
                            errorMsg={errorUserNameMsg}
                            customTextInputStyle={errorUserNameMsg !== "" ?  {paddingBottom:2} : {paddingBottom:10}}
                            onChangeText = {(value)=> {validator('user_name',value)}}
                        />
                        <CustomTextInput 
                            labelText="E-mail"
                            placeholder="Enter Email"
                            isSecureText={false}
                            value={email}
                            //errorTrue={errorTrue}
                            customTextInputStyle={errorEmailMsg !== "" ?  {paddingBottom:2} : {paddingBottom:10}}
                            errorMsg={errorEmailMsg}
                            onChangeText = {(value)=> {validator('email',value)}}
                        />
                        <CustomTextInput 
                            labelText="Phone Number"
                            placeholder="Enter Phone Number"
                            isSecureText={false}
                            value={phone}
                            maxLength={14}
                            keyboardType="phone-pad"
                            //errorTrue={errorTrue}
                            customTextInputStyle={errorPhoneMsg !== "" ?  {paddingBottom:2} : {paddingBottom:10}}
                            errorMsg={errorPhoneMsg}
                            onChangeText = {(value)=> {validator('mobile',value)}}
                        />


        <View style={stylesDropdown.mainView}>
            <Text style={stylesDropdown.labelStyle} >Country Code</Text>
            <View style={stylesDropdown.textInputCard}>
             
                    
                            <Dropdown
                            placeholder="Country code"
                            dropdownOffset={{top:10,marginLeft:30,left:0}}
                            value={countryCode}
                            itemCount={8}
                            fontSize={13}
                            onChangeText={(value) => {validator('country_code',value)}}
                            containerStyle={{width:'95%',marginRight:0,}}
                            data={countryList.countries}
                            inputContainerStyle={{ borderBottomColor: 'transparent' }} // remove underline color 
                            style={[stylesDropdown.textInputStyle]} //for changed text color
                           // baseColor={colors.COLOR_PRIMARY} //for theme color like dropdown button
                            //textColor={colors.COLOR_PRIMARY} // for item list color
                            //itemTextStyle={{backgroundColor:"blue",textColor:"white"}}//for item style
                        />
                   
             

            {
                // props.errorMsg !== "" 
                errorCountryCodeMsg
                ?
                <Text style={stylesDropdown.textError}>Choose Country Code </Text>
                :
                null
            }
            </View>
        </View>
    

                        <CustomTextInput 
                            labelText="City"
                            placeholder="Enter your City"
                            value={city}
                            // keyboardType="phone-pad"
                            //errorTrue={errorTrue}
                            customTextInputStyle={errorCityMsg !== "" ?  {paddingBottom:2} : {paddingBottom:10}}
                            errorMsg={errorCityMsg}
                            onChangeText = {(value)=> {validator('city',value)}}
                        />

                        <CustomTextInput 
                            labelText="Address"
                            placeholder="Enter your Address"
                            isSecureText={false}
                            value={address}
                            // keyboardType="phone-pad"
                            //errorTrue={errorTrue}
                            customTextInputStyle={errorAddressMsg !== "" ?  {paddingBottom:2} : {paddingBottom:10}}
                            errorMsg={errorAddressMsg}
                            onChangeText = {(value)=> {validator('address',value)}}
                        />
                        <CustomTextInput 
                            labelText="Password"
                            placeholder="Enter Password"
                            customTextInputStyle={{width:"85%"}}
                            isSecureText={true}
                            secureTextEntry={true}
                            value={password}
                            //errorTrue={errorTrue}
                            errorMsg={errorPasswordMsg}
                            customTextInputStyle={errorPasswordMsg !== "" ?  {paddingBottom:2} : {paddingBottom:10}}
                            onChangeText = {(value)=> {validator('password',value)}}
                            />

                        <CustomTextInput 
                            labelText="Confirm Password"
                            placeholder="Enter Confirm Password"
                            customTextInputStyle={{width:"85%"}}
                            isSecureText={true}
                            secureTextEntry={true}
                            value={confirm_password}
                            //errorTrue={errorTrue}
                            errorMsg={errorConfirmPasswordMsg}
                            customTextInputStyle={errorConfirmPasswordMsg !== "" ?  {paddingBottom:2} : {paddingBottom:10}}
                            onChangeText = {(value)=> {validator('confirm_password',value)}}
                            />

                            <CustomButton 
                            onPress={() => submitRegistration()}
                            btnText="SUBMIT" 
                            customStyle={{marginLeft:10,marginRight:10,marginTop:20}} />

                            <TouchableOpacity 
                            style={{margin:10}} 
                            onPress={()=> props.navigation.navigate("Login")}
                            >
                                <Text style={styles.dontHveAccStyle}>Already have an account ? <Text style={styles.signUPText}>Sign In</Text></Text>
                            </TouchableOpacity>


                    

                </View>
            </View>

        </KeyboardAwareScrollView>
        { common.loading  && <ProgressBar/> }
        </SafeAreaView>
        
    )
}



const styles = StyleSheet.create({

    container:{
        flex:1,
        backgroundColor:"white"
    },
    mainView:{
        justifyContent:'center',
        alignItems:'center',
        margin:20,
    },
    dontHveAccStyle:{
        fontSize:15
    },
    signUPText:{
        color : COLORS.theme_orange
    }
})


const stylesDropdown =  StyleSheet.create({

    mainView :{
        width:"100%",
        height:100,
        marginBottom:0,
      

    },
    labelStyle:{
        fontSize:15,
        color:COLORS.label_grey
    },
    textInputCard:{
        //flexDirection:'row',
         //justifyContent:'space-between',
       // alignItems:"center",
        marginTop:10,
        borderColor:"white",
        backgroundColor:"white",
        width:"100%",
        height:'auto',
        paddingTop:Platform.OS == 'ios' ?  4 : 4 ,
        shadowOpacity:4,
        //ßßshadowColor:'grey',
        borderWidth:1,
        shadowOffset: {width: 0, height: 2},
        //shadowOpacity: 0.8,
        elevation: 8,
        //backgroundColor:'green'

    },
    textError:{
        color:"red",
        fontSize:10,
        paddingLeft:10,
        paddingBottom:2
    },
    textInputCardRow:{
        flexDirection:'row',
        justifyContent:'space-between',
        alignItems:"center",
        // marginTop:10,
        // borderColor:"white",
        // backgroundColor:"white",
        width:"100%",
        // height:null,
        // padding:10,
        // shadowOpacity:4,
        // //ßßshadowColor:'grey',
        // borderWidth:1,
        // shadowOffset: {width: 0, height: 6},
        // //shadowOpacity: 0.8,
        // elevation: 8,

    },
    textInputStyle:{
        paddingLeft:10,
        width:"100%",
        textAlignVertical: "center"
        
    }

})

export default Registration;