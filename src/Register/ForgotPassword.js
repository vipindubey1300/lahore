import React ,{useState }from 'react';
import { Text ,View ,StyleSheet, SafeAreaView} from 'react-native';
import CustomTextInput from '../CustomUI/CustomTextInput';
import CustomButton from '../CustomUI/CustomButton';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import {showMessage} from '../Utils/ShowMessage';
import ProgressBar from '../Utils/ProgressBar';

import { useDispatch,useSelector } from 'react-redux';
import  {forgotPassword} from '../redux/actions/user_action';

const ForgotPassword = (props) => {

    const [email,setEmail] = useState("");
    const [errorEmailMsg ,setErrorEmailMsg ] = useState("");

    const dispatch = useDispatch();
    const common = useSelector(state => state.common);


    const validator = (type,value) => {
        switch(type){
             case 'email' :{
              setEmail(value)
               if(!value){
                    setErrorEmailMsg("Please Enter Email");
                     return false
                }
                else if(!(/[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/.test(
                value))){
                    setErrorEmailMsg("Please Enter Valid Email");
                    return false
                }
                setErrorEmailMsg("");
                return true
            }
            default :
                return true;
        }

    }

    const onSubmit = async() => {

        if(validator('email',email)){
            let formdata = new FormData();
            formdata.append("email",email);
        
            dispatch(forgotPassword(formdata))
            .then(response=> {
                console.log("LOGIN REsponse ---->>>>",response)

                if(!response.error){

                    // AsyncStorage.setItem("auth_token",response.data.result.auth_token);
                    //props.navigation.navigate('Home');
                    showMessage("OTP has been sent to your registered Email",false)
                    props.navigation.navigate('OtpVerification',
                    {"email" : email,
                    "id":response.id,
                    'path':'forgot'
                    });

                }else{

                    // if( response.result && response.result.email_verify == 2) props.navigation.navigate('OtpVerification',{"otp" : response.result.email_otp_token,"id":response.result.id,'path':'login'});
                    // else if(response.result && response.result.zipcode == null) props.navigation.navigate('ZipCode',{"id":response.result.id});
                  // props.navigation.navigate('Home');
                    showMessage(response.message)
        
                }

            })
            .catch(error => {
                showMessage(error.message)
            })
         }
    }

    return(
        <SafeAreaView style={{backgroundColor:'white',flex:1}}>
        <KeyboardAwareScrollView>

            <View style={styles.container}>
                <View style={styles.mainView}>
                <CustomTextInput 
                labelText="E-mail"
                placeholder="Enter Your Registered Email"
                isSecureText={false}
                value={email}
                customTextInputStyle={errorEmailMsg !== "" ?  {paddingBottom:2} : {paddingBottom:10}}
                errorMsg={errorEmailMsg}
                onChangeText = {(value)=> {validator("email",value)}}
            />

                    <CustomButton   onPress={() => onSubmit()}
                    btnText="SUBMIT" customStyle={{marginTop:20}} />

                    </View>
            
            </View>

        </KeyboardAwareScrollView>
        { common.loading  && <ProgressBar/> }
       
        </SafeAreaView>
    )
}


const styles = StyleSheet.create({

    container:{
        flex:1,
        backgroundColor:'white'
    },
    mainView:{
        margin:20,
       
    }
})
export default ForgotPassword;