import React, { useState ,useEffect} from 'react';
import { Text, Image, View ,StyleSheet,TouchableOpacity, SafeAreaView,Alert} from 'react-native';
import CustomTextInput from '../CustomUI/CustomTextInput';
import CustomButton from '../CustomUI/CustomButton';
import {COLORS} from '../Utils/Utils';
import ProgressBar from '../Utils/ProgressBar';
import AuthContext from '../AuthContext/AuthContext';
import AsyncStorage from '@react-native-community/async-storage';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { useDispatch,useSelector } from 'react-redux';
import  {submitLogin} from '../redux/actions/user_action';

import {showMessage} from '../Utils/ShowMessage';
const Login = (props) => {

   // const {sigIn} = React.useContext(AuthContext);

    const [email,setEmail] = useState("");
    const [password ,setPassword ] = useState("");

    const [errorEmailMsg ,setErrorEmailMsg ] = useState("");
    const [errorPasswordMsg ,setErrorPasswordMsg ] = useState("");
    const dispatch = useDispatch();

    const common = useSelector(state => state.common);


     useEffect(() => { 
        console.log("LOGIN MOUNT----",common)    
    }, [common]);


    const validator = (type,value) => {

        console.log("type",type);

        switch(type){

             case 'email' :{

              
                setEmail(value)
               if(!value){
                    setErrorEmailMsg("Please Enter Email");
                    
                   return false

                }
                else if(!(/[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/.test(
                value))){
                    setErrorEmailMsg("Please Enter Valid Email");
                  
                    return false
                }
                setErrorEmailMsg("");
              
                return true
            }
            case 'password' :{

                setPassword(value);
                if(!value){

                    setErrorPasswordMsg("Please Enter Password");
                  
                    return false

                }else if(value.length < 8 || value.length > 16){

                    setErrorPasswordMsg("Password Must Be Of Greater Than 8 Characters And Less Than 16.");
                  
                    return false
                    
                }
                setErrorPasswordMsg("");
               
                return true
            }
           
            default :
                return true;
        }

    }

    const onSubmitLogin = async() => {

        if(validator('email',email) && validator('password',password)){
          
            let formdata = new FormData();
            formdata.append("email",email);
            formdata.append("password",password);
            formdata.append("device_type","1");
            // formdata.append("role_id","");
            formdata.append("device_token","dfhfbgfjgnfhbsjdskgcfsafcsvhdks");
            dispatch(submitLogin(formdata))
            .then(response=> {
                console.log("LOGIN REsponse ---->>>>",response)

                if(!response.error){
                    showMessage('Login Successfully',false)
                }else{

                    if( response.result && response.result.email_verify == 2) props.navigation.navigate('OtpVerification',{"otp" : response.result.email_otp_token,"id":response.result.id,'path':'login','email':email});
                    else if(response.result && response.result.zipcode == null) props.navigation.navigate('ZipCode',{"id":response.result.id});
                  // props.navigation.navigate('Home');
                
                    showMessage(response.message)
        
                }

            })
            .catch(error => {

                showMessage(error.message)
    

            })
          
        
         }

        // await AsyncStorage.setItem("id","67896789i0");
        // await AsyncStorage.setItem("email",email);
        //props.navigation.navigate('ZipCode');

    }

    return(
        <SafeAreaView  style={{backgroundColor:'white',flex:1}}>
        <KeyboardAwareScrollView>
            <View style={styles.container}>
                <Image source={require('../assets/logo2.png')} style={{marginTop:60,alignSelf:"center",width:'70%',height:100}} />
                <View style={styles.mainView}>
        
                        <CustomTextInput 
                            labelText="E-mail"
                            placeholder="Enter Email"
                            isSecureText={false}
                            value={email}
                            customTextInputStyle={errorEmailMsg !== "" ?  {paddingBottom:2} : {paddingBottom:10}}
                            errorMsg={errorEmailMsg}
                            onChangeText = {(value)=> {validator("email",value)}}
                        />
                        <CustomTextInput 
                            labelText="Password"
                            placeholder="Enter Password"
                            customTextInputStyle={{width:"85%"}}
                            isSecureText={true}
                            secureTextEntry={true}
                            value={password}
                            errorMsg={errorPasswordMsg}
                            customTextInputStyle={errorPasswordMsg !== "" ?  {paddingBottom:2} : {paddingBottom:10}}
                            onChangeText = {(value)=> {validator("password",value)}}
                            />
            
                            <TouchableOpacity 
                            style={{alignSelf:'flex-end',marginBottom:10}} 
                            onPress={()=> props.navigation.navigate("ForgotPassword")}
                            >
                                <Text style={{fontFamily:'roboto-bold',}}>Forgot Password ?</Text>
                            </TouchableOpacity>
            
                            <CustomButton btnText="SIGN IN" 
                            onPress={() => onSubmitLogin()}
                            customStyle={{marginLeft:10,marginRight:10,marginTop:20}}/>
                            <View style={{height:10}}/>
                           
                                <Text style={styles.dontHveAccStyle}>Don't have an account ? <Text  onPress={()=> props.navigation.navigate("Registration")} style={styles.signUPText}>Sign Up</Text></Text>

            
            
                        
        
                    </View>
            </View>

        </KeyboardAwareScrollView>

        { common.loading  && <ProgressBar/> }

      
       </SafeAreaView>
    )
}


const styles = StyleSheet.create({

    container:{
        flex:1,
        backgroundColor:"white"
    },
    mainView:{
        justifyContent:'center',
        alignItems:'center',
        margin:20,
    },
    dontHveAccStyle:{
        fontSize:15
    },
    signUPText:{
        color : COLORS.theme_orange
    }
})

export default Login;