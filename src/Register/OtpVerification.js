import React , {useState, useEffect,useRef} from 'react';
import {View , Text , TouchableOpacity,Image, StyleSheet,TextInput, SafeAreaView, Platform,Alert} from 'react-native';
import {KeyboardAwareScrollView}  from 'react-native-keyboard-aware-scroll-view';
import CustomTextInput from '../CustomUI/CustomTextInput';
import AuthContext from '../AuthContext/AuthContext';
import AsyncStorage from '@react-native-community/async-storage';
import CustomButton from '../CustomUI/CustomButton';
import {submitVerifyOtp,submitResendOtp} from '../redux/actions/user_action';
import {useDispatch,useSelector} from 'react-redux'
import {showMessage} from '../Utils/ShowMessage';
import ProgressBar from '../Utils/ProgressBar';
import { color } from 'react-native-reanimated';



const OtpVerification = (props) => {
    const common = useSelector(state => state.common);


    const [otp1,setOtp1] = useState("");
    const [otp2,setOtp2] = useState("");
    const [otp3,setOtp3] = useState("");
    const [otp4,setOtp4] = useState("");
    const [otp5,setOtp5] = useState("");
    const [otp6,setOtp6] = useState("");

    const  otp1Ref =  useRef();
    const otp2Ref = useRef();
    const otp3Ref = useRef();
    const otp4Ref = useRef();
    const otp5Ref = useRef();
    const otp6Ref = useRef();

    const [leftTime , setLeftTime]  = useState(45);
    const [isActive , setIsActive] = useState(true);

    const dispatch = useDispatch();
    //const {signUp} = React.useContext(AuthContext);

    function timer(){
        let timer;
        if(isActive){
            timer  = setTimeout(()=> {
                setLeftTime(leftTime => leftTime - 1);
                if(leftTime == 0){
                    setIsActive(false);
                    setLeftTime(0)
                }
            },1000);
        }else{
            clearInterval(timer);
        }
    }

    useEffect(()=>{
        timer()
        return () => {
            clearInterval(timer);
        }
      
    },[leftTime]);

    useEffect(() => {
        if( props.route.params !== undefined && props.route.params.otp !== null && props.route.params.otp !== undefined ){

            let otp = props.route.params.otp;
            let otp_string =  otp;
             console.log("otp",otp)
            // for(let i=0 ;i<otp_string.length ; i++ ){
            //     console.log("i",otp_string.charAt(i));

            // }
            setOtp1(otp_string[0]);
            setOtp2(otp_string[1]);
            setOtp3(otp_string[2]);
            setOtp4(otp_string[3]);
            setOtp5(otp_string[4]);
            setOtp6(otp_string[5]);
           
           
         }
    }, [])


    const submitRegistration =   async() => {
        if((otp1+otp2+otp3+otp4+otp5+otp6).toString().length != 6){
            showMessage("Enter OTP First")
        }
        else{
            let formdata =  new FormData();
        formdata.append("user_id",props.route.params.id);
        formdata.append("otp",otp1+otp2+otp3+otp4+otp5+otp6)
        // formdata.append("user_id",'376');
        // formdata.append("otp",'038154')
         console.log(formdata)
        dispatch(submitVerifyOtp(formdata))
        .then(response => {
            if(!response.data.error){
                showMessage( response.data.message , false)
                if(props.route.params.path){
                    if(props.route.params.path == 'login' || props.route.params.path == 'register')  props.navigation.navigate('ZipCode',{"id":props.route.params.id});
                    else if(props.route.params.path == 'forgot') props.navigation.navigate('ChangePassword',{"id":props.route.params.id,"email":props.route.params.email});

                }

            }else{
                showMessage( response.data.message)
            }
        })
        .catch(error => {

            Alert.alert(
                "Alert",
               error.message,
                [
                  {
                    text: "Cancel",
                    onPress: () => console.log("Cancel Pressed"),
                    style: "cancel"
                  },
                  { text: "OK", onPress: () => console.log("OK Pressed") }
                ],
                { cancelable: false }
              );

        })
        }
    }




    const resendOtp =   async() => {
        setIsActive(true);
        setLeftTime(45)
      
        let formdata =  new FormData();
        formdata.append("user_id",props.route.params.id);
        formdata.append("email",props.route.params.email);
         console.log(formdata)
         
        dispatch(submitResendOtp(formdata))
        .then(response => {
            if(!response.error){
               
                console.log(response.error)
                showMessage( response.message , false)
                setOtp1('')
                setOtp2('')
                setOtp3('')
                setOtp4('')
                setOtp5('')
                setOtp6('')
                
            }else{
                showMessage( response.message)
            }
        })
        .catch(error => {
            showMessage(error.message)
        })
        
    }

    return(
        <SafeAreaView style={{backgroundColor:'white',flex:1}}>

      
        <KeyboardAwareScrollView
        contentContainerStyle={{flexGrow:1}}>
        <View style={styles.container}>
            <Image source={require('../assets/logo.png')} />
            <Text style={{color:'black',fontSize:13,marginLeft:15,marginRight:15,marginTop:20,fontFamily:'roboto-medium'}}>OTP has been sent on your email address. Please enter it below.</Text>
            <View style={styles.otpView}>

                    <View style={styles.otpTextInputView}>
                        <TextInput
                            ref={otp1Ref}
                            style={styles.textInput}
                            value={otp1}
                            maxLength={1}
                            //editable={false}
                            onKeyPress={e =>{ 
                                if(e.nativeEvent.key == 'Backspace'){
                                   // otp1Ref.current.focus();
                            }else{
                                console.log("dhfgfj")
                                otp2Ref.current.focus()
                            }}}
                            onChangeText={(value)=>{setOtp1(value); 
                                if(value !== ""){
                                    otp2Ref.current.focus() 
                                }
                             
                            }}
                            keyboardType="phone-pad" 
                            />
                    </View>
                    <View style={styles.otpTextInputView}>
                        <TextInput
                          ref={otp2Ref}
                            style={styles.textInput}
                            maxLength={1}
                            value={otp2}
                            //editable={false}
                            onChangeText={(value)=>{setOtp2(value); 
                                if(value !== ""){
                                    otp3Ref.current.focus() 
                                }
                               }} 
                            onKeyPress={e =>{ 
                                if(e.nativeEvent.key == 'Backspace'){
                                    otp1Ref.current.focus();
                                   // setOtp1("");
                                    return;
                                }else{
                                    otp3Ref.current.focus()
                                }}} 
                                keyboardType="phone-pad" />
                    </View>
                    <View style={styles.otpTextInputView}>
                         <TextInput
                           ref={otp3Ref}
                            style={styles.textInput}
                            maxLength={1}
                            value={otp3}
                            //editable={false}
                            onChangeText={(value)=>{setOtp3(value); 
                                if(value !== ""){
                                    otp4Ref.current.focus();
                                }
                            }}  
                            onKeyPress={e =>{ 
                                if(e.nativeEvent.key == 'Backspace'){
                                    otp2Ref.current.focus();
                                    //setOtp2("");
                                    return;
                            }else{
                                otp4Ref.current.focus()
                            }}} 
                            keyboardType="phone-pad" />
                    </View>
                    <View style={styles.otpTextInputView}>
                         <TextInput
                           ref={otp4Ref}
                            style={styles.textInput}
                            value={otp4}
                            maxLength={1}
                            //editable={false}
                            onKeyPress={e =>{ 
                                if(e.nativeEvent.key == 'Backspace'){
                                    otp3Ref.current.focus();
                                   // setOtp3("");
                                    return;
                            }else{

                            }}}
                            onChangeText={(value)=>{setOtp4(value); 
                                if(value !== ""){
                                    otp5Ref.current.focus();
                                }
                            }}
                            keyboardType="phone-pad" />
                    </View>
                    <View style={styles.otpTextInputView}>
                         <TextInput
                           ref={otp5Ref}
                            style={styles.textInput}
                            value={otp5}
                            maxLength={1}
                            //editable={false}
                            onKeyPress={e =>{ 
                                if(e.nativeEvent.key == 'Backspace'){
                                    otp4Ref.current.focus();
                                    //setOtp4("");
                                    return;
                            }else{

                            }}}
                            onChangeText={(value)=>{setOtp5(value); 
                                if(value !== ""){
                                    otp6Ref.current.focus();
                                }
                            }}
                            keyboardType="phone-pad" />
                    </View>

                    <View style={styles.otpTextInputView}>
                         <TextInput
                           ref={otp6Ref}
                            style={styles.textInput}
                            value={otp6}
                            maxLength={1}
                            //editable={false}
                            onKeyPress={e =>{ 
                                if(e.nativeEvent.key == 'Backspace'){
                                    otp5Ref.current.focus();
                                   // setOtp5("");
                                    return;
                            }else{

                            }}}
                            onChangeText={(value)=>{setOtp6(value); 
                            }}
                            keyboardType="phone-pad" />
                    </View>

                </View>
{
    leftTime > 0 
    ?                <Text style={{margin:20,alignSelf:"flex-start",fontFamily:"roboto-medium"}}>Resend OTP : {leftTime} seconds</Text>
    :                 <Text onPress={()=> resendOtp()}
    style={{margin:20,alignSelf:"flex-start",fontFamily:"roboto-medium",color:'green'}}>Resend OTP</Text>

}
                <CustomButton 
                    onPress={() => submitRegistration()}
                    btnText="SUBMIT" 
                    customStyle={{marginLeft:10,marginRight:10,marginTop:20}} />
        
           


        </View>
        </KeyboardAwareScrollView>
        { common.loading  && <ProgressBar/> }
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({

    container:{
        flex:1,
        backgroundColor:"white",
        alignItems:'center',
        padding:20
    },



    textInput:{
        alignSelf:"center",fontSize:14,color:"black",padding:Platform.OS  == "ios" ? 10 : 0,
        textAlign:"center"
    },
    otpView:{
        width:"100%",
        flexDirection:"row",
        justifyContent:"space-evenly",
        alignItems:"center",
        marginTop:40,
    },
    otpTextInputView:{
        borderWidth:1.5,
        borderColor:"black",
        height:35,
        width:35,
        justifyContent:"center",
        alignItems:"center"
    },
    imageStyle:{
        marginTop:25,
        width:150,
        height:150,
    },
    forgotPassword:{
        color:"#2C684F",
        fontFamily:'roboto-medium',
        fontSize:14,
        marginRight:20,
        textDecorationLine:"underline",
        lineHeight:20
    },
    loginText:{
        fontSize:20,
        fontFamily:'roboto-bold',
        textAlign:"center",
        color:"white",

    },
    viewRow:{
        flexDirection:"row",
        justifyContent:"center",
        alignItems:'center',
        marginTop:23,
        position: 'absolute',
        bottom: 30
    },
    loginBtn:{
        width: '100%', 
        height: 50, 
        backgroundColor: '#2C684F', 
        alignItems:'center',justifyContent:'center'
       // position: 'absolute',
      //  bottom: 70
    },

})

export default OtpVerification;