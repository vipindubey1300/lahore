import React, { useState,useEffect } from 'react';
import { Text, Image, View ,StyleSheet,TouchableOpacity} from 'react-native';
import CustomTextInput from '../CustomUI/CustomTextInput';
import CustomButton from '../CustomUI/CustomButton';
import {COLORS} from '../Utils/Utils';
import AuthContext from '../AuthContext/AuthContext';
import AsyncStorage from '@react-native-community/async-storage';
import {showMessage} from '../Utils/ShowMessage';

import { useDispatch,useSelector } from 'react-redux';
import  {changePassword, submitAccount} from '../redux/actions/user_action';

import ProgressBar from '../Utils/ProgressBar';


const ChangePassword = (props) => {

   // const {sigIn} = React.useContext(AuthContext);

   const [email,setEmail] = useState("");
   const [errorEmailMsg ,setErrorEmailMsg ] = useState("");

    const [password ,setPassword ] = useState("");
    const [confirmPassword ,setConfirmPassword ] = useState("");
    const [newPassword ,setNewPassword ] = useState("");
    const common = useSelector(state => state.common);

    const [errorConfirmPasswordMsg ,setConfirmPasswordErrorMsg ] = useState("");
    const [errorNewPasswordMsg ,setNewPasswordErrorMsg ] = useState("");
    const [errorPasswordMsg ,setErrorPasswordMsg ] = useState("");


    const dispatch = useDispatch();

    useEffect(() => {
        if( props.route.params){

            let email = props.route.params.email;
            setEmail(email)
            
         }
    }, [])

    const validator = (type,value) => {
     
        switch(type){
          
            case 'new_pass' :{
                setNewPassword(value)
               if(!value){
                setNewPasswordErrorMsg("Please Enter New Password");  
                   return false
                }
                else if(value.length < 8 || value.length > 16){
                    setNewPasswordErrorMsg("New Password Must Be Of Greater Than 8 Characters And Less Than 16.");
                    return false
                    
                }
                setNewPasswordErrorMsg("");
                return true
            }
            case 'cnf_pass' :{
                setConfirmPassword(value)
               if(!value){
                setConfirmPasswordErrorMsg("Please Enter Confirm Password");  
                   return false
                }
                else if(value !== newPassword){
                    setConfirmPasswordErrorMsg("Password And Confirm Password Must Be Same");
                    return false
               }
                setConfirmPasswordErrorMsg("");
                return true
            }
            default :
                return true;
        }

    }

    const onSubmit = async() => {

        if(validator('new_pass',newPassword) && validator('cnf_pass',confirmPassword)){
            let formdata = new FormData();
            formdata.append("email",email);
            formdata.append("password",newPassword);

            formdata.append("confirm_password",confirmPassword);

            console.log("LOGIN REsponse ---->>>>",formdata)
            dispatch(changePassword(formdata))
            .then(response=> {
              
                if(!response.error){
                    props.navigation.navigate('Login');
                    showMessage(response.message,false)

                }else{
                    // props.navigation.navigate('Login');
                    showMessage(response.message)
                }
            })
            .catch(error => {
                showMessage(error.message)
            })
         }
    }

    return(
   
         <View style={styles.container}>
         
         <View style={styles.mainView}>
 
                <CustomTextInput 
                labelText="E-mail"
                placeholder="Registered Email"
                isSecureText={false}
                editable={false}
                value={email}
                customTextInputStyle={errorEmailMsg !== "" ?  {paddingBottom:2} : {paddingBottom:10}}
                errorMsg={errorEmailMsg}
                onChangeText = {(value)=> {validator("email",value)}}
                />

             <CustomTextInput 
                 labelText="New Password"
                 placeholder="Enter New Password"
                 customTextInputStyle={{width:"85%"}}
                 isSecureText={true}
                 secureTextEntry={true}
                 value={newPassword}
                 errorMsg={errorNewPasswordMsg}
                 customTextInputStyle={errorNewPasswordMsg !== "" ?  {paddingBottom:2} : {paddingBottom:10}}
                 //onChangeText = {(value)=> {setNewPassword(value)}}
                 onChangeText = {(value)=> {validator("new_pass",value)}}
                 />

                <CustomTextInput 
                 labelText="Confirm Password"
                 placeholder="Enter Confirm Password"
                 customTextInputStyle={{width:"85%"}}
                 isSecureText={true}
                 secureTextEntry={true}
                 value={confirmPassword}
                 errorMsg={errorConfirmPasswordMsg}
                 customTextInputStyle={errorConfirmPasswordMsg !== "" ?  {paddingBottom:2} : {paddingBottom:10}}
                // onChangeText = {(value)=> {setConfirmPassword(value)}}
                onChangeText = {(value)=> {validator("cnf_pass",value)}}
                 />
 
               
                 <CustomButton btnText="UPDATE" 
                 onPress={() => onSubmit()}
                 customStyle={{marginLeft:10,marginRight:10,marginTop:20}}/>
 
 
         </View>
         { common.loading  && <ProgressBar/> }
     </View>
    )
}


const styles = StyleSheet.create({

    container:{
        flex:1,
        backgroundColor:"white"
    },
    mainView:{
        justifyContent:'center',
        alignItems:'center',
        margin:20,
        marginTop:40
    },
    dontHveAccStyle:{
        fontSize:15
    },
    signUPText:{
        color : COLORS.theme_orange
    }
})

export default ChangePassword;