import React ,{useState }from 'react';
import { Text ,View ,StyleSheet,Image,TouchableOpacity, SafeAreaView,Alert} from 'react-native';
import CustomTextInput from '../CustomUI/CustomTextInput';
import CustomButton from '../CustomUI/CustomButton';
import AuthContext from '../AuthContext/AuthContext';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import  {addZipCode} from '../redux/actions/user_action';
import { useDispatch,useSelector } from 'react-redux';
import {showMessage} from '../Utils/ShowMessage';
import ProgressBar from '../Utils/ProgressBar';


const ZipCode = (props) => {

    const [zipCode,setZipCode] = useState("");
    const [errorZipMsg ,setErrorZipMsg ] = useState("");
    //const {signIn} = React.useContext(AuthContext);

    // console.log(AuthContext);
    const dispatch = useDispatch();

    const common = useSelector(state => state.common);


    const validator = (type,value) => {
        switch(type){
             case 'zip' :{
               setZipCode(value)
               if(!value){
                   setErrorZipMsg("Please enter zip - code");
                   return false
                }
                setErrorZipMsg("");
              
                return true
            }
         
            default :
                return true;
        }

    }

    const onSubmitZip = async() => {

        if(validator('zip',zipCode)){
            let formdata = new FormData();
            formdata.append("user_id",props.route.params.id);
            formdata.append("zipcode",zipCode);
            dispatch(addZipCode(formdata))
            .then(response=> {
                console.log("ZIP CODE REsponse ---->>>>",response)
                if(!response.error){
                }else{
                    showMessage(response.message)
                }

            })
            .catch(error => {
                showMessage(error.message)
            })
          
        
         }
    }

    function onSkipZip(){
        console.log(props.route.params)
        // let obj = {
        //     'id':props.route.params.id,
        //     'email':props.route.params.email,
        //     'image':props.route.params.user_image,
        //     'token':props.route.params.auth_token,
        //     'name':props.route.params.name
        // } 
        // dispatch({ type:types.SAVE_USER_RESULTS,  user:obj })
    }

    return(
        <SafeAreaView style={{backgroundColor:'white',flex:1}}>
        <KeyboardAwareScrollView>
        <View style={styles.container}>
                {/* <TouchableOpacity 
                onPress={ async()=> { 
                    // signIn("67896789i0");
                 //props.navigation.push('Home')
                 onSkipZip()
                }}
                 style={{alignSelf:"flex-end",
                 marginTop:20,}}
                >
                    <Text style={{fontFamily:"roboto-bold",color:"#4FAB2B",fontSize:15,textAlign:"right",}}>Skip</Text>
                  
                </TouchableOpacity> */}
                <View style={{flex:0.8, alignSelf:"center",marginTop:20,marginBottom:40}} >
                <Image source={require('../assets/zip.png')}/>
                </View>
               
                <CustomTextInput
                     isSecureText={false}
                     customTextInputStyle={errorZipMsg !== "" ?  {paddingBottom:2} : {paddingBottom:10}}
                     errorMsg={errorZipMsg}
                     placeholder="Enter your Zip Code"
                     labelText="Zip Code"
                     value={zipCode}
                      keyboardType="phone-pad"
                     // onChangeText={(value)=>setZipCode(value)} 
                     onChangeText = {(value)=> {validator("zip",value)}}

                    />

                <CustomButton btnText="ENTER" customStyle={{marginTop:20}} 
                onPress={async()=>{ onSubmitZip()
                }} />

        </View>
        </KeyboardAwareScrollView>
        { common.loading  && <ProgressBar/> }
        </SafeAreaView>
    )
}


const styles = StyleSheet.create({

    container:{
        flex:1,
        backgroundColor:'white',
       // width:"100%",
        alignItems:"center",
        justifyContent:"space-between",
        paddingLeft:20,
        paddingRight:20,
        
    },
    mainView:{
        margin:20,
       
    }
})
export default ZipCode;