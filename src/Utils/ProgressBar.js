import React from 'react';
import {Text, View, Image,Dimensions,
    ToastAndroid,StatusBar,StyleSheet, TouchableOpacity, SafeAreaView} from 'react-native';


import {COLORS} from './Utils';

import {
    BallIndicator,
    BarIndicator,
    DotIndicator,
    MaterialIndicator,
    PacmanIndicator,
    PulseIndicator,
    SkypeIndicator,
    UIActivityIndicator,
    WaveIndicator
  } from 'react-native-indicators';



const ProgressBar = () => {
    return(
        <View style={[
            StyleSheet.absoluteFill,
            { backgroundColor: 'rgba(0, 0, 0, 0.5)', justifyContent: 'center' }
          ]}>
          <UIActivityIndicator
           color={COLORS.loader_color}
           />
          </View>
    )
  };
  export default ProgressBar;





 