import Snackbar from 'react-native-snackbar';
import {COLORS} from './Utils';


export const showMessage = async (message,error = true) => {
  Snackbar.show({
    title: message,
    duration: Snackbar.LENGTH_SHORT,
    backgroundColor: error ?  'red' : COLORS.theme_green,
    color: error ?  'white' : 'black',
   
  });
}

