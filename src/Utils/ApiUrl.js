export default {

    image_base_url: "http://webmobril.org/dev/lahore-app/",
    base_url : "http://webmobril.org/dev/lahore-app/api/",
    signup : "signup",
    login : "login",
    verify_otp : "verify_otp",
    resend_otp : "resend_otp",
    zip : "zipcode",
    forgot:'forgot_password',
    change_password:'update_password',
    reset_password:'reset_password',
    home:'home_listing',
    category:'categories_listing',
    details:'get_product_details',
    category_product:'get_products',
    get_profile:'get_profile',
    update_profile:'edit_profile',
    add_to_cart:'cart',
    payment:'payment',
    cart_update:'cart_update',
    cart_remove_product:'cart_remove_product',
    cart_listing:'cart_listing',
    whishlist:'whishlist',
    popular_products:'popular_products',
    whishlist_list:'whishlist_list',
    contact_us:'contact_us',
    item_search:'item_search',
    about_us:'about_us',
    faq:'faq',
    add_address:'add_address',
    address_listing:'address_listing',
    edit_address:'edit_address',
    remove_address:'remove_address',
    get_checkout_data:'get_checkout_data',
    order_place:'order_place',
    my_orders:'my_orders',
    cancel_order:'cancel_order',
    'get-notification':'get-notification',
    'api-ordernotification-success':'api-ordernotification-success',
    'get-delivery-slots':'get-delivery-slots',
    reorder:'reorder',
    rating:'rating',
    cart_remove_product:'cart_remove_product',
    pages:'pages'







} 