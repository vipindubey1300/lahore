export const COLORS = {
    'theme_orange' : "#F17F3A" ,
    'theme_green' : '#4FAB2B',
    "label_grey" : "#A9A9A9",
    "search_color" : "#E6E6E6",
    "dark_black" : "#a8a8a8",
    "desp_grey" : "#9F9F9F",
    "loader_color": '#4FAB2B',
}