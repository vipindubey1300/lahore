import React ,{useState} from 'react';
import {View ,Text, Image, TouchableOpacity,StyleSheet, BackHandler, Alert } from 'react-native';
import { Card } from 'react-native-elements';
import {COLORS} from '../Utils/Utils'
import AntDesign from 'react-native-vector-icons/AntDesign';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import ApiUrl from '../Utils/ApiUrl';
import  {updateCart} from '../redux/actions/home';
import { useDispatch,useSelector } from 'react-redux';
import {showMessage} from '../Utils/ShowMessage';
import  {getItemDetails,addToCart,addToWishlist} from '../redux/actions/home';




const WishlistComponent = (props) => {
    const createAlert = (id) =>
    Alert.alert(
      "Delete",
      "Are you sure you want to remove this item from the wishlist? ",
      [
        {
          text: "Cancel",
          onPress: () => console.log("Cancel Pressed"),
          style: "cancel"
        },
        { text: "OK", onPress: () => deleteWishlist() }
      ],
      { cancelable: false }
    );


    const [quantity, setQuantity] = useState(props.item.quantity);

    const dispatch = useDispatch();
    const userReducer = useSelector(state => state.user);

    const deleteWishlist = async() =>{
         
          let formData = new FormData();
          formData.append("item_id",props.item.itemdetails.id);
          formData.append("user_id",userReducer.user_details.id);
          //formData.append("status",1); //1 add 2 delete
          formData.append("status",2);
          console.log(formData)
          dispatch(addToWishlist(formData))
          .then(response=> {
              if(!response.error){
                //   console.log(response)
                //   showMessage(response.message,false)       
                  props.onDelete(props.item)                 
              }else  showMessage(response.message)
          })
          .catch(error => {
              showMessage(error.message)
          })
        
      }

     
   

    return (
            <Card containerStyle={styles.cardStyle}>
                <View style={{flexDirection:"row",width:"100%"}}>
                    <Image source={{uri:ApiUrl.image_base_url + props.item.itemdetails.item_image}} style={styles.imageStyle} resizeMode="cover"  />
                    <View style={{paddingLeft:0,flex:1,marginTop:0,flexDirection:'row',justifyContent:"space-between",alignItems:"center"}}>
                        <View style={{flex:0.9,padding:10}}>
                            <Text style={styles.productTitle}>{props.item.itemdetails.name}</Text>
                            <Text style={styles.productDesp}>{props.item.itemdetails.about}</Text>
                            <View style={{flexDirection:'row',justifyContent:"space-between",alignItems:'center',marginTop:10}}>
                                <Text style={{color:COLORS.desp_grey}}>DDK {props.item.itemdetails.price}</Text>
                                <View style={{height:20,width:1 ,backgroundColor:'grey'}} />
                                <Text style={styles.ViewMoreText}>View More</Text>
                                
                            </View>
                        </View>
                      
                        <View style={{flex:0.2,padding:10,justifyContent:'space-between',alignItems:'center'}}>
                            <TouchableOpacity onPress={()=> createAlert()}>
                            <MaterialCommunityIcons  name="delete-sweep" color="orange" size={30} />
                            </TouchableOpacity>
                        </View>
                        
                        
                    </View>
                </View>
            </Card>
            
            )
       
            
       
            
    
}

const styles = StyleSheet.create({
    
    circularImage:{
       
        width:60,
        height:60,
        alignItems:'center',
       justifyContent:"center",
       borderRadius:120/2,
       borderColor:COLORS.theme_orange,
       borderWidth:2.5,
       padding:32,
       flex:0.02

    },
    imageStyle:{
        margin:5,alignItems:"center",alignSelf:"center",width:60,height:60,borderRadius:120/2,
    },
    cardStyle:{
        justifyContent:'space-between',
        padding:10,
        margin:0,
        elevation:4,
        marginBottom:10,
        marginLeft:5,
        marginRight:5,
        borderRadius:5,
    },
    productTitle:{
        fontFamily:'roboto-medium',
        color:"black",
        fontSize:17,
        lineHeight:20,
        marginTop:5,
    },
    productDesp:{
        width:"100%",
        fontFamily:'roboto-regular',
        color:COLORS.desp_grey,
        fontSize:12,
        lineHeight:20,
        marginTop:0

    },
    ViewMoreText:{
        color:COLORS.desp_grey,
        fontSize:13,
        fontFamily:'roboto-regular'
    },

    quantityText:{
        fontFamily:'roboto-bold',
        fontSize:15,
        color:'black'
    },
    

    
})

export default WishlistComponent;