import React ,{useState,useImperativeHandle,ref} from 'react';
import {View ,Text, Image, TouchableOpacity,StyleSheet, BackHandler } from 'react-native';
import { Card } from 'react-native-elements';
import {COLORS} from '../Utils/Utils'
import ApiUrl from '../Utils/ApiUrl';
import AntDesign from 'react-native-vector-icons/AntDesign';



const QuantityComponent  = React.forwardRef((props,ref) => {
    const [quantity, setQuantity] = useState(1);

    useImperativeHandle(ref, () => ({
        getQuantity() {
            return quantity
        }
    }));

    function increaseQuantity(){
        setQuantity(quantity + 1)
        props.onChange(quantity + 1)
    }
    function decreaseQuantity(){
       if(quantity != 1)  {
        setQuantity(quantity - 1)
        props.onChange(quantity - 1)
       }
    }

    return(
        <View style={{flexDirection: props.isHorizontal ? 'row' : 'column',justifyContent:'center',alignItems:'center'}}>
            <AntDesign  onPress={() => decreaseQuantity()} 
             name="minuscircle" color="green" size={30} style={{marginRight:20}} />

             <Text style={styles.quantityText}>{quantity}</Text>

            <AntDesign  onPress={() => increaseQuantity()} 
             name="pluscircle" color="green" size={30} style={{marginLeft:20}} />
        </View>
    )
            
    
})
QuantityComponent.propTypes = {
   // isHorizontal: PropTypes.boolean,
  };

QuantityComponent.defaultProps ={
    isHorizontal:true
}

const styles = StyleSheet.create({
    
    circularImage:{
       
        width:70,
        height:70,
        alignItems:'center',
       justifyContent:"center",
       borderRadius:140/2,
       borderColor:COLORS.theme_orange,
       borderWidth:2.5,
    },
   
})

export default QuantityComponent;