import React ,{useEffect } from 'react';
import { Text } from 'react-native';

import Carousel from 'react-native-banner-carousel';
import { StyleSheet, Image, View, Dimensions } from 'react-native';
import ApiUrl from '../Utils/ApiUrl';
 
const BannerWidth = Dimensions.get('window').width  ;
const BannerHeight = 200;
 
const images = [
    require('./../assets/banner1.jpg'),
    require('./../assets/banner2.jpg'),
    require('./../assets/banner3.jpg'),
];
 

const CustomBanner = (props) => {

    useEffect(() => {
        console.log('bannerItems---',props.bannerItems)
        return () => {
           
        }
    }, [])

    const renderPage = (image, index) => {
      
        return (
            <View key={index}>
                <Image style={{ width:'96%', height: BannerHeight, alignSelf:"center", borderRadius:5}} source={{uri:ApiUrl.image_base_url + image.file}} />
            </View>
        );
    }


    return(
       
        <View style={styles.container}>
                <Carousel
                    autoplay
                    autoplayTimeout={5000}
                    loop
                    index={0}
                    pageSize={BannerWidth}
                >
                    {props.bannerItems.map((image, index) => renderPage(image, index))}
                </Carousel>
            </View>
    )
}


const styles = StyleSheet.create({
    container: {
        width:'100%',
        height:null,
        backgroundColor: 'white',
        justifyContent: 'center',
        marginLeft:30,
        marginRight:30,
        marginTop:10,
        alignSelf:"center",
        borderRadius:7

       
      
    },
});

export default CustomBanner ;