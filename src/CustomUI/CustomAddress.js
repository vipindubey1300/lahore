import React ,{useState} from 'react';
import {View , Text ,Image , TouchableOpacity, StyleSheet, Dimensions} from 'react-native';
import { Card } from 'react-native-elements';
import AntDesign from 'react-native-vector-icons/AntDesign';
import {COLORS} from '../Utils/Utils';


const CustomAddress = (props) => {

    return(
        <Card
            containerStyle={styles.container}>
                {
                    props.item.apartment_unit_no ?
                    <Text style={styles.addressText}>{props.item.apartment_unit_no}</Text>

                        :null

                }
                <Text style={styles.addressText}>{props.item.address}</Text>
                <Text style={styles.addressText}>{props.item.postal_code}</Text>
                <Text style={styles.addressText}>{props.item.state} {props.item.city}</Text>
               {
                   props.deliveryPart == false ?
                   null :
                   <View style={{flexDirection:'row',alignSelf:'flex-end'}}>
                   <Text style={[styles.addressText,{fontSize:14}]}>Delivery Address</Text>
                   {props.item.selected
                   ?
                   <TouchableOpacity 
                   onPress={() => {props.onSelectAddress(props.item.id,props.index)}}
                   >
                       <AntDesign  name="checkcircle" color={COLORS.theme_green} size={20} style={{marginLeft:10}} />
                   </TouchableOpacity>
                   
                   :
                   <TouchableOpacity 
                   onPress={() => {props.onSelectAddress(props.item.id,props.index)}}
                   >
                       <AntDesign  name="checkcircle" color={"#a9a9a9"} size={20} style={{marginLeft:10}} />
                   </TouchableOpacity>
                   }
                   
               </View>
               }
            </Card>
    )

}

CustomAddress.defaultProps = {
    deliveryPart: true
}

const styles =  StyleSheet.create({

    container:{
       // width:'100%',
        width:Dimensions.get('window').width * 0.85,
        elevation:4,
        justifyContent:'center',
         paddingTop:15,
         paddingBottom:10,
         paddingLeft:20,
         paddingRight:20,
         margin:0,
         borderRadius:10,
         elevation:2,
         shadowColor:'grey',
         marginLeft:10,
    },
    addressText:{
        fontSize:12,
        color:COLORS.dark_black,
        fontFamily:'roboto-medium',
        lineHeight:20
    }

})

export default CustomAddress;