import React from 'react' ;
import { View ,TouchableOpacity,StyleSheet,Text} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';

const CustomButton = (props) => {

    return(
        <TouchableOpacity onPress={props.onPress}
        style={[styles.btnViewStyle,props.customStyle]}>
            <LinearGradient 
            colors= {["#ff7b00","#4FAB2B"]}
            start= {{x: 0.0, y: 0}}
            end= {{ x: 1, y:0}} 
            style={[styles.btnViewStyle]}>
                
                     <Text style={styles.btnTextStyle}>{props.btnText}</Text>
            
            </LinearGradient>
        </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
 
    btnViewStyle:{
      width:'100%',
      height:50,
      justifyContent:"center",
      alignItems:"center"
    },

    btnTextStyle:{
        fontFamily:'roboto-medium',
        fontSize:17,
        color:'white',
        alignSelf:'center'
    }
})

export default CustomButton;