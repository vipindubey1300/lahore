import React ,{useState} from 'react';
import {View ,Text, Image, TouchableOpacity,StyleSheet, BackHandler } from 'react-native';
import { Card } from 'react-native-elements';
import {COLORS} from '../Utils/Utils'
import ApiUrl from '../Utils/ApiUrl';

const ProductItem = (props) => {

    return props.showCategories == 1
            ?
           ( <Card containerStyle={styles.cardStyle}>
                <View style={{flexDirection:"row",flex:1}}>
                    <View style={styles.circularImage} >
                    <Image source={{uri:ApiUrl.image_base_url+ props.item.icon_image}} style={styles.imageStyle} resizeMode="cover" />
                    </View>
                    <Text style={[styles.productTitle,{textAlign:"center",alignSelf:"center",flex:0.9}]}>{props.item.name}</Text>
                </View>
            </Card>)
       
            :
            (<Card containerStyle={styles.cardStyle}>
                <View style={{flexDirection:"row",flex:1,width:"100%"}}>
                    <Image source={{uri:ApiUrl.image_base_url+ props.item.item_image}} style={styles.imageStyle} resizeMode="cover"  />
                    <View style={{paddingLeft:20,flex:0.9,marginTop:10}}>
                        <Text style={styles.productTitle}>{props.item.name}</Text>
                        <Text style={styles.productDesp}>{props.item.about}</Text>
                        <View style={{flexDirection:'row',justifyContent:"space-between",width:"100%",marginTop:10}}>
                               <Text style={{ color:COLORS.desp_grey,}}>DDK {props.item.price}</Text>
                            <View style={{height:20,width:1 ,backgroundColor:'grey'}} />
                            <Text style={styles.ViewMoreText}>View More</Text>
                        </View>

                    </View>
                </View>
            </Card>
            
            )
       
            
       
            
    
}

const styles = StyleSheet.create({
    
    circularImage:{
       
        width:70,
        height:70,
        alignItems:'center',
       justifyContent:"center",
       borderRadius:140/2,
       borderColor:COLORS.theme_orange,
       borderWidth:2.5,
    },
    imageStyle:{
        margin:10,alignItems:"center",alignSelf:"center",width:60,height:60,borderRadius:120/2,
    },
    cardStyle:{
        justifyContent:'space-between',
        padding:15,
        margin:0,
        elevation:4,
        marginBottom:10,
        marginLeft:5,
        marginRight:5,
        borderRadius:5,
    },
    productTitle:{
        fontFamily:'roboto-medium',
        color:"black",
        fontSize:18,
        lineHeight:20
    },
    productDesp:{
        width:"100%",
        fontFamily:'roboto-regular',
        color:COLORS.desp_grey,
        fontSize:12,
        lineHeight:20

    },
    ViewMoreText:{
        color:'grey',
        fontSize:13,
        fontFamily:'roboto-regular'
    }


    

    
})

export default ProductItem;