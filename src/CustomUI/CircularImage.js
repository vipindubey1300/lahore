import React ,{useState, useEffect} from 'react';
import {View,Image,Text,StyleSheet} from 'react-native';
import { COLORS } from '../Utils/Utils';


const CircularImage = (props) => {

    let path = props.imagePath;
    return (
        <View style={{ flexDirection:'column',flex:1,alignItems:'center',justifyContent:'center',width:80,height:110}}>
        <View style={[styles.container,props.customStyles]}>
            <Image source={{uri:path}} 
            resizeMode="cover"
            style={{margin:10,alignItems:"center",alignSelf:"center",width:60,height:60,borderRadius:120/2}} />
           
        </View>
        <Text numberOfLines={1} style={styles.textstyle}>{props.typeName}</Text>
        </View>
    )
}

const styles = StyleSheet.create({

    container:{
       
        width:70,
        height:70,
        alignItems:'center',
       justifyContent:"center",
       borderRadius:140/2,
       borderColor:COLORS.theme_orange,
       borderWidth:3,

    },
    textstyle:{
        fontFamily:'roboto-bold',
       // fontWeight:"bold",
       textAlign:"center",
       lineHeight:15.8,
        fontSize:12,
        marginTop:5,
        color:COLORS.desp_grey
    }
})

export default CircularImage ;