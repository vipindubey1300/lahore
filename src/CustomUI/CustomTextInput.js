import React, { useState } from 'react';
import {StyleSheet,TextInput ,View ,Text, Image ,TouchableOpacity, Platform} from 'react-native';
import {COLORS } from '../Utils/Utils';

const CustomTextInput = (props) => {

    const [secureTextShow,setSecureTextShow]  = useState(true);

    return(
        <View style={styles.mainView}>
            <Text style={styles.labelStyle} >{props.labelText}</Text>
            <View style={styles.textInputCard}>
                <View style={styles.textInputCardRow}>
                    <TextInput 
                        {...props}
                        secureTextEntry={!props.isSecureText ?  false : secureTextShow}
                        style={[styles.textInputStyle,props.customTextInputStyle]}
                        onChangeText={(value)=> props.onChangeText(value)}
                        />
                    {
                        props.isSecureText 
                        ?
                        <TouchableOpacity onPress={() => setSecureTextShow(!secureTextShow)}>
                            <Image source={secureTextShow ? require('../assets/eye2.png') : require('../assets/eye.png')} style={{width:25,height:25,marginRight:0}} />
                        </TouchableOpacity>
                    
                        :
                        <View/>

                    }
                </View>

            {
                props.errorMsg !== "" 
                ?
                <Text style={styles.textError}>{props.errorMsg} </Text>
                :
                null
            }
            </View>
         
           
           

        </View>
    )
}

const styles =  StyleSheet.create({

    mainView :{
        width:"100%",
        height:100,
       // backgroundColor:'red'

    },
    labelStyle:{
        fontSize:15,
        color:COLORS.label_grey
    },
    textInputCard:{
        //flexDirection:'row',
        // justifyContent:'space-between',
        // alignItems:"center",
        marginTop:10,
        borderColor:"white",
        backgroundColor:"white",
        width:"100%",
        height:null,
        padding:Platform.OS == 'ios' ?  10 : 0 ,
        shadowOpacity:4,
        //ßßshadowColor:'grey',
        borderWidth:1,
        shadowOffset: {width: 0, height: 2},
        //shadowOpacity: 0.8,
        elevation: 8,

    },
    textError:{
        color:"red",
        fontSize:10,
        paddingLeft:10,
        paddingBottom:2
    },
    textInputCardRow:{
        flexDirection:'row',
        justifyContent:'space-between',
        alignItems:"center",
        // marginTop:10,
        // borderColor:"white",
        // backgroundColor:"white",
        width:"90%",
        // height:null,
        // padding:0,
        // shadowOpacity:4,
        // //ßßshadowColor:'grey',
        // borderWidth:1,
        // shadowOffset: {width: 0, height: 6},
        // //shadowOpacity: 0.8,
        // elevation: 8,

    },
    textInputStyle:{
        paddingLeft:10,
        width:"100%",
        textAlignVertical: "center"
        
    }

})

export default CustomTextInput;