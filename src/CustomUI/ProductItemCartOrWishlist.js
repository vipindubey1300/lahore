import React ,{useState} from 'react';
import {View ,Text, Image, TouchableOpacity,StyleSheet, BackHandler ,Alert} from 'react-native';
import { Card } from 'react-native-elements';
import {COLORS} from '../Utils/Utils'
import AntDesign from 'react-native-vector-icons/AntDesign';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import ApiUrl from '../Utils/ApiUrl';
import  {removeFromCart, updateCart} from '../redux/actions/home';
import { useDispatch,useSelector } from 'react-redux';
import {showMessage} from '../Utils/ShowMessage';




const ProductItemCartOrWishlist = (props) => {
    const [quantity, setQuantity] = useState(props.item.quantity);

    const dispatch = useDispatch();
    const userReducer = useSelector(state => state.user);

    const deleteCart = async() =>{
          let formData = new FormData();
          formData.append("cart_id",props.item.id);
          formData.append("user_id",userReducer.user_details.id);
          console.log(formData)
          dispatch(removeFromCart(formData))
          .then(response=> {
              if(!response.error){     
                  props.onDelete(props.item)                 
              }else  showMessage(response.message)
          })
          .catch(error => {
              showMessage(error.message)
          })
        
      }

    const createAlert = (id) =>
    Alert.alert(
      "Delete",
      "Are you sure you want to remove this item from the cart? ",
      [
        {
          text: "Cancel",
          onPress: () => console.log("Cancel Pressed"),
          style: "cancel"
        },
        { text: "OK", onPress: () => deleteCart() }
      ],
      { cancelable: false }
    );

    function updateQuantity(quant){
        let formData = new FormData();
        formData.append("cart_id",props.item.id);
        formData.append("quantity",quant);
        console.log(formData)
        dispatch(updateCart(formData))
        .then(response=> {
           
            if(!response.error){
               showMessage(response.message,false)

            }else{
                showMessage(response.message)
            }
        })
        .catch(error => {
            showMessage(error.message)
        })

    }


    function increaseQuantity(){
        updateQuantity(quantity + 1)
        setQuantity(quantity + 1)
        
    }
    function decreaseQuantity(){
       if(quantity != 1)  {
           updateQuantity(quantity - 1)
           setQuantity(quantity - 1)
          
         }
    }

    return (
            <Card containerStyle={styles.cardStyle}>
                <View style={{flexDirection:"row",width:"100%"}}>
                    <Image source={{uri:ApiUrl.image_base_url + props.item.cartitem.item_image}} style={styles.imageStyle} resizeMode="cover"  />
                    <View style={{paddingLeft:0,flex:1,marginTop:0,flexDirection:'row',justifyContent:"space-between",alignItems:"center"}}>
                        <View style={{flex:0.9,padding:10}}>
                            <Text style={styles.productTitle}>{props.item.cartitem.name}</Text>
                            <Text style={styles.productDesp}>{props.item.cartitem.about}</Text>
                            <View style={{flexDirection:'row',justifyContent:"space-between",alignItems:'center',marginTop:10}}>
                              <Text style={{color:COLORS.desp_grey}}>DDK {props.item.cartitem.price}</Text>
                                <View style={{height:20,width:1 ,backgroundColor:'grey'}} />
                                <Text onPress={createAlert}
                                style={[styles.ViewMoreText,{color:'red'}]}>Remove</Text>
                                
                            </View>
                        </View>
                        {props.showWishList 
                        ?
                        <View style={{flex:0.2,padding:10,justifyContent:'space-between',alignItems:'center'}}>
                            <TouchableOpacity  onPress={() => increaseQuantity()} >
                            <AntDesign  name="pluscircle" color="green" size={25} style={{marginBottom:5}} />
                            </TouchableOpacity>
                            <Text style={styles.quantityText}>{quantity}</Text>
                            <TouchableOpacity  onPress={() => decreaseQuantity()} >
                            <AntDesign 
                             name="minuscircle" color="green" size={25}  style={{marginTop:5}} />
                            </TouchableOpacity>
                        </View>
                        :
                        <View style={{flex:0.2,padding:10,justifyContent:'space-between',alignItems:'center'}}>
                            <TouchableOpacity>
                            <MaterialCommunityIcons  name="delete-sweep" color="orange" size={30} />
                            </TouchableOpacity>
                        </View>
                        }
                        
                    </View>
                </View>
            </Card>
            
            )
       
            
       
            
    
}

const styles = StyleSheet.create({
    
    circularImage:{
       
        width:60,
        height:60,
        alignItems:'center',
       justifyContent:"center",
       borderRadius:120/2,
       borderColor:COLORS.theme_orange,
       borderWidth:2.5,
       padding:32,
       flex:0.02

    },
    imageStyle:{
        margin:5,alignItems:"center",alignSelf:"center",width:60,height:60,borderRadius:120/2,
    },
    cardStyle:{
        justifyContent:'space-between',
        padding:10,
        margin:0,
        elevation:4,
        marginBottom:10,
        marginLeft:5,
        marginRight:5,
        borderRadius:5,
    },
    productTitle:{
        fontFamily:'roboto-medium',
        color:"black",
        fontSize:17,
        lineHeight:20,
        marginTop:5,
    },
    productDesp:{
        width:"100%",
        fontFamily:'roboto-regular',
        color:COLORS.desp_grey,
        fontSize:12,
        lineHeight:20,
        marginTop:0

    },
    ViewMoreText:{
        color:COLORS.desp_grey,
        fontSize:13,
        fontFamily:'roboto-regular'
    },

    quantityText:{
        fontFamily:'roboto-bold',
        fontSize:15,
        color:'black'
    },
    

    
})

export default ProductItemCartOrWishlist;