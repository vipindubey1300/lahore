import React from 'react' ;
import LinearGradient from 'react-native-linear-gradient';
import { View, StyleSheet, Text, TouchableOpacity, Image, SafeAreaView, Alert } from 'react-native';
import { Card } from 'react-native-elements';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import AsyncStorage from '@react-native-community/async-storage';
import AuthContext from '../AuthContext/AuthContext';
import { COLORS } from '../Utils/Utils';
import * as types  from '../redux/types';

import { useDispatch,useSelector } from 'react-redux';
import ApiUrl from '../Utils/ApiUrl';
import { emptyCart } from '../redux/actions/cart_action';


const CustomDrawer = (props) => {
    const dispatch = useDispatch();
    const userReducer = useSelector(state => state.user)

   // const {signOut} = React.useContext(AuthContext);

    const onSubmitSignout = () => {

        console.log("LOGOUT -----");
        AsyncStorage.getAllKeys().then(AsyncStorage.multiRemove);
        props.navigation.closeDrawer()
        dispatch({ type:types.LOGOUT_USER })
        dispatch(emptyCart({}))

      
    }


    const createAlert = (id) =>
    Alert.alert(
      "Logout",
      "Are you sure you want to logout?",
      [
        {
          text: "Cancel",
          onPress: () => console.log("Cancel Pressed"),
          style: "cancel"
        },
        { text: "OK", onPress: () => onSubmitSignout() }
      ],
      { cancelable: false }
    );
   
    return(
        <SafeAreaView style={{backgroundColor:'green',flex:1}}>
        <LinearGradient
            start={{ x: 0, y: 0.75 }}
            end={{ x: 1, y: 0}}
            locations={[0.1, 0.5, 1]}
            useAngle={true} angle={360} angleCenter={{x:0.5,y:0.5}}
            colors={['green', 'white', 'green']}
            style={styles.container}>
                <TouchableOpacity onPress={() => props.navigation.toggleDrawer()}>
                <Image source={require('../assets/back.png')} style={{width:30,height:30}} />
                </TouchableOpacity>
                <TouchableOpacity onPress={() => props.navigation.navigate('MyAccount')}>
                <Card 
                containerStyle={{paddingTop:7,paddingLeft:10,paddingRight:10,paddingBottom:7,borderRadius:5,marginBottom:35}}>
                    <View style={{flexDirection:"row",justifyContent:"space-between",alignItems:"center"}}>
                        <View style={styles.circularImage}>
                            <Image source={{uri:ApiUrl.image_base_url + userReducer.user_details.image}} style={styles.imageStyle} resizeMode="contain" />
                        </View>
                     
                        <View style={{flexDirection:"column",alignSelf:"center"}}>
                            <Text style={styles.nameText}>{userReducer.user_details.name}</Text>
                            <Text style={styles.emailText}>{userReducer.user_details.email}</Text>
                        </View>
                        <FontAwesome name={"angle-right"} size={25} />
                    </View>
                </Card>
                </TouchableOpacity >
                <Card 
                containerStyle={{paddingTop:0,paddingLeft:0,paddingRight:0,paddingBottom:0,borderRadius:5}}>
                    <TouchableOpacity onPress={() => props.navigation.navigate('Wishlist')}>
                    <View style={styles.rowStyle}>
                        <View style={{flexDirection:'row',alignItems:"center"}}>
                        <Image source={require('../assets/heart.png')} style={styles.imageStyle1} resizeMode="contain" />
                        <Text style={styles.titleText}>Wishlist</Text>
                        </View>
                        <FontAwesome name={"angle-right"} size={25} />
                    </View>
                    </TouchableOpacity>
                    <View   style={styles.viewLine1} />
                    <TouchableOpacity onPress={() => props.navigation.navigate('OrderHistory')}>
                    <View style={styles.rowStyle}>
                        <View style={{flexDirection:'row',alignItems:"center"}}>
                            <Image source={require('../assets/order_history.png')} style={styles.imageStyle1} resizeMode="contain" />
                            <Text style={styles.titleText}>Order History</Text>
                        </View>
                        <FontAwesome name={"angle-right"} size={25} />
                    </View>
                    </TouchableOpacity>
                    {/* <View   style={styles.viewLine2} />
                    <View style={styles.rowStyle}>
                        <View style={{flexDirection:'row',alignItems:"center"}}>
                            <Image source={require('../assets/settingf.png')} style={styles.imageStyle1} resizeMode="contain" />
                            <Text style={styles.titleText}>Settings</Text>
                        </View>
                        <FontAwesome name={"angle-right"} size={25} />
                    </View> */}
                    <View   style={styles.viewLine2} />
                    <TouchableOpacity onPress={() => props.navigation.navigate('ContactUs')}>
                    <View style={styles.rowStyle}>
                        <View style={{flexDirection:'row',alignItems:"center"}}>
                            <Image source={require('../assets/help.png')} style={[styles.imageStyle1]} resizeMode="contain" />
                            <Text style={styles.titleText}>Help</Text>
                        </View>
                        <FontAwesome name={"angle-right"} size={25} style={{alignSelf:"flex-end"}}/>
                    </View>
                    </TouchableOpacity>
                    <View   style={styles.viewLine2} />
                    <TouchableOpacity onPress={() => props.navigation.navigate('AboutUs')}>
                    <View style={styles.rowStyle}>
                        <View style={{flexDirection:'row',alignItems:"center"}}>
                            <Image source={require('../assets/about.png')} style={[styles.imageStyle1]} resizeMode="contain" />
                            <Text style={styles.titleText}>About Us</Text>
                        </View>
                        <FontAwesome name={"angle-right"} size={25} style={{alignSelf:"flex-end"}}/>
                    </View>
                    </TouchableOpacity>
                    <View   style={styles.viewLine2} />
                    <TouchableOpacity onPress={() => props.navigation.navigate('Faq')}>
                    <View style={styles.rowStyle}>
                        <View style={{flexDirection:'row',alignItems:"center"}}>
                            <Image source={require('../assets/faq.png')} style={[styles.imageStyle1]} resizeMode="contain" />
                            <Text style={styles.titleText}>FAQs</Text>
                        </View>
                        <FontAwesome name={"angle-right"} size={25} style={{alignSelf:"flex-end"}}/>
                    </View>
                    </TouchableOpacity>

                    <View   style={styles.viewLine1} />
                    <TouchableOpacity onPress={() => props.navigation.navigate('Privacy')}>
                    <View style={styles.rowStyle}>
                        <View style={{flexDirection:'row',alignItems:"center"}}>
                            <Image source={require('../assets/privacy.png')} style={styles.imageStyle1} resizeMode="contain" />
                            <Text style={styles.titleText}>Privacy Policy</Text>
                        </View>
                        <FontAwesome name={"angle-right"} size={25} />
                    </View>
                    </TouchableOpacity>
                   
                </Card>

                <TouchableOpacity onPress={() =>{ createAlert()}  }>
                <Card 
                containerStyle={{paddingTop:0,paddingLeft:0,paddingRight:0,paddingBottom:5,borderRadius:5,marginTop:65}}>
                    <View style={styles.rowStyle}>
                        <View style={{flexDirection:'row',alignItems:"center"}}>
                            <Image source={require('../assets/logout.png')} style={[styles.imageStyle,{width:30,height:30,alignItems:"center"}]} resizeMode="contain" />
                            <Text style={[styles.titleText,{color:'red'}]}>Logout</Text>
                        </View>
                        <FontAwesome name={"angle-right"} size={25} style={{alignSelf:"flex-end"}}/>
                    </View>
                </Card>
                </TouchableOpacity>
                

            
        </LinearGradient>
        </SafeAreaView>
    )
}

const styles  = StyleSheet.create({
    container:{
        flex:1,
        paddingLeft:10,
        paddingTop:20,
        paddingRight:10,
        paddingBottom:20
    },
    nameText:{
       color:"black",
       fontSize:15,
       fontFamily:'roboto-bold'
    },
    emailText:{
        color:"grey",
        fontSize:12,
        fontFamily:'roboto-medium'

    },
    titleText:{
        color:"black",
        fontSize:14,
        fontFamily:'roboto-medium',
        marginLeft:20

    },
    rowStyle:{
      //  flex:1,
        width:"100%",
        flexDirection:"row",
       justifyContent:"space-between",
       alignContent:"center",
       alignItems:"center",
        paddingTop:15,
        paddingLeft:15,
        paddingRight:15,
        paddingBottom:15,
    },
    
    imageStyle:{
        width:60,height:60,borderRadius:60/2,
    },
    imageStyle1:{
        width:30,height:30,
    },
    btnView:{
        flexDirection:'row',
       
    },
    textStyle:{
        marginLeft:30,
        color:"black",
        fontSize:17,
    },
    viewLineStyle :{
        width:"90%",
        height:1,
        borderWidth:0.7,
        marginLeft:10,
        marginRight:10,
        backgroundColor:"#a9a9a9",
        borderColor:'#a9a9a9'
        
    },
    viewLine1:{
        width:"100%",height:0.4,borderWidth:0.4 ,borderColor:'#a9a9a9',
    },
    viewLine2:{
        width:"100%",height:0.5,borderWidth:0.5 ,borderColor:'#a9a9a9',
    },
    circularImage:{
       
        width:60,
        height:60,
        alignSelf:"center",
        alignItems:'center',
       justifyContent:"center",
       borderRadius:60/2,
       borderColor:COLORS.desp_grey,
       borderWidth:0.5,
       overflow:'hidden'
    },
})

export default CustomDrawer;