import React ,{useState} from 'react';
import {View ,Text, Image, TouchableOpacity,StyleSheet, BackHandler, Alert } from 'react-native';
import { Card } from 'react-native-elements';
import {COLORS} from '../Utils/Utils'
import AntDesign from 'react-native-vector-icons/AntDesign';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import ApiUrl from '../Utils/ApiUrl';
import  {updateCart} from '../redux/actions/home';
import { useDispatch,useSelector } from 'react-redux';
import {showMessage} from '../Utils/ShowMessage';
import  {getItemDetails,addToCart,removeAddress} from '../redux/actions/home';




const AddressComponent = (props) => {
    const [quantity, setQuantity] = useState(props.item.quantity);

    const dispatch = useDispatch();
    const userReducer = useSelector(state => state.user);

    const deleteAddress = async() =>{
         
          let formData = new FormData();
          formData.append("address_id",props.item.id);
          
          console.log(formData)
          dispatch(removeAddress(formData))
          .then(response=> {
              if(!response.error){
                  console.log(response)
                  showMessage('Address has been removed successfully',false)       
                  props.onDelete(props.item)                 
              }else  showMessage(response.message)
          })
          .catch(error => {
              showMessage(error.message)
          })
        
      }


const createAlert = (id) =>
      Alert.alert(
        "Delete",
        "Are you sure you want to remove this address? ",
        [
          {
            text: "Cancel",
            onPress: () => console.log("Cancel Pressed"),
            style: "cancel"
          },
          { text: "OK", onPress: () => deleteAddress() }
        ],
        { cancelable: false }
      );
  

       const editAddress = async() =>{
         props.onEdit(props.item) 
       }
   

    return (
            <Card containerStyle={styles.cardStyle}>
                <View style={{flexDirection:"row",width:"100%",alignItems:'center'}}>
                        <View style={{flex:0.9,padding:10}}>
                            <Text style={styles.productTitle}>{props.item.name}</Text>
                            <Text style={styles.productDesp}>{props.item.address}</Text>
                            {
                                props.item.apartment_unit_no ?
                                <Text style={styles.productDesp}>{props.item.apartment_unit_no}</Text>
                                : null

                            }
                            <Text style={styles.productDesp}>{props.item.city}</Text>
                            <Text style={styles.productDesp}>{props.item.state}</Text>
                            <Text style={styles.productDesp}>Postal Code : {props.item.postal_code}</Text>
                            <Text style={styles.productDesp}>Phone : {props.item.contact_number}</Text>
                           
                        </View>
                      
                        <View style={{flex:0.2,padding:10,height:'auto',justifyContent:'space-between'}}>
                            <TouchableOpacity onPress={()=> createAlert()}>
                            <MaterialCommunityIcons  name="delete-sweep" color="orange" size={30} />
                            </TouchableOpacity>

                            <TouchableOpacity onPress={()=> editAddress()}>
                            <MaterialCommunityIcons  name="account-edit" color="orange" size={30} />
                            </TouchableOpacity>
                           
                        </View>
                        
                        
                </View>
            </Card>
            
            )
       
            
       
            
    
}

const styles = StyleSheet.create({
    
    circularImage:{
       
        width:60,
        height:60,
        alignItems:'center',
       justifyContent:"center",
       borderRadius:120/2,
       borderColor:COLORS.theme_orange,
       borderWidth:2.5,
       padding:32,
       flex:0.02

    },
    imageStyle:{
        margin:5,alignItems:"center",alignSelf:"center",width:60,height:60,borderRadius:120/2,
    },
    cardStyle:{
        justifyContent:'space-between',
        padding:10,
        margin:0,
        elevation:4,
        marginBottom:10,
        marginLeft:5,
        marginRight:5,
        borderRadius:5,
    },
    productTitle:{
        fontFamily:'roboto-medium',
        color:"black",
        fontSize:17,
        lineHeight:20,
        marginTop:5,
    },
    productDesp:{
        width:"100%",
        fontFamily:'roboto-regular',
        color:COLORS.desp_grey,
        fontSize:12,
        lineHeight:20,
        marginTop:0

    },
    ViewMoreText:{
        color:COLORS.desp_grey,
        fontSize:13,
        fontFamily:'roboto-regular'
    },

    quantityText:{
        fontFamily:'roboto-bold',
        fontSize:15,
        color:'black'
    },
    

    
})

export default AddressComponent;