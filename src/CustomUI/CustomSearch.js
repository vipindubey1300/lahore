import React,{useEffect,useState} from 'react';
import { StyleSheet, View, TextInput, Image, TouchableOpacity } from 'react-native';
import { COLORS} from './../Utils/Utils';


const CustomSearch = (props) => {

    const [value, setValue] = useState('');

    useEffect(() => {
        console.log(props.editable)
        return () => {
           // cleanup
        }
    }, [])


    return(
       
        <View style={[styles.container,props.customContainer]}>
            <TextInput 
                autoFocus={props.focus ? props.focus : false}
                editable={(props.editable !== undefined)? props.editable : true}
                style ={styles.textInputStyle}
                placeholder="Search Here" 
                onChangeText= {(value) => {setValue(value)}}
                />

            <TouchableOpacity onPress={()=> props.onSearch(value)}
            style={styles.roundView}>
                <Image source={require('../assets/search.png')} style={{width:30,height:30,alignSelf:'center'}} />
            </TouchableOpacity>

        </View>
    )
}


CustomSearch.defaultProps = {
   onSearch :()=>{}
}
const styles = StyleSheet.create({

    container:{
        width:"100%",
        backgroundColor:COLORS.search_color,
        height:45,
        borderRadius:25,
        flexDirection:'row',

    },
    textInputStyle:{
        marginLeft:10,
        width:'78%',
        fontSize:15
    },
    roundView:{
        backgroundColor:COLORS.theme_orange,
        width:"19%",
        justifyContent:"center",
        alignItems:'center',
        borderRadius:25
    }

})
export default CustomSearch;