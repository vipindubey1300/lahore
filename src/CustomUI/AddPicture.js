import React, { useState } from 'react';
import {View ,Text , Image , TouchableOpacity, StyleSheet } from 'react-native';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import FontAwesome5Icon from 'react-native-vector-icons/FontAwesome5';


const AddPicture = (props) => {

   

    return(
        
        
            <View style={styles.container}>
                {!props.source 
                ?
                <Image source={require('../assets/avatar1.png')} style={{width:100, height:100,alignSelf:"center",borderRadius:100/2,}}  />
                :
                <Image source={props.source} style={{width:100, height:100,alignSelf:"center",borderRadius:100/2,}}  />
                }
              
                <FontAwesome5Icon name="pen" size={20} color="grey" style={{top:10,right:15}} />
            </View>
        
       
    )
}

const styles = StyleSheet.create({

    container:{
        borderRadius:100/2,
        borderWidth:1,
        borderColor:'black',
        width:100,
        height:100,
        flexDirection:"row",
    }
})

export default AddPicture;