import React  from 'react';
import { StyleSheet, View, Text, Image,TouchableOpacity,SafeAreaView } from 'react-native';
import { Avatar, Badge, Icon, withBadge } from 'react-native-elements'


const CustomHeader = (props) => {

    return(
        <SafeAreaView style={{backgroundColor:'white',}}>
       
        <View style={styles.container}>
            <TouchableOpacity 
            style={{flex:0.05,paddingLeft:15}} 
            onPress={() => props.navigation.openDrawer()}>
            <Image 
                style={{width:30,height:30,}}
                source={require('./../assets/menu.png')} 
            />
            </TouchableOpacity>
            <Text style={[styles.headerText,{marginLeft:50}]}>Home</Text>
            <TouchableOpacity 
            onPress={()=> {props.navigation.navigate('Notifications')}}
            style={{width:30,height:30,flex:0.1,marginRight:20}}>
            <Image 
                source={require('./../assets/bell.png')} 
                style={{width:30,height:30,paddingLeft:15,}} 
            />
            </TouchableOpacity>
            <TouchableOpacity 
            onPress={()=> {props.navigation.navigate('Cart')}}
            style={{width:30,height:30,flex:0.1,marginRight:20}}>
                <Image 
                    source={require('./../assets/cart.png')} 
                    style={{width:30,height:30,paddingLeft:15}} 
                />
                  <Badge
                    value={props.cartCount}
                    status="success"
                    containerStyle={{ position: 'absolute', top: -4, right: -4 }}
                />

                </TouchableOpacity>
                
            </View>
            </SafeAreaView>
        )
    }

    const styles = StyleSheet.create({
        container:{
            width:"100%",
            height:55,
            elevation:0,
            alignItems:'center',
            flexDirection:"row",
            backgroundColor:"white",
        justifyContent:"center"

    },
    headerText:{
        flex:0.8,
        alignSelf:"center",
        textAlign:'center',
        fontSize:20,
        fontFamily:'roboto-bold',
        color:'black',
    }
    
})

export default CustomHeader;