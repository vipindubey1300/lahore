import 'react-native-gesture-handler';
import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import {createDrawerNavigator } from '@react-navigation/drawer';
import CustomHeader from '../CustomUI/CustomHeader';
import HomeDrawerStack from './HomeDrawerStack';
import CustomDrawer from '../CustomUI/CustomDrawer';
const DrawerNavigator = (props) => {

    const Drawer = createDrawerNavigator();
    return(
        
            <Drawer.Navigator
             drawerContent={(props) => <CustomDrawer {...props} />}
             drawerType={'slide'}
             drawerStyle={{ width: '100%' }}
             overlayColor="transparent"
             
             >
                <Drawer.Screen 
                     component = {HomeDrawerStack}
                     name="Home"
                    //  options ={{
                    //      header : props => <CustomHeader  {...props}/>
                    //  }}

                />
                
            </Drawer.Navigator>
    
    )
}

export default DrawerNavigator;