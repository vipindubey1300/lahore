import React ,{useEffect, useState} from 'react';
import  AsyncStorage from  '@react-native-community/async-storage';
import SignedInStack from '../Navigators/SignedInStack';
import DrawerNavigator from './DrawerNavigator';
import ApiUrl from '../Utils/ApiUrl';
import {View ,ActivityIndicator,StyleSheet} from 'react-native';
import AuthContext from '../AuthContext/AuthContext'
import Axios from 'axios';
import {useSelector, useDispatch} from 'react-redux'


import * as types  from '../redux/types';
import Splash from '../Splash';

const AuthNavigation = (props) => {

    const [isSignedIn,setIsSignedIn]  = useState(null); 

    const [showSplash , setShowSplash ]  = useState(true);


     /**The equivalent of map dispatch to props is useDispatch */
     const dispatch =  useDispatch();
     /**The equivalent of map state to props is useSelector */
     const userReducer = useSelector(state => state.user)

     const checkAuthorization = async() => {
        const auth_token =  await AsyncStorage.getItem("token");
        const name =  await AsyncStorage.getItem("name");
        const email =  await AsyncStorage.getItem("email");
        const image =  await AsyncStorage.getItem("image");
        const id =  await AsyncStorage.getItem("id");

         console.log("Auth Token is -> ",JSON.stringify(userReducer));

        if(auth_token) {
            let obj = {
                'id':id,
                'email':email,
                'image':image,
                'token':auth_token,
                'name':name
            } 
            dispatch({ type:types.SAVE_USER_RESULTS,  user:obj })

            console.log("Auth Token is after -> ",JSON.stringify(userReducer));
            setIsSignedIn(auth_token)
           
        }
        setShowSplash(false);
   }

      //component did mount
      useEffect(()=> {
        console.log("User details--", userReducer)
        const timeout = setTimeout(async () => {
            checkAuthorization();
        },3000);

        //component will un-mount
        return () => {
            timeout;
        }
    },[]);



    

   

    if(showSplash){
         return <Splash />
    }


    // const [state, dispatch] = React.useReducer(
    //     (prevState, action) => {
    //       switch (action.type) {
    //         case 'RESTORE_TOKEN':
    //             console.log("restoring the token");
    //           return {
    //             ...prevState,
    //             userToken: action.token,
    //             isLoading: false,
    //           };
    //         case 'SIGN_IN':
    //           return {
    //             ...prevState,
    //             isSignout: false,
    //             userToken: action.token,
    //           };
    //         case 'SIGN_OUT':
    //           return {
    //             ...prevState,
    //             isSignout: true,
    //             userToken: null,
    //           };
    //         case 'SET_LOADING':
    //         return {
    //             ...prevState,
    //             isLoading:action.loading
    //         };
    //       }
    //     },
    //     {
    //       isLoading: false,
    //       isSignout: false,
    //       userToken: null,
    //     }
    //   );

   
    // useEffect(async()=> {

    //     //dispatch({ type: 'SET_LOADING', loading: true });
    //     const uid =  await AsyncStorage.getItem("id");
    //     console.log("hashcxhgvhbjcfxfxfxcvjbjkyvgjbxjvcs",uid);
    //    // dispatch({ type: 'RESTORE_TOKEN', token: uid });
    //     if(uid) {
    //      //   dispatch({ type: 'RESTORE_TOKEN', token: uid });
    //         setIsSignedIn(uid)
    //     }
        
    //     else{
    //         dispatch({ type: 'SET_LOADING', loading: false });
    //     }

    // },[]);


    // const authContext = React.useMemo(
    //     () => ({
    //       signIn: data => {
    //         // In a production app, we need to send some data (usually username, password) to server and get a token
    //         // We will also need to handle errors if sign in failed
    //         // After getting token, we need to persist the token using `AsyncStorage`
    //         // In the example, we'll use a dummy token;
    //         console.log("i am  calling sign in function ");
    //         dispatch({ type: 'SET_LOADING', loading: true });
    //         dispatch({ type: 'SIGN_IN', token: data });
    //         // Axios.post(ApiUrl.base_url + ApiUrl.login ,data)
    //         // .then(response => {
    //         //     dispatch({ type: 'SET_LOADING', loading: false });
    //         //     console.log("res",response.data);
    //         //     if(response.data.success){
    //         //         // AsyncStorage.setItem("id",response.data.hash);
    //         //         // AsyncStorage.setItem("name",response.data.name);
    //         //         //props.navigation.navigate('Home')
    //         //      //   dispatch({ type: 'SIGN_IN', token: response.data.hash });
                    
    //         //     }
              
    //         // }).catch(error => {
    //         //     dispatch({ type: 'SET_LOADING', loading: false });
    //         //     console.log("error",error);
    //         // })

    
           
    //       },
    //       signOut:   () =>{
    //            //AsyncStorage.clear(  );
    //           console.log("sign out")
    //           dispatch({ type: 'SIGN_OUT' })
    //     },
    //       signUp: data => {
    //         // In a production app, we need to send user data to server and get a token
    //         // We will also need to handle errors if sign up failed
    //         // After getting token, we need to persist the token using `AsyncStorage`
    //         // In the example, we'll use a dummy token

            
    //         dispatch({ type: 'SET_LOADING', loading: true });
    //         dispatch({ type: 'SIGN_IN', token: data});
    //       //  await AsyncStorage.setItem("id",data)
    //         // Axios.post(ApiUrl.base_url + ApiUrl.signup ,data)
    //         // .then(response => {
    //         //     dispatch({ type: 'SET_LOADING', loading: false });
    //         //     console.log("res",response.data);
    //         //     if(response.data.success){
    //         //         // AsyncStorage.setItem("id",response.data.hash);
    //         //         // AsyncStorage.setItem("name",response.data.name);
    //         //         //props.navigation.navigate('Home')
    //         //      //   dispatch({ type: 'SIGN_IN', token: response.data.hash });
                    
    //         //     }
              
    //         // }).catch(error => {
    //         //     dispatch({ type: 'SET_LOADING', loading: false });
    //         //     console.log("error",error);
    //         // })
    
    //       },
    //     }),
    //     []
    //   );
    
    //   // if(state.isLoading){
    //   //   return (<View
    //   //               style={[
    //   //               StyleSheet.absoluteFill,
    //   //               { backgroundColor: 'rgba(0, 0, 0, 0.7)', justifyContent: 'center' }
    //   //               ]}
    //   //               >
    //   //                   <ActivityIndicator color={"grey"} size={20} />
    //   //           </View>)
    //   // }




    // // return isSignedIn ? (
    // //      null
        
    // //   ) : (
    // //     <SignedInStack />
    // //    //<DrawerNavigator />
    // //   )

      return (
        // <AuthContext.Provider value={authContext}>
           userReducer.user_details == null ?
                (
                 <SignedInStack />
                 //  <DrawerNavigator />
                )
            :
                (
                    //<SignedInStack />
                    <DrawerNavigator />
                )
        
        // </AuthContext.Provider>
    )
}

export default AuthNavigation;