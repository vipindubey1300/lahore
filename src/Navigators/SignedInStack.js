import React from 'react';
import { NavigationContainer } from '@react-navigation/native'
import { createStackNavigator, StackView } from '@react-navigation/stack'
import Splash from '../Splash';
import Home from './../HomeComponents/Home';
import Login from '../Register/Loign';
import Registration from '../Register/Registration';
import ForgotPassword from '../Register/ForgotPassword';
import CustomHeader from '../CustomUI/CustomHeader';
import Categories from '../HomeComponents/Categories';
import CustomBackHeader from '../CustomUI/CustomBackHeader';
import ListingPage from '../HomeComponents/ListingPage';
import ZipCode from '../Register/ZipCode';
import OtpVerification from '../Register/OtpVerification';


import DrawerNavigator from './DrawerNavigator';
import ChangePassword from '../Register/ChangePassword';
const Stack = createStackNavigator()

const SignedInStack = (props) => {
    return(
       
            <Stack.Navigator initialRouteName="Login" >
            {/* <Stack.Screen 
                 name="Home"
                 component={Home}
                 options={{
                     header: props => <CustomHeader  {...props}/>
                 }}
                />
                 <Stack.Screen 
                 name="Categories"
                 component={Categories}
                 options={{
                     header: props => <CustomBackHeader {...props} title="Categories" />
                 }}
                />
                 <Stack.Screen 
                 name="ListingPage"
                 component={ListingPage}
                 options={{
                     header: props => <CustomBackHeader {...props} title="ListingPage" />
                 }}


                /> */}


                
                <Stack.Screen 
                options ={{
                    headerShown :false
                }}
                 name="Login"
                 component={Login}
                
                />
                <Stack.Screen 
                options ={{
                    headerShown :false
                }}
                 name="Home"
                 component={DrawerNavigator}
                
                />


                 <Stack.Screen 
                 name="Registration"
                 component={Registration}
                 options = {{
                     title:"Registration",
                    headerBackTitleVisible:false,
                    headerTintColor:'black'
                }}
                />
                 <Stack.Screen 
                 name="ForgotPassword"
                 component={ForgotPassword}
                 options = {{
                     title:"Forgot Password",
                    headerBackTitleVisible:false,
                    headerTintColor:'black'
                }}

                />


                <Stack.Screen 
                name="ChangePassword"
                component={ChangePassword}
                options = {{
                    title:"Change Password",
                   headerBackTitleVisible:false,
                   headerTintColor:'black'
               }}

               />

                <Stack.Screen 
                 name="ZipCode"
                 component={ZipCode}
                 options = {{
                    title:"Zip Code",
                    headerBackTitleVisible:false,
                    headerTintColor:'black'
                }}
                />

                <Stack.Screen 
                 name="OtpVerification"
                 component={OtpVerification}
                 options = {{
                     title:"OTP",
                    headerBackTitleVisible:false,
                    headerTintColor:'black'
                }}
                />

               
               
            </Stack.Navigator>
      
    )
}

export default SignedInStack;