import React, { useEffect } from 'react';
import { Text, Image, View ,StyleSheet,TouchableOpacity} from 'react-native';

import { NavigationContainer } from '@react-navigation/native'
import { createStackNavigator, StackView } from '@react-navigation/stack'


import Splash from '../Splash';
import Home from './../HomeComponents/Home';
import Login from '../Register/Loign';
import Registration from '../Register/Registration';
import ForgotPassword from '../Register/ForgotPassword';
import CustomHeader from '../CustomUI/CustomHeader';
import Categories from '../HomeComponents/Categories';
import CustomBackHeader from '../CustomUI/CustomBackHeader';
import ListingPage from '../HomeComponents/ListingPage';
import ZipCode from '../Register/ZipCode';
import DetailsPage from '../HomeComponents/DetailsPage';
import Cart from '../HomeComponents/Cart';
import Wishlist from '../HomeComponents/Wishlist';
import CustomerType from '../HomeComponents/CustomerType';
import CustomerCheckout from '../HomeComponents/CustomerCheckout';
import MyAccount from '../HomeComponents/MyAccount';
import ResetPassword from '../HomeComponents/ResetPassword';
import EditProfile from '../HomeComponents/EditProfile';
import OrderHistory from '../HomeComponents/OrderHistory';
import AboutUs from '../HomeComponents/AboutUs';
import ContactUs from '../HomeComponents/ContactUs';
import Faq from '../HomeComponents/Faq';
import { Avatar, Badge, Icon, withBadge } from 'react-native-elements'


const Stack = createStackNavigator()


import { useDispatch,useSelector } from 'react-redux';
import AllProducts from '../HomeComponents/AllProducts';
import AddAddress from '../HomeComponents/AddAddress';
import Address from '../HomeComponents/Address';
import Search from '../HomeComponents/Search';
import OrderPlaced from '../HomeComponents/OrderPlaced';
import PaymentWebview from '../HomeComponents/PaymentWebview';
import EditAddress from '../HomeComponents/EditAddress';
import Notifications from '../HomeComponents/Notifications';
import RateProduct from '../HomeComponents/RateProduct';
import Privacy from '../HomeComponents/Privacy';


const HeaderRight = (props)=>{
   const cart  = useSelector(state => state.cart)

   return(
      <View style={{flexDirection:'row'}}>
                     <TouchableOpacity 
                     onPress={()=> {props.navigation.navigate('Notifications')}}
                      style={{width:30,height:30,flex:0.1,marginRight:20}}>
                      <Image 
                          source={require('./../assets/bell.png')} 
                          style={{width:30,height:30,paddingLeft:10,}} 
                      />
                      </TouchableOpacity>
                      <TouchableOpacity 
                      onPress={()=> {props.navigation.navigate('Cart')}}
                      style={{width:30,height:30,marginRight:20}}>
                          <Image 
                              source={require('./../assets/cart.png')} 
                              style={{width:30,height:30,paddingLeft:0}} 
                          />
                          <Badge
                           value={cart.length}
                           status="success"
                           containerStyle={{ position: 'absolute', top: -4, right: -4 }}
                        />
                      </TouchableOpacity>
                     </View>
   )
   }


const HomeDrawerStack = (props) => {
   const common = useSelector(state => state.common);
   const userReducer = useSelector(state => state.user);
   const cartReducer = useSelector(state => state.cart);

   useEffect(()=>{
     // console.log('!!!!!!!',JSON.stringify(cartReducer))
   },[])

    return(
       
            <Stack.Navigator >
            <Stack.Screen 
                 name="Home"
                 component={Home}
                 options={{
                     header: props => <CustomHeader cartCount ={cartReducer.length}  {...props}/>
                 }}
                />


                <Stack.Screen 
                  name="Search"
                  component={Search}
                  options = {{
                     headerRight: () => <HeaderRight  {...props}/>,
                     title:"Search",
                     headerTintColor:'black',
                     headerBackTitleVisible:false,
                  }}
               />

               <Stack.Screen 
                  name="Payment"
                  component={PaymentWebview}
                  options = {{
                     title:"Payment",
                     headerTintColor:'black',
                     headerBackTitleVisible:false,
                  }}
               />


               <Stack.Screen 
                  name="OrderPlaced"
                  component={OrderPlaced}
                  options={{
                     headerShown:false
                  }}   
             />


               <Stack.Screen 
               name="AllProducts"
               component={AllProducts}
               options = {{
                  headerRight: () => <HeaderRight {...props}/>,
                  title:"All Products",
                  headerTintColor:'black',
                  headerBackTitleVisible:false,
               }}
            />


                 <Stack.Screen 
                 name="Categories"
                 component={Categories}
                 options = {{
                  headerRight: () => <HeaderRight  {...props}/>,
                  title:"Categories",
                  headerTintColor:'black',
                  headerBackTitleVisible:false,
               }}
                //  options={{
                //      header: props => <CustomBackHeader {...props} title="Categories" filter={0} />
                //  }}
                />
                 <Stack.Screen 
                 name="ListingPage"
                 component={ListingPage}
                 options = {{
                  headerRight: () => <HeaderRight  {...props}/>,
                  title:"Listing Page",
                  headerTintColor:'black',
                  headerBackTitleVisible:false,
               }}
                //  options={{
                //      header: props => <CustomBackHeader {...props} title="Listing Page"  filter={1} />
                //  }}
                />
                <Stack.Screen 
                 name="DetailsPage"
                 component={DetailsPage}
                 options = {{
                  headerRight: () => <HeaderRight {...props}/>,
                  title:"Details Page",
                  headerTintColor:'black',
                  headerBackTitleVisible:false,
               }}
                //  options={{
                //      header: props => <CustomBackHeader {...props} title="Details Page"  filter={0} />
                //  }}
                />
                <Stack.Screen 
                 name="Cart"
                 component={Cart}
                 options = {{
                  // headerRight: () => <HeaderRight/>,
                  title:"Cart",
                  headerTintColor:'black',
                  headerBackTitleVisible:false,
               }}
                //  options={{
                //      header: props => <CustomBackHeader {...props} title="Cart"  filter={0} />
                //  }}
                />

         <Stack.Screen 
                 name="Notifications"
                 component={Notifications}
                 options = {{
                  title:"Notifications",
                  headerTintColor:'black',
                  headerBackTitleVisible:false,
               }}
                //  options={{
                //      header: props => <CustomBackHeader {...props} title="Order History"  filter={1} />
                //  }}
                />


                <Stack.Screen 
                 name="Wishlist"
                 component={Wishlist}
                 options = {{
                  headerRight: () => <HeaderRight  {...props}/>,
                  title:"Wishlist",
                  headerTintColor:'black',
                  headerBackTitleVisible:false,
               }}
                //  options={{
                //      header: props => <CustomBackHeader {...props} title="Wishlist"  filter={0} />
                //  }}
                />
                <Stack.Screen 
                 name="CustomerType"
                 component={CustomerType}
                 options = {{
                    
                  title:"Customer Type",
                  headerBackTitleVisible:false,
                  headerTintColor:'black'
               }}
                //  options={{
                //      header: props => <CustomBackHeader {...props} title="Customer Type"  filter={0} />
                //  }}
                />
                 <Stack.Screen 
                 name="CustomerCheckout"
                 component={CustomerCheckout}
                 options = {{
                  title:"Checkout",
                  headerTintColor:'black',
                  headerBackTitleVisible:false,
               }}
                //  options={{
                //      header: props => <CustomBackHeader {...props} title="Checkout"  filter={1} />
                //  }}
                />
                 <Stack.Screen 
                 name="MyAccount"
                 component={MyAccount}
                 options = {{
                  title:"My Account",
                  headerTintColor:'black',
                  headerBackTitleVisible:false,
               }}
                //  options={{
                //      header: props => <CustomBackHeader {...props} title="Account"  filter={1} />
                //  }}
                />
                 <Stack.Screen 
                 name="ResetPassword"
                 component={ResetPassword}
                 options = {{
                  title:"Reset Password",
                  headerTintColor:'black',
                  headerBackTitleVisible:false,
               }}
                //  options={{
                //      header: props => <CustomBackHeader {...props} title="Reset Password"  filter={1} />
                //  }}
                />
                 <Stack.Screen 
                 name="EditProfile"
                 component={EditProfile}
                 options = {{
                  title:"Edit Profile",
                  headerTintColor:'black',
                  headerBackTitleVisible:false,
               }}
                //  options={{
                //      header: props => <CustomBackHeader {...props} title="Edit Profile"  filter={1} />
                //  }}
                />
                 <Stack.Screen 
                 name="OrderHistory"
                 component={OrderHistory}
                 options = {{
                  title:"Order History",
                  headerTintColor:'black',
                  headerBackTitleVisible:false,
               }}
                //  options={{
                //      header: props => <CustomBackHeader {...props} title="Order History"  filter={1} />
                //  }}
                />
               <Stack.Screen 
                 name="AboutUs"
                 component={AboutUs}
                 options = {{
                  title:"About Us",
                  headerTintColor:'black',
                  headerBackTitleVisible:false,
               }}
                //  options={{
                //      header: props => <CustomBackHeader {...props} title="Order History"  filter={1} />
                //  }}
                />

                <Stack.Screen 
                name="Faq"
                component={Faq}
                options = {{
                 title:"FAQ",
                 headerTintColor:'black',
                 headerBackTitleVisible:false,
              }}
               />


          <Stack.Screen 
                name="Privacy"
                component={Privacy}
                options = {{
                 title:"Privacy Policy",
                 headerTintColor:'black',
                 headerBackTitleVisible:false,
              }}
               />


         <Stack.Screen 
                name="RateProduct"
                component={RateProduct}
                options = {{
                 title:"Write a review",
                 headerTintColor:'black',
                 headerBackTitleVisible:false,
              }}
               />


                 <Stack.Screen 
                 name="ContactUs"
                 component={ContactUs}
                 options = {{
                  title:"Contact Us",
                  headerTintColor:'black',
                  headerBackTitleVisible:false,
               }}
                //  options={{
                //      header: props => <CustomBackHeader {...props} title="Order History"  filter={1} />
                //  }}
                />


                <Stack.Screen 
                name="Address"
                component={Address}
                options = {{
                 title:"Manage Address",
                 headerTintColor:'black',
                 headerBackTitleVisible:false,
              }}
               //  options={{
               //      header: props => <CustomBackHeader {...props} title="Reset Password"  filter={1} />
               //  }}
               />

               <Stack.Screen 
               name="AddAddress"
               component={AddAddress}
               options = {{
                title:"Add Address",
                headerTintColor:'black',
                headerBackTitleVisible:false,
             }}
              //  options={{
              //      header: props => <CustomBackHeader {...props} title="Reset Password"  filter={1} />
              //  }}
              />

            <Stack.Screen 
               name="EditAddress"
               component={EditAddress}
               options = {{
                title:"Edit Address",
                headerTintColor:'black',
                headerBackTitleVisible:false,
             }}
            />
               
            </Stack.Navigator>
       
    )
}

export default HomeDrawerStack;