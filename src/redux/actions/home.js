import * as types from '../types';
import Axios from 'axios';
import ApiUrl from '../../Utils/ApiUrl';

export const getHomeProducts = (user_id) => dispatch => 
    new Promise((resolve ,reject) => {
        let formData = new FormData();
        formData.append("user_id",user_id);


        dispatch({type: types.LOADING, isLoading:true });
        fetch(ApiUrl.base_url+ ApiUrl.home, {method: 'POST',  body: formData})
        .then((response) => response.json())
        .then( (responseJson) => {
            dispatch({ type: types.LOADING,isLoading:false });
            resolve(responseJson);
        })
        .catch((error) => {
            dispatch({type: types.LOADING, isLoading:false});
            reject(error);
        });
})


    export const getCategories = (user_id) => dispatch => 
    new Promise((resolve ,reject) => {
        dispatch({type: types.LOADING, isLoading:true });
        fetch(ApiUrl.base_url+ ApiUrl.category, {method: 'GET'})
        .then((response) => response.json())
        .then( (responseJson) => {
            dispatch({ type: types.LOADING,isLoading:false });
            resolve(responseJson);
        })
        .catch((error) => {
            dispatch({type: types.LOADING, isLoading:false});
            reject(error);
        });
      
    })


export const getCartItems = (formData) => dispatch => 
    new Promise((resolve ,reject) => {
        dispatch({type: types.LOADING, isLoading:true });
        fetch(ApiUrl.base_url+ ApiUrl.cart_listing, {method: 'POST',body:formData})
        .then((response) => response.json())
        .then( (responseJson) => {
            console.log('---',responseJson)
            dispatch({ type: types.LOADING,isLoading:false });
            resolve(responseJson);
        })
        .catch((error) => {
            dispatch({type: types.LOADING, isLoading:false});
            reject(error);
        });
      
})


    export const getCheckoutData = (formData) => dispatch => 
    new Promise((resolve ,reject) => {
        dispatch({type: types.LOADING, isLoading:true });
        fetch(ApiUrl.base_url+ ApiUrl.get_checkout_data, {method: 'POST',body:formData})
        .then((response) => response.json())
        .then( (responseJson) => {
            dispatch({ type: types.LOADING,isLoading:false });
            resolve(responseJson);
        })
        .catch((error) => {
            dispatch({type: types.LOADING, isLoading:false});
            reject(error);
        });
      
 })



 export const getDeliveryData = () => dispatch => 
 new Promise((resolve ,reject) => {
     dispatch({type: types.LOADING, isLoading:true });
     fetch(ApiUrl.base_url+ ApiUrl['get-delivery-slots'], {method: 'GET'})
     .then((response) => response.json())
     .then( (responseJson) => {
         dispatch({ type: types.LOADING,isLoading:false });
         resolve(responseJson);
     })
     .catch((error) => {
         dispatch({type: types.LOADING, isLoading:false});
         reject(error);
     });
   
})



    export const placeOrder = (formData) => dispatch => 
    new Promise((resolve ,reject) => {
        dispatch({type: types.LOADING, isLoading:true });
        fetch(ApiUrl.base_url+ ApiUrl.order_place, {method: 'POST',body:formData})
        .then((response) => response.json())
        .then( (responseJson) => {
            dispatch({ type: types.LOADING,isLoading:false });
            resolve(responseJson);
        })
        .catch((error) => {
            dispatch({type: types.LOADING, isLoading:false});
            reject(error);
        });
      
    })




export const orderSuccessApi = (formData) => dispatch => 
    new Promise((resolve ,reject) => {
        dispatch({type: types.LOADING, isLoading:true });
        fetch(ApiUrl.base_url+ ApiUrl['api-ordernotification-success'], {method: 'POST',body:formData})
        .then((response) => response.json())
        .then( (responseJson) => {
            dispatch({ type: types.LOADING,isLoading:false });
            resolve(responseJson);
        })
        .catch((error) => {
            dispatch({type: types.LOADING, isLoading:false});
            reject(error);
        });
      
    })

    export const getMyOrders = (formData) => dispatch => 
    new Promise((resolve ,reject) => {
        dispatch({type: types.LOADING, isLoading:true });
        fetch(ApiUrl.base_url+ ApiUrl.my_orders, {method: 'POST',body:formData})
        .then((response) => response.json())
        .then( (responseJson) => {
            dispatch({ type: types.LOADING,isLoading:false });
            resolve(responseJson);
        })
        .catch((error) => {
            dispatch({type: types.LOADING, isLoading:false});
            reject(error);
        });
      
    })


    export const getMyNotifications = (formData) => dispatch => 
    new Promise((resolve ,reject) => {
        dispatch({type: types.LOADING, isLoading:true });
        fetch(ApiUrl.base_url+ ApiUrl['get-notification'], {method: 'POST',body:formData})
        .then((response) => response.json())
        .then( (responseJson) => {
            dispatch({ type: types.LOADING,isLoading:false });
            resolve(responseJson);
        })
        .catch((error) => {
            dispatch({type: types.LOADING, isLoading:false});
            reject(error);
        });
      
    })





    export const getWishlistItems = (formData) => dispatch => 
    new Promise((resolve ,reject) => {
        dispatch({type: types.LOADING, isLoading:true });
        fetch(ApiUrl.base_url+ ApiUrl.whishlist_list, {method: 'POST',body:formData})
        .then((response) => response.json())
        .then( (responseJson) => {
            dispatch({ type: types.LOADING,isLoading:false });
            resolve(responseJson);
        })
        .catch((error) => {
            dispatch({type: types.LOADING, isLoading:false});
            reject(error);
        });
      
    })




    export const contactUs = (formData) => dispatch => 
    new Promise((resolve ,reject) => {
        dispatch({type: types.LOADING, isLoading:true });
        fetch(ApiUrl.base_url+ ApiUrl.contact_us, {method: 'POST',body:formData})
        .then((response) => response.json())
        .then( (responseJson) => {
            dispatch({ type: types.LOADING,isLoading:false });
            resolve(responseJson);
        })
        .catch((error) => {
            dispatch({type: types.LOADING, isLoading:false});
            reject(error);
        });
      
    })

    export const getAddress = (formData) => dispatch => 
    new Promise((resolve ,reject) => {
        dispatch({type: types.LOADING, isLoading:true });
        fetch(ApiUrl.base_url+ ApiUrl.address_listing, {method: 'POST',body:formData})
        .then((response) => response.json())
        .then( (responseJson) => {
            dispatch({ type: types.LOADING,isLoading:false });
            resolve(responseJson);
        })
        .catch((error) => {
            dispatch({type: types.LOADING, isLoading:false});
            reject(error);
        });
      
    })


    export const addAddress = (formData) => dispatch => 
    new Promise((resolve ,reject) => {
        dispatch({type: types.LOADING, isLoading:true });
        fetch(ApiUrl.base_url+ ApiUrl.add_address, {method: 'POST',body:formData})
        .then((response) => response.json())
        .then( (responseJson) => {
            dispatch({ type: types.LOADING,isLoading:false });
            resolve(responseJson);
        })
        .catch((error) => {
            dispatch({type: types.LOADING, isLoading:false});
            reject(error);
        });
      
    })




    export const removeAddress  = (formData) => dispatch => 
    new Promise((resolve , reject) => {
        dispatch({type: types.LOADING, isLoading:true });
        fetch(ApiUrl.base_url+ ApiUrl.remove_address, {method: 'POST',body:formData})
        .then((response) => response.json())
        .then( (responseJson) => {
            dispatch({ type: types.LOADING,isLoading:false });
            resolve(responseJson);
        })
        .catch((error) => {
            dispatch({type: types.LOADING, isLoading:false});
            reject(error);
        });

});

export const editAddress  = (formData) => dispatch => 
new Promise((resolve , reject) => {
    dispatch({type: types.LOADING, isLoading:true });
    fetch(ApiUrl.base_url+ ApiUrl.edit_address, {method: 'POST',body:formData})
    .then((response) => response.json())
    .then( (responseJson) => {
        dispatch({ type: types.LOADING,isLoading:false });
        resolve(responseJson);
    })
    .catch((error) => {
        dispatch({type: types.LOADING, isLoading:false});
        reject(error);
    });

});


export const searchProducts  = (formData) => dispatch => 
new Promise((resolve , reject) => {
    dispatch({type: types.LOADING, isLoading:true });
    fetch(ApiUrl.base_url+ ApiUrl.item_search, {method: 'POST',body:formData})
    .then((response) => response.json())
    .then( (responseJson) => {
        dispatch({ type: types.LOADING,isLoading:false });
        resolve(responseJson);
    })
    .catch((error) => {
        dispatch({type: types.LOADING, isLoading:false});
        reject(error);
    });

});


export const rateProduct  = (formData) => dispatch => 
new Promise((resolve , reject) => {
    dispatch({type: types.LOADING, isLoading:true });
    fetch(ApiUrl.base_url+ ApiUrl.rating, {method: 'POST',body:formData})
    .then((response) => response.json())
    .then( (responseJson) => {
        dispatch({ type: types.LOADING,isLoading:false });
        resolve(responseJson);
    })
    .catch((error) => {
        dispatch({type: types.LOADING, isLoading:false});
        reject(error);
    });

});


export const getFaq  = () => dispatch => 
new Promise((resolve , reject) => {
    dispatch({type: types.LOADING, isLoading:true });
    fetch(ApiUrl.base_url+ ApiUrl.faq, {method: 'GET'})
    .then((response) => response.json())
    .then( (responseJson) => {
        dispatch({ type: types.LOADING,isLoading:false });
        resolve(responseJson);
    })
    .catch((error) => {
        dispatch({type: types.LOADING, isLoading:false});
        reject(error);
    });

});


export const getPrivacy  = (formdata) => dispatch => 
new Promise((resolve , reject) => {
    dispatch({type: types.LOADING, isLoading:true });
    fetch(ApiUrl.base_url+ ApiUrl.pages, {method: 'POST',body:formdata})
    .then((response) => response.json())
    .then( (responseJson) => {
        dispatch({ type: types.LOADING,isLoading:false });
        resolve(responseJson);
    })
    .catch((error) => {
        dispatch({type: types.LOADING, isLoading:false});
        reject(error);
    });

});


export const getAboutUs  = () => dispatch => 
new Promise((resolve , reject) => {
    dispatch({type: types.LOADING, isLoading:true });
    fetch(ApiUrl.base_url+ ApiUrl.about_us, {method: 'GET'})
    .then((response) => response.json())
    .then( (responseJson) => {
        dispatch({ type: types.LOADING,isLoading:false });
        resolve(responseJson);
    })
    .catch((error) => {
        dispatch({type: types.LOADING, isLoading:false});
        reject(error);
    });

});

 export const getCategoryProducts  = (category) => dispatch => 
    new Promise((resolve , reject) => {
        let formdata = new FormData();
        formdata.append("category_id",category);
        

        dispatch({type: types.LOADING, isLoading:true });
        fetch(ApiUrl.base_url+ ApiUrl.category_product, {method: 'POST',body:formdata})
        .then((response) => response.json())
        .then( (responseJson) => {
            dispatch({ type: types.LOADING,isLoading:false });
            resolve(responseJson);
        })
        .catch((error) => {
            dispatch({type: types.LOADING, isLoading:false});
            reject(error);
        });

    });


    export const allProducts  = () => dispatch => 
    new Promise((resolve , reject) => {
        dispatch({type: types.LOADING, isLoading:true });
        fetch(ApiUrl.base_url+ ApiUrl.popular_products, {method: 'GET'})
        .then((response) => response.json())
        .then( (responseJson) => {
            dispatch({ type: types.LOADING,isLoading:false });
            resolve(responseJson);
        })
        .catch((error) => {
            dispatch({type: types.LOADING, isLoading:false});
            reject(error);
        });

    });


    export const categoryProducts = (category_product_id) => dispatch => 
    new Promise((resolve ,reject) => {

        dispatch({
            type:types.LOADING,
            isLoading: true
        })

        Axios.get(ApiUrl.baseurl + ApiUrl.category_product+category_product_id)
            .then(response => {

                dispatch({
                    type:types.LOADING,
                    isLoading: false
                })
                
                if(response.data.result.status){

                    resolve(response.data.result);
                }else{

                }

            })
            .catch(error => {

                dispatch({
                    type:types.LOADING,
                    isLoading:false,
                })
                reject(error);

        })

    })



    // single details of product

export const getItemDetails  = (product_id,user_id) => dispatch => 
    new Promise((resolve , reject) => {
        let formdata = new FormData();
        formdata.append("item_id",product_id);
        formdata.append("user_id",user_id);

        dispatch({type: types.LOADING, isLoading:true });
        fetch(ApiUrl.base_url+ ApiUrl.details, {method: 'POST',body:formdata})
        .then((response) => response.json())
        .then( (responseJson) => {
            dispatch({ type: types.LOADING,isLoading:false });
            resolve(responseJson);
        })
        .catch((error) => {
            dispatch({type: types.LOADING, isLoading:false});
            reject(error);
        });

});



export const addToCart  = (formData) => dispatch => 
    new Promise((resolve , reject) => {
        dispatch({type: types.LOADING, isLoading:true });
        fetch(ApiUrl.base_url+ ApiUrl.add_to_cart, {method: 'POST',body:formData})
        .then((response) => response.json())
        .then( (responseJson) => {
            dispatch({ type: types.LOADING,isLoading:false });
            resolve(responseJson);
        })
        .catch((error) => {
            dispatch({type: types.LOADING, isLoading:false});
            reject(error);
        });

});


export const addToWishlist  = (formData) => dispatch => 
    new Promise((resolve , reject) => {
       dispatch({type: types.LOADING, isLoading:true });
        fetch(ApiUrl.base_url+ ApiUrl.whishlist, {method: 'POST',body:formData})
        .then((response) => response.json())
        .then( (responseJson) => {
            dispatch({ type: types.LOADING,isLoading:false });
            resolve(responseJson);
        })
        .catch((error) => {
            dispatch({type: types.LOADING, isLoading:false});
            reject(error);
        });

});




export const removeFromCart  = (formData) => dispatch => 
    new Promise((resolve , reject) => {
       dispatch({type: types.LOADING, isLoading:true });
        fetch(ApiUrl.base_url+ ApiUrl.cart_remove_product, {method: 'POST',body:formData})
        .then((response) => response.json())
        .then( (responseJson) => {
            dispatch({ type: types.LOADING,isLoading:false });
            resolve(responseJson);
        })
        .catch((error) => {
            dispatch({type: types.LOADING, isLoading:false});
            reject(error);
        });

});

export const cancelOrder  = (formData) => dispatch => 
    new Promise((resolve , reject) => {
        dispatch({type: types.LOADING, isLoading:true });
        fetch(ApiUrl.base_url+ ApiUrl.cancel_order, {method: 'POST',body:formData})
        .then((response) => response.json())
        .then( (responseJson) => {
            dispatch({ type: types.LOADING,isLoading:false });
            resolve(responseJson);
        })
        .catch((error) => {
            dispatch({type: types.LOADING, isLoading:false});
            reject(error);
        });

});


export const reorder  = (formData) => dispatch => 
    new Promise((resolve , reject) => {
        dispatch({type: types.LOADING, isLoading:true });
        fetch(ApiUrl.base_url+ ApiUrl.reorder, {method: 'POST',body:formData})
        .then((response) => response.json())
        .then( (responseJson) => {
            dispatch({ type: types.LOADING,isLoading:false });
            resolve(responseJson);
        })
        .catch((error) => {
            dispatch({type: types.LOADING, isLoading:false});
            reject(error);
        });

});


export const updateCart  = (formData) => dispatch => 
    new Promise((resolve , reject) => {
        dispatch({type: types.LOADING, isLoading:true });
        fetch(ApiUrl.base_url+ ApiUrl.cart_update, {method: 'POST',body:formData})
        .then((response) => response.json())
        .then( (responseJson) => {
            dispatch({ type: types.LOADING,isLoading:false });
            resolve(responseJson);
        })
        .catch((error) => {
            dispatch({type: types.LOADING, isLoading:false});
            reject(error);
        });

});


    // add to cart
export const submitAddToCart  = (product_id,quantity,size_id,color_id,seesion_id) => dispatch => 
new Promise((resolve , reject) => {

    dispatch({
        type:types.LOADING,
        isLoading: true
    })

    let formData = new FormData();
    formData.append("product_id",product_id);
    formData.append("quantity",quantity);
    formData.append("size_id",size_id);
    formData.append("color_id",color_id);
    formData.append("sess_id",seesion_id);
    console.log("formsdsd",formData);
    Axios.post(ApiUrl.baseurl + ApiUrl.add_to_cart,formData)
        .then(response => {

          
            dispatch({
                type:types.LOADING,
                isLoading: false
            })
            
            console.log("new cart",response.data);
            if(response.data.result.status){

                resolve(response.data.result);
            }else{
                resolve(response.data.result);
            }

        })
        .catch(error => {

            console.log("add",error);

            dispatch({
                type:types.LOADING,
                isLoading:false,
            })
            reject(error);

    })


});


// get cart
export const getCartData  = (seesion_id) => dispatch => 
new Promise((resolve , reject) => {

    dispatch({
        type:types.LOADING,
        isLoading: true
    })

    console.log(seesion_id);
    let formdata = new FormData();
    formdata.append("sess_id",seesion_id);
    Axios.post(ApiUrl.baseurl + ApiUrl.get_cart_data,formdata)
        .then(response => {

            
            dispatch({
                type:types.LOADING,
                isLoading: false
            })
            console.log("check cart data1",response.data)
            if(response.data.result.status){

                resolve(response.data.result);
            }else{

            }

        })
        .catch(error => {

            console.log("chk error",error);

            dispatch({
                type:types.LOADING,
                isLoading:false,
            })
            reject(error);

    })


});


 
// remove completely from cart
export const updateCartData  = (seesion_id,cart_id,product_id,quantity) => dispatch => 
new Promise((resolve , reject) => {

    dispatch({
        type:types.LOADING,
        isLoading: true
    })

    console.log(seesion_id);

    let formData = new FormData();
    formData.append("sess_id",seesion_id);
    formData.append("product_id",product_id);
    formData.append("color_id","");
    formData.append("size_id","");
    formData.append("cart_id",cart_id);
    formData.append("quantity",quantity);

    console.log("for,",formData);
    Axios.post(ApiUrl.baseurl + ApiUrl.update_to_cart,formData)
        .then(response => {

            
            dispatch({
                type:types.LOADING,
                isLoading: false
            })
            console.log("check cart upadte data",response.data)
            if(response.data.result.status){

                resolve(response.data.result);
            }else{
                resolve(response.data.result);
            }

        })
        .catch(error => {

            console.log("chk error",error);

            dispatch({
                type:types.LOADING,
                isLoading:false,
            })
            reject(error);

    })


});  


// remove completely from cart
export const removeCompletelyFromCart  = (seesion_id,cart_id) => dispatch => 
new Promise((resolve , reject) => {

    dispatch({
        type:types.LOADING,
        isLoading: true
    })

    console.log(seesion_id);

    let formData = new FormData();
    formData.append("sess_id",seesion_id);
    formData.append("cart_id",cart_id);

    console.log("for,",formData);
    Axios.post(ApiUrl.baseurl + ApiUrl.removeFromCart,formData)
        .then(response => {

            
            dispatch({
                type:types.LOADING,
                isLoading: false
            })
            console.log("check cart data1",response.data)
            if(response.data.result.status){

                resolve(response.data.result);
            }else{

            }

        })
        .catch(error => {

            console.log("chk error",error);

            dispatch({
                type:types.LOADING,
                isLoading:false,
            })
            reject(error);

    })


});







// add to wishlist / saved Item
export const removeFromWhislist  = (seesion_id,wishlist_id) => dispatch => 
new Promise((resolve , reject) => {

    dispatch({
        type:types.LOADING,
        isLoading: true
    })

    console.log(seesion_id);

    let formData = new FormData();
    formData.append("sess_id",seesion_id);
    formData.append("id",wishlist_id);

    console.log("for,",formData);
    Axios.post(ApiUrl.baseurl + ApiUrl.remove_from_whishlist,formData)
        .then(response => {

            
            dispatch({
                type:types.LOADING,
                isLoading: false
            })
            console.log("check remobe whish list",response.data)
            if(response.data.result.status){

                resolve(response.data.result);
            }else{

            }

        })
        .catch(error => {

            console.log("chk error",error);

            dispatch({
                type:types.LOADING,
                isLoading:false,
            })
            reject(error);

    })


});


// get Saved Items/Recently Viewed/Recently Searched
//1=Saved,2=Recently Viewed,3=Recently Search



// add to wishlist / saved Item
export const getSavedOrRecentViewOrSearched  = (seesion_id,type) => dispatch => 
new Promise((resolve , reject) => {

    dispatch({
        type:types.LOADING,
        isLoading: true
    })

    console.log(seesion_id);

    let formData = new FormData();
    formData.append("sess_id",seesion_id);
    formData.append("type",type);

    console.log("for,",formData);
    Axios.post(ApiUrl.baseurl + ApiUrl.saved_history,formData)
        .then(response => {

            
            dispatch({
                type:types.LOADING,
                isLoading: false
            })
            console.log("check save whish list",response.data)
            if(response.data.result.status){

                resolve(response.data.result);
            }else{
                resolve(response.data.result);
            }

        })
        .catch(error => {

            console.log("chk errorvgvfhf",error);

            dispatch({
                type:types.LOADING,
                isLoading:false,
            })
            reject(error);

    })


});