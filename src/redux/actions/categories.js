import * as types from '../types';
import Axios from 'axios';
import ApiUrl from '../../utils/ApiUrl';


export const getCategories = ( ) =>dispatch => 
    new Promise((resolve, reject) => {

        console.log("hi")
        dispatch({
            type:types.LOADING,
            isLoading:true ,

        });

        Axios.get(ApiUrl.baseurl+ApiUrl.product_categories)
            .then(response => {

                dispatch({
                    type:types.LOADING,
                    isLoading:false ,
        
                });

                if(response.data.result.status){

                    resolve(response);
                }

            })
            .catch(error => {
                dispatch({
                    type:types.LOADING,
                    isLoading:false ,
        
                });

                reject(error);

            });


    })




export const getDeals = ( ) =>dispatch => 
new Promise((resolve, reject) => {

    dispatch({
        type:types.LOADING,
        isLoading:true ,

    });

    Axios.get(ApiUrl.baseurl+ApiUrl.deals)
        .then(response => {
            dispatch({
                type:types.LOADING,
                isLoading:false ,
    
            });


            if(response.data.result.status){

                resolve(response);
            }

        })
        .catch(error => {
            dispatch({
                type:types.LOADING,
                isLoading:false ,
    
            });


            reject(error);

        });


})