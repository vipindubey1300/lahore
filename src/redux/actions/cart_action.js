import { ADD_CART, CART_COUNT, EMPTY_CART, REMOVE_CART } from "../types";

export function updateCartCount(cartCount) {
    return {
      type: CART_COUNT,
      payload: cartCount,
    };
  }



  export function addCartItem(cartCount) {
    return {
      type: ADD_CART,
      payload: cartCount,
    };
  }

  export function removeCartItem(cartCount) {
    return {
      type: REMOVE_CART,
      payload: cartCount,
    };
  }


  export function emptyCart({}) {
    return {
      type: EMPTY_CART,
      payload: {},
    };
  }