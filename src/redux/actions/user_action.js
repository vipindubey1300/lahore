import * as types  from '../types';
import Axios from 'axios';
import ApiUrl from '../../Utils/ApiUrl';
import { Platform } from 'react-native';

export const submitAccount = (formdata) => dispatch => 
    new Promise((resolve , reject) => {

        dispatch({
            type: types.LOADING,
            isLoading:true
        });

        console.log("form",formdata);
        
        Axios.post(ApiUrl.base_url+ApiUrl.signup,formdata)
            .then(response => {

                dispatch({
                    type: types.LOADING,
                    isLoading:false
                });

              
                console.log("response user",response.data);
                if(!response.data.error){
                    // dispatch({
                    //     type:types.SAVE_USER_RESULTS,
                    //     user:response.data.result
                    // })
                }

                resolve(response)

            })
            .catch(error => {
                console.log("err",error);

                dispatch({
                    type: types.LOADING,
                    isLoading:false
                });

                reject(error)

            })

    })




export const submitVerifyOtp = (formdata) => dispatch => 
    new Promise((resolve ,reject) =>{
        dispatch({
            type: types.LOADING,
            isLoading:true
        });
        Axios.post(ApiUrl.base_url+ApiUrl.verify_otp,formdata)
            .then(response => {
                console.log("OTP VEIFY RESPONSE",response)
                dispatch({
                    type: types.LOADING,
                    isLoading:false
                });

                console.log("hi1",response.data);

                if(!response.data.error){
                    console.log("hi")

                    // dispatch({
                    //     type:types.SAVE_USER_RESULTS,
                    //     user:response.data.result
                    // })

                }

                resolve(response);

            })
            .catch(error => {

                dispatch({
                    type: types.LOADING,
                    isLoading:false
                });

                reject(error);
            })

    })


export const submitResendOtpOld = (formdata) => dispatch => 
    new Promise((resolve ,reject) =>{
        dispatch({
            type: types.LOADING,
            isLoading:true
        });
        Axios.post(ApiUrl.base_url+ApiUrl.resend_verify_otp,formdata)
            .then(response => {
                console.log("OTP VEIFY RESPONSE",response)
                dispatch({
                    type: types.LOADING,
                    isLoading:false
                });

                console.log("hi1",response.data);
                resolve(response);

            })
            .catch(error => {

                dispatch({
                    type: types.LOADING,
                    isLoading:false
                });

                reject(error);
            })

    }) 




    export const submitResendOtp = (formdata) => dispatch => 
    new Promise((resolve ,reject) =>{
        dispatch({type: types.LOADING, isLoading:true });
        fetch(ApiUrl.base_url+ ApiUrl.resend_verify_otp, {method: 'POST',  body: formdata})
        .then((response) => response.json())
        .then( (responseJson) => {
            console.log("hi1",responseJson);
            dispatch({ type: types.LOADING,isLoading:false });
            resolve(responseJson);
        })
        .catch((error) => {
            dispatch({type: types.LOADING, isLoading:false});
            reject(error);
        });
    }) 

 export const submitLogin = (formdata) => dispatch => 
    new Promise((resolve ,reject) =>{
        dispatch({type: types.LOADING, isLoading:true });
         
        fetch(ApiUrl.base_url+ ApiUrl.login, {method: 'POST',  body: formdata})
        .then((response) => response.json())
        .then( (responseJson) => {
           // console.log("hi1",responseJson);
            dispatch({ type: types.LOADING,isLoading:false });
            if(!responseJson.error){
                let obj = {
                    'id':responseJson.result.id,
                    'email':responseJson.result.email,
                    'image':responseJson.result.user_image,
                    'token':responseJson.result.auth_token,
                    'name':responseJson.result.name
                } 
                dispatch({ type:types.SAVE_USER_RESULTS,  user:obj })
            }
           
            resolve(responseJson);
            
        })
        .catch((error) => {
            dispatch({type: types.LOADING, isLoading:false});
            reject(error);
        });
/**

        Axios.post(ApiUrl.base_url+ ApiUrl.login,formdata)
            .then(response => {

                dispatch({
                    type: types.LOADING,
                    isLoading:false
                });
            });
            if(response.data.status){
                console.log("hi User")
                dispatch({
                    type:types.SAVE_USER_RESULTS,
                    user:response.data.result
                })

            }
                resolve(response);

            })
            .catch(error => {

                dispatch({
                    type: types.LOADING,
                    isLoading:false
                });

                reject(error);
            })

 */


})



export const forgotPassword = (formdata) => dispatch => 
    new Promise((resolve ,reject) =>{
        dispatch({type: types.LOADING, isLoading:true });
        fetch(ApiUrl.base_url+ ApiUrl.forgot, {method: 'POST',  body: formdata})
        .then((response) => response.json())
        .then( (responseJson) => {
            console.log("hi1",responseJson);
            dispatch({ type: types.LOADING,isLoading:false });
            resolve(responseJson);
        })
        .catch((error) => {
            dispatch({type: types.LOADING, isLoading:false});
            reject(error);
        });
/**

        Axios.post(ApiUrl.base_url+ ApiUrl.login,formdata)
            .then(response => {

                dispatch({
                    type: types.LOADING,
                    isLoading:false
                });
            });
            if(response.data.status){
                console.log("hi User")
                dispatch({
                    type:types.SAVE_USER_RESULTS,
                    user:response.data.result
                })

            }
                resolve(response);

            })
            .catch(error => {

                dispatch({
                    type: types.LOADING,
                    isLoading:false
                });

                reject(error);
            })

 */


})



export const changePassword = (formdata) => dispatch => 
    new Promise((resolve ,reject) =>{
        dispatch({type: types.LOADING, isLoading:true });
        fetch(ApiUrl.base_url+ ApiUrl.change_password, {method: 'POST',  body: formdata})
        .then((response) => response.json())
        .then( (responseJson) => {
            dispatch({ type: types.LOADING,isLoading:false });
            resolve(responseJson);
        })
        .catch((error) => {
            dispatch({type: types.LOADING, isLoading:false});
            reject(error);
        });
})




export const resetPassword = (formdata) => dispatch => 
    new Promise((resolve ,reject) =>{
        dispatch({type: types.LOADING, isLoading:true });
        fetch(ApiUrl.base_url+ ApiUrl.reset_password, {method: 'POST',  body: formdata})
        .then((response) => response.json())
        .then( (responseJson) => {
            dispatch({ type: types.LOADING,isLoading:false });
            resolve(responseJson);
        })
        .catch((error) => {
            dispatch({type: types.LOADING, isLoading:false});
            reject(error);
        });
})




export const addZipCode = (formdata) => dispatch => 

new Promise((resolve ,reject) =>{
    console.log("hi1",formdata);

    dispatch({type: types.LOADING, isLoading:true });
    fetch(ApiUrl.base_url+ ApiUrl.zip, {method: 'POST',  body: formdata})
    .then((response) => response.json())
    .then( (responseJson) => {
        console.log("hi1",responseJson);
        dispatch({ type: types.LOADING,isLoading:false });
        resolve(responseJson);
         
        if(!responseJson.error){
            let obj = {
                'id':responseJson.result.id,
                'email':responseJson.result.email,
                'image':responseJson.result.user_image,
                'token':responseJson.result.auth_token,
                'name':responseJson.result.name
            } 
            dispatch({ type:types.SAVE_USER_RESULTS,  user:obj })
        }
       
    })
    .catch((error) => {
        dispatch({type: types.LOADING, isLoading:false});
        reject(error);
    });
/**

    Axios.post(ApiUrl.base_url+ ApiUrl.login,formdata)
        .then(response => {

            dispatch({
                type: types.LOADING,
                isLoading:false
            });
        });
        if(response.data.status){
            console.log("hi User")
            dispatch({
                type:types.SAVE_USER_RESULTS,
                user:response.data.result
            })

        }
            resolve(response);

        })
        .catch(error => {

            dispatch({
                type: types.LOADING,
                isLoading:false
            });

            reject(error);
        })

*/


})




export const getProfile  = (user_id) => dispatch => 
new Promise((resolve , reject) => {
    let formdata = new FormData();
    formdata.append("user_id",user_id);
    dispatch({type: types.LOADING, isLoading:true });
    fetch(ApiUrl.base_url+ ApiUrl.get_profile, {method: 'POST',body:formdata})
    .then((response) => response.json())
    .then( (responseJson) => {
        dispatch({ type: types.LOADING,isLoading:false });
        resolve(responseJson);
    })
    .catch((error) => {
        dispatch({type: types.LOADING, isLoading:false});
        reject(error);
    });

});



export const editProfile  = (formdata) => dispatch => 
new Promise((resolve , reject) => {
    dispatch({type: types.LOADING, isLoading:true });
    fetch(ApiUrl.base_url+ ApiUrl.update_profile, {method: 'POST',body:formdata})
    .then((response) => response.json())
    .then( (responseJson) => {
        dispatch({ type: types.LOADING,isLoading:false });
        resolve(responseJson);
    })
    .catch((error) => {
        dispatch({type: types.LOADING, isLoading:false});
        reject(error);
    });

});


export const getAsyncStorage = (user) => {

    return {
        type:types.SAVE_USER_RESULTS,
        user:user
    }

}

export const onLogoutUser = () => {

    return {
        type:types.LOGOUT_USER,
        user:null
    }
}