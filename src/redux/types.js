export const HOME = "HOME" ;

export const LOADING = "LOADING";

export const CREATE_ACCOUNT = "CREATE_ACCOUNT";

export const RESPONSE_ERROR = "RESPONSE_ERROR";

export const SAVE_USER_RESULTS ="SAVE_USER_RESULTS";

export const LOGOUT_USER = "LOGOUT_USER";

export const GET_CATEGORIES = "GET_CATEGORIES" ;

export const UPDATE_USER = "UPDATE_USER";

export const CART_COUNT = "CART_COUNT";


export const ADD_CART = "ADD_CART";

export const REMOVE_CART = "REMOVE_CART";

export const EMPTY_CART = "EMPTY_CART";