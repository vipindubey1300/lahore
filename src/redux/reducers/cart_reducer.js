import * as types  from '../types';

// const initialState = {
//   cartCount: 0,
// };

// const cartReducer = (state = initialState, action) => {
//   switch (action.type) {
//     case types.CART_COUNT:
//       return {
//         ...state,
//         cartCount: action.payload,
//       };
//     default:
//       return state;
//   }
// };

const initialState = {
  cartItems: [],
};


const cartItemExists = (carts, cart) => {
    return carts.some((e) => e.id === cart.id);
}


const cartReducer = (state = [], action) => {
    switch (action.type) {
        case types.ADD_CART:
            console.log('-----',cartItemExists(state, action.payload))
            if (cartItemExists(state, action.payload)) {
                return state;
            } else {
                return [...state, action.payload];
            }

        case types.REMOVE_CART:  
            const id = action.payload.id  
            console.log('---->>>') 
            console.log(id) 
            console.log(state) 
            console.log(state.filter((item) => item.id !== id)) 
            return state.filter((item) => item.id !== id)
            
        
        case types.EMPTY_CART:  
             return []
        default:
            return state;
    }
};
 

export default cartReducer;