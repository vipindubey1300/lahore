import * as types  from '../types';
import  AsyncStorage from  '@react-native-community/async-storage';



const initialState = {

    user_details:null

}


export default (state = initialState ,action) => {

    switch(action.type){

        case types.SAVE_USER_RESULTS :
            let user_details_action = action.user;
           // console.log("Saving user details in redux--->",user_details_action);
            if(user_details_action !== null){
                 AsyncStorage.setItem('id',user_details_action.id.toString())
                 AsyncStorage.setItem('token',user_details_action.token)
                 AsyncStorage.setItem('name',user_details_action.name)
                 AsyncStorage.setItem('email',user_details_action.email)
                 AsyncStorage.setItem('image',user_details_action.image)
                return{
                    ...state ,
                    user_details : {...user_details_action},
                }
            }else{
                return{
                    ...state ,
                    user_details : null,
                }
            }

          
        case types.LOGOUT_USER :
            return {
                ...state,
                user_details :null
            }


     case types.UPDATE_USER:
      console.log('updating,',action.user)
          return {
              ...state,
              user_details: {
              ...state.user_details,
              ...action.user
              }
          };

           default :
           return state;
   

    }




}


