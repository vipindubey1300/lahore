import React ,{useState , useEffect} from 'react';
import {View , Text, TouchableOacity, Image, StyleSheet ,FlatList,TouchableOpacity} from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { Card } from 'react-native-elements';
import AntDesign from 'react-native-vector-icons/AntDesign';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'
import { COLORS } from '../Utils/Utils';
import  {getCartItems, getMyOrders, reorder} from '../redux/actions/home';
import {showMessage} from '../Utils/ShowMessage';
import { useDispatch,useSelector } from 'react-redux';
import ApiUrl from '../Utils/ApiUrl';
import  {updateCart} from '../redux/actions/home';
import  {cancelOrder} from '../redux/actions/home';
import ProgressBar from '../Utils/ProgressBar';
import { addCartItem } from '../redux/actions/cart_action';


const OrderHistory = (props) => {

    const [orderHistory , setOrderHistory] = useState([
       
    ]);


    function cancel(id){
         
        let formData = new FormData();
        formData.append("order_id",id);
        formData.append("user_id",userReducer.user_details.id);
        console.log(formData)
        dispatch(cancelOrder(formData))
        .then(response=> {
            if(!response.error){
                var temp = orderHistory.filter(i => i.id != id)
                setOrderHistory(temp) 
                                
            }else  showMessage(response.message)
        })
        .catch(error => {
            showMessage('Something went wrong,Try Again')
        })
      
    }

    function fetchCartCount(){
        let formData = new FormData();
        formData.append("single",2);
        formData.append("user_id",userReducer.user_details.id);
        dispatch(getCartItems(formData))
        .then(response=> {
           // console.log('response of cart count---',JSON.stringify(response))
            if(!response.error){
                let items = []
                //response.result.map(e => items.push({'id':e.cartitem.id}))
                response.result.map(e =>  dispatch(addCartItem({'id':e.cartitem.id})))
                //console.log(items)
               
            }else{
                showMessage(response.message)
                
            }
        })
        .catch(error => {
            console.log(error.message)
            showMessage('Something went wrong,Try Again')
        })

    }

    function reOrder(item){
         
        let formData = new FormData();
        formData.append("order_no",item.order_number);
        formData.append("user_id",userReducer.user_details.id);
        console.log(formData)
        dispatch(reorder(formData))
        .then(response=> {
            if(!response.error){
                fetchCartCount()
                props.navigation.navigate('Cart')
                                
            }else  showMessage(response.message)
        })
        .catch(error => {
            showMessage('Something went wrong,Try Again')
        })
      
    }


    function rate(id){
        props.navigation.navigate("RateProduct",{id:id})
    }

    function getStatus(item){
    /**
         * Status Code 
         *  0: Waiting for confirmation; 
         *  1. Processing;
         *  2.Out for delivery; 
         *  3. Delivered;
         *  4: Order Cancel,
        */
        let status = item.status
         if(status == 0 ){
             return(
                <TouchableOpacity  onPress={()=> cancel(item.id)}
                style={{paddingTop:7,paddingBottom:7,paddingLeft:10,paddingRight:10,backgroundColor:'red',borderRadius:5}}>
                    <Text style={{color:'white',fontSize:12}}>Cancel</Text>
                </TouchableOpacity>
             )
         }
         else if(status == 1){
            return(
                <TouchableOpacity  onPress={()=> cancel(item.id)}
                style={{paddingTop:7,paddingBottom:7,paddingLeft:10,paddingRight:10,backgroundColor:'#F17D3A',borderRadius:5}}>
                   <Text style={{color:'white',fontSize:12}}>Cancel </Text>
                </TouchableOpacity>
             )
         }
         else if(status == 2){
            return(
                <TouchableOpacity style={{paddingTop:7,paddingBottom:7,paddingLeft:10,paddingRight:10,backgroundColor:'black',borderRadius:5}}>
                <Text style={{color:'white',fontSize:12}}>Out for Delivery</Text>
            </TouchableOpacity>
             )
         }
         else if(status == 3){
            return(
                <View style={{alignItems:'center'}}>
                     <TouchableOpacity style={{paddingTop:7,paddingBottom:7,paddingLeft:10,paddingRight:10,backgroundColor:'black',borderRadius:5,width:90,justifyContent:'center',alignItems:'center'}}>
                        <Text style={{color:'white',fontSize:12}}>Delivered</Text>
                    </TouchableOpacity>
                    <View style={{height:10}}/>
                    <TouchableOpacity onPress={()=> rate(item.item_id)}
                    style={{paddingTop:7,paddingBottom:7,paddingLeft:10,paddingRight:10,backgroundColor:'orange',borderRadius:5,width:90,justifyContent:'center',alignItems:'center'}}>
                        <Text style={{color:'white',fontSize:12}}>Rate</Text>
                    </TouchableOpacity>
                    <View style={{height:10}}/>
                    <TouchableOpacity  onPress={()=> reOrder(item)}
                    style={{paddingTop:7,paddingBottom:7,paddingLeft:10,paddingRight:10,backgroundColor:'green',borderRadius:5,width:90,justifyContent:'center',alignItems:'center'}}>
                        <Text style={{color:'white',fontSize:12}}>Repeat Order</Text>
                    </TouchableOpacity>
                </View>
             )
         }
         else if(status == 4){
            return(
                <TouchableOpacity style={{paddingTop:7,paddingBottom:7,paddingLeft:10,paddingRight:10,backgroundColor:'black',borderRadius:5}}>
                <Text style={{color:'white',fontSize:12}}>Order Cancelled</Text>
            </TouchableOpacity>
             )
         }
    }

    const renderItem = (item) => {

      
        return(
           <Card
            containerStyle={{paddingTop:10,paddingLeft:15,paddingRight:15,paddingBottom:10,borderRadius:5,elevation:5}}>
                <View style={{flexDirection:"row",width:"100%"}}>
                    <View style={{justifyContent:"center",width:"20%",alignItems:"center"}}>
                        <Image source={{uri:ApiUrl.image_base_url + item.item.item_image}} style={{width:50,height:50,marginBottom:10}} />
                        {
                            item.status  == 0 
                            ?
                            <MaterialIcons name={"cancel"} size={30} color="red" />
                            :
                            (item.status  ==  1 
                            ?
                            <AntDesign name={"clockcircle"} size={30} color="#FBD64A" />
                            :
                            <AntDesign name={"checkcircle"} size={30} color={COLORS.theme_green} />
                            )

                        }
                    </View>
                    <View style={{width:"75%",marginLeft:10,marginRight:15}}>
                        <View style={{flexDirection:'row', justifyContent:'space-between',alignItems:"center",marginBottom:10}}>
                            <View>
                                <Text style={styles.boldText}>Order Date :</Text>
                                <Text style={{color:COLORS.desp_grey,fontFamily:"roboto-medium"}}>{item.created_at.substring(0,11)}</Text>
                            </View>
                            <View>
                                <Text style={[styles.boldText,{textAlign:'right'}]}>Delivery Date :</Text>
                                <Text style={{color:COLORS.desp_grey,fontFamily:"roboto-medium",textAlign:'right'}}>{item.delivery_timing == null ? "Soon.." : item.delivery_timing.substring(0,11)}</Text>
                            </View>

                        </View>
                        <View style={{flexDirection:'row', justifyContent:'space-between',alignItems:"center",}}>
                            <View>
                                <Text style={styles.boldText}>Order Id :</Text>
                                <Text style={{color:COLORS.desp_grey,fontFamily:"roboto-medium"}}>{item.order_number}</Text>
                            </View>
                            <View>
                            { getStatus(item)   }
                               
                            </View>

                        </View>
                    </View>
                    

                </View>
                
            </Card>
        )

    }

    const common = useSelector(state => state.common);
    const userReducer = useSelector(state => state.user);
    const dispatch = useDispatch();


    function fetchOrders(){
        let formData = new FormData();
        formData.append("skip",0);
        formData.append("take",10);
        formData.append("user_id",userReducer.user_details.id);
        dispatch(getMyOrders(formData))
        .then(response=> {
            console.log(JSON.stringify(response))
            if(!response.error){
               // var category = 
               setOrderHistory(response.result)
                 

            }else{
                //showMessage(response.message)
            }
        })
        .catch(error => {
            showMessage('Something went wrong,Try Again')
        })

    }

    useEffect(() => { 
        fetchOrders()
    }, []);


    return(
        <>
        <KeyboardAwareScrollView
            contentContainerStyle={{flexGrow:1}}
        >
            <View style={styles.container}>

            {!common.loading  && orderHistory.length == 0 
            ?
            <Image resizeMode='contain'
            source={require('../assets/history.png')} 
            style={{alignSelf:"center",width:300,height:300}} />

            :   <FlatList 
            data={orderHistory}
            renderItem={({item}) => 
                renderItem(item)}
            />
           }
              
                
            </View>
        </KeyboardAwareScrollView>
        { common.loading  && <ProgressBar/> }
        </>
    )


}

const styles  = StyleSheet.create({
    container : {
        flex:1,
        backgroundColor:'white',
        paddingTop:20
    },
    boldText:{
        fontFamily:'roboto-bold',
        fontSize:14,
        color:"black"
    }
})
export default OrderHistory;