import React ,{useState,useEffect}from 'react';
import {Text, StyleSheet,View, FlatList, SafeAreaView,Image,TouchableOpacity} from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import CustomBanner from '../CustomUI/CustomBanner';
import CustomSearch from '../CustomUI/CustomSearch';
import CircularImage from '../CustomUI/CircularImage';
import ProductItem from '../CustomUI/ProductItem';
import ProgressBar from '../Utils/ProgressBar';

import { useDispatch,useSelector } from 'react-redux';
import  {searchProducts} from '../redux/actions/home';
import user_reducer from '../redux/reducers/user_reducer';
import {showMessage} from '../Utils/ShowMessage';
import ApiUrl from '../Utils/ApiUrl';



const Search = (props) => {

   // console.log("props",props.showFilter);

    const [products, setProducts] = useState([]);

    const common = useSelector(state => state.common);
    const userReducer = useSelector(state => state.user);
    const dispatch = useDispatch();


    function searchProduct(text){
        console.log(text)
        var formData = new FormData()
        formData.append('search',text)
        dispatch(searchProducts(formData))
        .then(response=> {
            console.log(response)
            if(!response.error) setProducts(response.result)
            else 
             {  
                 setProducts([])
                //showMessage(response.message)
                showMessage("Invalid Search")
            }
        })
        .catch(error => {
            showMessage('Something went wrong,Try Again')
        })

    }
    useEffect(() => { 

    }, []);
   
   
    const renderProductItem = (item) => {

        return(
            <TouchableOpacity onPress={() => props.navigation.navigate('DetailsPage',{name:item.name ,id:item.id})}>
            <ProductItem  item={item} />
            </TouchableOpacity>
        )
    }

  

    // const showFilter = () => {
    //     setShowFilter(!showFilter);
    // }

    return(
        <SafeAreaView style={{backgroundColor:'white',flex:1}}>
        <KeyboardAwareScrollView style={styles.conatiner}>
        <CustomSearch 
         focus={true}
        onSearch={(text) =>searchProduct(text)}
        customContainer={{marginTop:20,alignSelf:'center',width : '95%'}}/>
            <View>
                <FlatList 
                style={{marginTop:10}}
                showsVerticalScrollIndicator={false}
                data={products}
                renderItem={({ item }) => renderProductItem(item)}
                keyExtractor={(item, index) => item.id } />

            </View>
        </KeyboardAwareScrollView>
        { common.loading  && <ProgressBar/> }
        
        </SafeAreaView>
    )
}

const styles =  StyleSheet.create({

    conatiner :{
        flex:1,
      //  backgroundColor:'white',
       
    },
    textStyle:{
        fontFamily:'roboto-bold',
        fontSize:15,

    },
    ViewMoreText:{
        color:'grey',
        fontSize:13,
        fontFamily:'roboto-regular'
    }

    
})

export default Search;