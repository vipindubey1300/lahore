import React, { useState } from 'react';
import { Text, Image, View ,StyleSheet,TouchableOpacity} from 'react-native';
import CustomTextInput from '../CustomUI/CustomTextInput';
import CustomButton from '../CustomUI/CustomButton';
import {COLORS} from '../Utils/Utils';
import AuthContext from '../AuthContext/AuthContext';
import AsyncStorage from '@react-native-community/async-storage';

import {showMessage} from '../Utils/ShowMessage';

import { useDispatch,useSelector } from 'react-redux';
import  {addAddress} from '../redux/actions/home';
import * as types  from '../redux/types';
import ProgressBar from '../Utils/ProgressBar';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';


const AddAddress = (props) => {

    // const {sigIn} = React.useContext(AuthContext);

    const [name ,setName ] = useState("");
    const [contact ,setContact ] = useState("");
    const [address ,setAddress ] = useState("");
    const [apartment ,setApartment ] = useState("");
    const [city ,setCity ] = useState("");
    const [state ,setState ] = useState("");
    const [zip ,setZip ] = useState("");

    const [errorNameMsg ,setErrorNameMsg ] = useState("");
    const [errorContactMsg ,setErrorContactMsg ] = useState("");
    const [errorAddressMsg ,setErrorAddressMsg ] = useState("");
    const [errorApartmentMsg ,setErrorApartmentMsg ] = useState("");
    const [errorCityMsg ,setErrorCityMsg ] = useState("");
    const [errorStateMsg ,setErrorStateMsg ] = useState("");
    const [errorZipMsg ,setErrorZipMsg ] = useState("");

    const common = useSelector(state => state.common);
    const userReducer = useSelector(state => state.user);
    const dispatch = useDispatch();

    function validateSpecialChars(value){
        return ((/[ `!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?~]/).test(value)) 
    }

    function isEmoji(str) {
        var ranges = [
            '(?:[\u2700-\u27bf]|(?:\ud83c[\udde6-\uddff]){2}|[\ud800-\udbff][\udc00-\udfff]|[\u0023-\u0039]\ufe0f?\u20e3|\u3299|\u3297|\u303d|\u3030|\u24c2|\ud83c[\udd70-\udd71]|\ud83c[\udd7e-\udd7f]|\ud83c\udd8e|\ud83c[\udd91-\udd9a]|\ud83c[\udde6-\uddff]|[\ud83c[\ude01-\ude02]|\ud83c\ude1a|\ud83c\ude2f|[\ud83c[\ude32-\ude3a]|[\ud83c[\ude50-\ude51]|\u203c|\u2049|[\u25aa-\u25ab]|\u25b6|\u25c0|[\u25fb-\u25fe]|\u00a9|\u00ae|\u2122|\u2139|\ud83c\udc04|[\u2600-\u26FF]|\u2b05|\u2b06|\u2b07|\u2b1b|\u2b1c|\u2b50|\u2b55|\u231a|\u231b|\u2328|\u23cf|[\u23e9-\u23f3]|[\u23f8-\u23fa]|\ud83c\udccf|\u2934|\u2935|[\u2190-\u21ff])' // U+1F680 to U+1F6FF
        ];
        if (str.match(ranges.join('|'))) {
            return true;
        } else {
            return false;
        }
    }
    


    const validator = (type,value) => {
        switch(type){
             case 'name' :{
                setName(value)
               if(!value){
                 setErrorNameMsg("Please Enter Name");  
                   return false
                }
                // else if(validateSpecialChars(value)){
                //     setErrorNameMsg("Please Enter Valid Name");  
                //      return false
                // }
                else if(isEmoji(value)){
                    setErrorNameMsg("Please Enter Valid Name");  
                     return false
                }
                setErrorNameMsg("");
                return true
            }
            case 'contact' :{
                setContact(value)
               if(!value){
                setErrorContactMsg("Please Enter Contact Number");  
                   return false
                }
                else if(value.length < 7 || value.length >15){
                    setErrorContactMsg("Contact Number Should Be 7-15 Digits");
                    return false
                  }
                  else if(value == '0000000'){
                    setErrorContactMsg("Contact Number Should Be Valid");
                    return false
                }
    
                  else if(value == '00000000'){
                    setErrorContactMsg("Contact Number Should Be Valid");
                    return false
                    
                }
                else if(value == '000000000'){
                    setErrorContactMsg("Contact Number Should Be Valid");
                    return false
                    
                }
                else if(value == '0000000000'){
                    setErrorContactMsg("Contact Number Should Be Valid");
                   
                    return false
                    
                }
                else if(value == '00000000000'){
                    setErrorContactMsg("Contact Number Should Be Valid");
                  
                    return false
                    
                }
                else if(value == '000000000000'){
                    setErrorContactMsg("Contact Number Should Be Valid");
                    
                    return false
                    
                }
                else if(validateSpecialChars(value)){
                    setErrorContactMsg("Contact Number Should Be Valid");
                     return false
                }
                else if(isEmoji(value)){
                    setErrorContactMsg("Contact Number Should Be Valid");  
                     return false
                }

                setErrorContactMsg("");
                return true
            }
            case 'address' :{
                setAddress(value)
               if(!value){
                setErrorAddressMsg("Please Enter Address Line ");  
                   return false
                }
                // else if(validateSpecialChars(value)){
                //     setErrorAddressMsg("Please Enter Valid Address Line");  
                //      return false
                // }
                else if(isEmoji(value)){
                    setErrorAddressMsg("Please Enter Valid Address Line");  
                     return false
                }
               setErrorAddressMsg("");
                return true
            }
            case 'apartment' :{
                setApartment(value)
               if(!value){
                setErrorApartmentMsg("Please Enter Apartment ");  
                   return false
                }
                // else if(validateSpecialChars(value)){
                //     setErrorApartmentMsg("Please Enter Valid Apartment");  
                //      return false
                // }
                else if(isEmoji(value)){
                    setErrorApartmentMsg("Please Enter Valid Apartment");  
                     return false
                }
                setErrorApartmentMsg("");
                return true
            }
            case 'city' :{
                setCity(value)
               if(!value){
                setErrorCityMsg("Please Enter City ");  
                   return false
                }
                // else if(validateSpecialChars(value)){
                //     setErrorCityMsg("Please Enter Valid City");  
                //      return false
                // }
                else if(isEmoji(value)){
                    setErrorCityMsg("Please Enter Valid City");  
                     return false
                }
                setErrorCityMsg("");
                return true
            }
            case 'state' :{
                setState(value)
               if(!value){
                setErrorStateMsg("Please Enter State ");  
                   return false
                }

                // else if(validateSpecialChars(value)){
                //     setErrorStateMsg("Please Enter valid State");  
                //      return false
                // }
                else if(isEmoji(value)){
                    setErrorStateMsg("Please Enter Valid State");  
                     return false
                }


                setErrorStateMsg("");
                return true
            }
            case 'zip' :{
                setZip(value)
               if(!value){
                setErrorZipMsg("Please Enter Zip Code ");  
                   return false
                }
                else if(value.length < 4 || value.length > 6){
                    setErrorZipMsg("Zip Code Should Be 4-6 digits");
                    return false
                  }
                setErrorZipMsg("");
                return true
            }
            default :
                return true;
        }

    }

    const onSubmit = async() => {

        if(validator('name',name) && 
        validator('contact',contact) && 
        validator('address',address) &&
       // validator('apartment',apartment) && 
        validator('city',city) && 
        validator('state',state) &&
        validator('zip',zip)  ){

            let formdata = new FormData();
            formdata.append("user_id",userReducer.user_details.id);
            formdata.append("contact_number",contact);
            formdata.append("name",name);
            formdata.append("address",address);
            formdata.append("apartment_unit_no",apartment);
            formdata.append("city",city);
            formdata.append("state",state);
            formdata.append("postal_code",zip);

            dispatch(addAddress(formdata))
            .then(response=> {
                console.log("addAddress REsponse ---->>>>",response)
                if(!response.error){
                    showMessage(response.message,false)
                    props.navigation.goBack()
                }else{
                    showMessage(response.message)
                }
            })
            .catch(error => {
                showMessage('Something went wrong,Try Again')
            })
         }
         
    }



    return(
   
         <View style={styles.container}>
         
         <KeyboardAwareScrollView contentContainerStyle={styles.mainView}>
 
             <CustomTextInput 
                 labelText="Name"
                 placeholder="Enter Name"
                 value={name}
                 customTextInputStyle={errorNameMsg !== "" ?  {paddingBottom:2} : {paddingBottom:10}}
                 errorMsg={errorNameMsg}
                 onChangeText = {(value)=> {validator('name',value)}}
             />
             <CustomTextInput 
             labelText="Contact"
             placeholder="Enter Contact Number"
             value={contact}
             keyboardType="phone-pad"
             customTextInputStyle={errorContactMsg !== "" ?  {paddingBottom:2} : {paddingBottom:10}}
             errorMsg={errorContactMsg}
             onChangeText = {(value)=> {validator('contact',value)}}
             />

             <CustomTextInput 
             labelText="Address Line"
             placeholder="Enter Address Line"
             value={address}
             customTextInputStyle={errorAddressMsg !== "" ?  {paddingBottom:2} : {paddingBottom:10}}
             errorMsg={errorAddressMsg}
             onChangeText = {(value)=> {validator('address',value)}}
         />

            <CustomTextInput 
            labelText="Apartment"
            placeholder="Enter Apartment"
            value={apartment}
            customTextInputStyle={errorApartmentMsg !== "" ?  {paddingBottom:2} : {paddingBottom:10}}
            errorMsg={errorApartmentMsg}
           // onChangeText = {(value)=> {validator('apartment',value)}}
           onChangeText = {(value)=> setApartment(value)}
            />


            <CustomTextInput 
            labelText="City"
            placeholder="Enter City"
            value={city}
            customTextInputStyle={errorCityMsg !== "" ?  {paddingBottom:2} : {paddingBottom:10}}
            errorMsg={errorCityMsg}
            onChangeText = {(value)=> {validator('city',value)}}
        />

            <CustomTextInput 
            labelText="State"
            placeholder="Enter State"
            value={state}
            customTextInputStyle={errorStateMsg !== "" ?  {paddingBottom:2} : {paddingBottom:10}}
            errorMsg={errorStateMsg}
            onChangeText = {(value)=> {validator('state',value)}}
            />

            <CustomTextInput 
            labelText="Postal Code"
            placeholder="Enter Postal Code"
            value={zip}
            keyboardType="phone-pad"
            customTextInputStyle={errorZipMsg !== "" ?  {paddingBottom:2} : {paddingBottom:10}}
            errorMsg={errorZipMsg}
            onChangeText = {(value)=> {validator('zip',value)}}
            />

                    
 
               
                 <CustomButton btnText="UPDATE" 
                 onPress={() => onSubmit()}
                 customStyle={{marginLeft:10,marginRight:10,marginTop:20,marginBottom:40}}/>
 
 
         </KeyboardAwareScrollView>
         { common.loading  && <ProgressBar/> }
     </View>
    )
}


const styles = StyleSheet.create({

    container:{
        flex:1,
        backgroundColor:"white"
    },
    mainView:{
        justifyContent:'center',
        alignItems:'center',
        padding:20,
        marginTop:20
    },
    dontHveAccStyle:{
        fontSize:15
    },
    signUPText:{
        color : COLORS.theme_orange
    }
})

export default AddAddress;