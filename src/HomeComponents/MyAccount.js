import React ,{useState, useEffect} from 'react';
import {View, Text, StyleSheet ,Image, TouchableOpacity, FlatList} from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { Card } from 'react-native-elements';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import { COLORS } from '../Utils/Utils';
import CustomAddress from '../CustomUI/CustomAddress';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import AntDesign from 'react-native-vector-icons/AntDesign'
import  {getProfile} from '../redux/actions/user_action';
import {showMessage} from '../Utils/ShowMessage';
import ApiUrl from '../Utils/ApiUrl';
import { useDispatch,useSelector } from 'react-redux';
import ProgressBar from '../Utils/ProgressBar';
import { getAddress } from '../redux/actions/home';
import { SafeAreaView } from 'react-native-safe-area-context';



const MyAccount = (props) =>{

    const [addresses , setAddresses] = useState([]);

    const [payByCards ,setPayByCards] = useState(false);
    const [userDetails ,setUserDetails] = useState('');


    const common = useSelector(state => state.common);
    const userReducer = useSelector(state => state.user);
    const dispatch = useDispatch();



    function fetchProfile(){
        dispatch(getProfile(userReducer.user_details.id))
        .then(response=> {
            if(!response.error){
              setUserDetails(response.result)
            }else{
                showMessage(response.message)
            }
        })
        .catch(error => {
            showMessage('Something went wrong,Try Again')
        })
    }

    function fetchAddress(){
        let formData = new FormData();
        formData.append("user_id",userReducer.user_details.id);
        dispatch(getAddress(formData))
        .then(response=> {
            if(!response.error){
               console.log('fetchAddress --->>',response)
               setAddresses(response.result)
               //if(response.result.length == 0) showMessage("No Address Found")

            }else{
               // showMessage("No Address Found")
            }
        })
        .catch(error => {
            showMessage('Something went wrong,Try Again')
        })

    }


    useEffect(() => { 
        fetchProfile()
        fetchAddress()
    }, []);

    const renderItem = (item,index) => {

        return <CustomAddress deliveryPart={false}
        item={item} index={index}
        onSelectAddress={(id, index) => onSelectAddress(id,index)} 
        />
    }

    let onSelectAddress = (id,index) => {

        let copyAddress = [...addresses];
        copyAddress.forEach((element,index) => {

           
            element.selected =  false;
            if(element.id == id){
                console.log(element.selected);
                element.selected = true;
            }
        })

        setAddresses(copyAddress);
    }


    return(
        <SafeAreaView style={{flex:1}}>
        <KeyboardAwareScrollView>
            <View style={styles.container}>
                <Card 
                containerStyle={{padding:20, margin:20, borderRadius:5,elevation:3,marginBottom:20}}>
                    <TouchableOpacity onPress={()=> props.navigation.navigate('EditProfile')}>
                        <FontAwesome name={"pencil"} color={COLORS.theme_green} size={20} style={{alignSelf:"flex-end"}} />
                    </TouchableOpacity>
                    <View style={styles.circularImage}>
                        <Image source={{uri : ApiUrl.image_base_url + userDetails.user_image}} style={styles.imgStyle} resizeMode="contain"/>
                    </View>
                   
                    <Text style={[styles.accountText,{fontSize:15,color:'black'}]}>{userDetails.name}</Text>
                    <Text style={styles.accountText}>{userDetails.mobile}</Text>
                    <Text style={styles.accountText}>{userDetails.email}</Text>
                </Card>
                <Text style={styles.addressTitle}>Addresses</Text>
                <FlatList
                horizontal
                showsHorizontalScrollIndicator={false}
                data={addresses}
                renderItem={({ item,index }) =>  renderItem(item,index)}
                keyExtractor={(item, index) => item.id }
                />
                 <Card
                containerStyle={{padding:0,borderRadius:5,margin:20,paddingLeft:0,paddingRight:0,elevation:10}}>
                        <TouchableOpacity 
                        onPress={()=> props.navigation.navigate('ResetPassword')} 
                        style={{flexDirection:'row',justifyContent:'space-between',alignItems:'center',marginTop:20,marginBottom:20,paddingLeft:10,paddingRight:10}}>
                            <View style={{flexDirection:'row'}}>
                           
                            <FontAwesome5 name="key" color={'black'} size={20}  />
                             <Text style={[styles.headingText,{marginLeft:20}]}>Change Password</Text>
                            </View>
                            <FontAwesome  name="angle-right" color={'grey'} size={20}  />
                        </TouchableOpacity>
                   
                    <View style={styles.viewLine} />
                  

                    <TouchableOpacity 
                    onPress={()=> props.navigation.navigate('Address')} 
                    style={{flexDirection:'row',justifyContent:'space-between',alignItems:'center',marginTop:20,marginBottom:20,paddingLeft:10,paddingRight:10}}>
                        <View style={{flexDirection:'row'}}>
                       
                        <FontAwesome5 name="address-book" color={'black'} size={20}  />
                         <Text style={[styles.headingText,{marginLeft:20}]}>Manage Address</Text>
                        </View>
                        <FontAwesome  name="angle-right" color={'grey'} size={20}  />
                    </TouchableOpacity>
               
                <View style={styles.viewLine} />
              
                        {/* <TouchableOpacity 
                        //onPress={()=>setPayByCards(!payByCards)} 
                        style={{flexDirection:'row',alignItems:'center',marginTop:20,marginBottom:20,paddingLeft:10,paddingRight:10,justifyContent:"space-between"}}>
                            <View style={{flexDirection:"row"}}>
                            <AntDesign  name="minuscircle" color="black" size={20}  />
                            <Text style={[styles.headingText,{marginLeft:20,color:'red',alignSelf:"flex-start"}]}>Deactivate Account </Text>
                            </View>
                            
                            <FontAwesome  name="angle-right" color={'grey'} size={20} style={{alignSelf:"flex-end",textAlign:"right"}} />
                        </TouchableOpacity> */}
                   
                </Card>

            </View>
            
        </KeyboardAwareScrollView>
        { common.loading  && <ProgressBar/> }
        </SafeAreaView>


       
    )
}

const styles = StyleSheet.create({
    container:{
        flex:1,
       // padding:20
    },
    imgStyle:{
        width:80,
        height:80,
        borderRadius:80/2,
        alignSelf:"center",
      
    },
    accountText:{
        fontFamily:'roboto-medium',
        fontSize:12,
        color:'#808080',
        lineHeight:20
    },
    addressTitle:{
        fontFamily:'roboto-medium',
        color:'grey',
        fontSize:15,
        marginBottom:15,
        marginLeft:20,
        marginRight:20,
    },
    viewLine:{
        width:'100%',
        height:1,
        backgroundColor:"grey"
    },
    circularImage:{
       
        width:90,
        height:90,
        alignSelf:"center",
        alignItems:'center',
       justifyContent:"center",
       borderRadius:180/2,
       borderColor:COLORS.desp_grey,
       borderWidth:0.5,
       overflow:'hidden'
    },
})

export default MyAccount;