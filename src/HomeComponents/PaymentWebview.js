import React ,{useState,useEffect}from 'react';
import {Text, StyleSheet,View, FlatList, SafeAreaView,Image,TouchableOpacity} from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import CustomBanner from '../CustomUI/CustomBanner';
import CircularImage from '../CustomUI/CircularImage';
import ProductItem from '../CustomUI/ProductItem';
import ProgressBar from '../Utils/ProgressBar';

import { useDispatch,useSelector } from 'react-redux';
import  {PaymentWebviewProducts, orderSuccessApi} from '../redux/actions/home';
import user_reducer from '../redux/reducers/user_reducer';
import {showMessage} from '../Utils/ShowMessage';
import ApiUrl from '../Utils/ApiUrl';
import { WebView } from 'react-native-webview';
import ProgressWebView from "react-native-progress-webview";



const PaymentWebview = (props) => {
  
    const common = useSelector(state => state.common);
    const userReducer = useSelector(state => state.user);
    const dispatch = useDispatch();


    useEffect(() => { 
        console.log('----->',props.route.params.url)
    }, []);


    function orderSucces(){
   
            let formData = new FormData();
            formData.append("user_id",userReducer.user_details.id);
            formData.append("order_no",props.route.params.order_no);
            dispatch(orderSuccessApi(formData))
            .then(response=> {
                if(!response.error){
                    props.navigation.replace("OrderPlaced")
                }else{
                    showMessage(response.message)
                }
            })
            .catch(error => {
                showMessage(error.message)
            })
    }
   
   function onNavigationStateChange (eve) {
       
            console.log('Webview Status---->> ',eve)
            if(eve.title == 'Payment Success'){
                 orderSucces()
                // setTimeout(() => {
                //     props.navigation.navigate("OrderPlaced")
                //     //showMessage("Payment Done Successfully.")
                // }, 1000)
            }
      };

      function onMessage( event ) {
        console.log( "On Message", event.nativeEvent.data );
    }
   

    return(
        <SafeAreaView style={{backgroundColor:'white',flex:1}}>
       
            <ProgressWebView color={'green'}
                errorColor={'red'}
                disappearDuration={500}
                onNavigationStateChange={(eve)=>onNavigationStateChange(eve)}
                style={styles.conatiner}
                onMessage={(eve)=>onMessage(eve)}
                source={{ uri: props.route.params.url}} />
           
       
        { common.loading  && <ProgressBar/> }
        
        </SafeAreaView>
    )
}

const styles =  StyleSheet.create({

    conatiner :{
        flex:1,
      //  backgroundColor:'white',
       
    },
    
})

export default PaymentWebview;