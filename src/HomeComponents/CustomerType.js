import React from 'react';
import {View , Text,StyleSheet} from 'react-native';
import CustomButton from '../CustomUI/CustomButton';
import ProgressBar from '../Utils/ProgressBar';

const CustomerType = (props) => {

    return(
        <View style={styles.container}>

            <CustomButton btnText="PRIVATE" 
                 onPress={() => props.navigation.replace('CustomerCheckout',{type:'private'})}
                 customStyle={{elevation:10}}
                />
            <CustomButton btnText="BUSINESS / PUBLIC" 
                 onPress={() => props.navigation.replace('CustomerCheckout',{type:'public'})}
                 customStyle={{elevation:10,marginTop:20,}}
                />
            
        </View>
    )
}

const styles = StyleSheet.create({
    container:{
        flex:1,
        justifyContent:'center',
        alignItems:'center',
        padding:20
    }
})

export default CustomerType;