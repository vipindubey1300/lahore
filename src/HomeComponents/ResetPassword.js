import React, { useState } from 'react';
import { Text, Image, View ,StyleSheet,TouchableOpacity} from 'react-native';
import CustomTextInput from '../CustomUI/CustomTextInput';
import CustomButton from '../CustomUI/CustomButton';
import {COLORS} from '../Utils/Utils';
import AuthContext from '../AuthContext/AuthContext';
import AsyncStorage from '@react-native-community/async-storage';

import {showMessage} from '../Utils/ShowMessage';

import { useDispatch,useSelector } from 'react-redux';
import  {resetPassword} from '../redux/actions/user_action';
import * as types  from '../redux/types';
import ProgressBar from '../Utils/ProgressBar';


const ResetPassword = (props) => {

   // const {sigIn} = React.useContext(AuthContext);

    const [password ,setPassword ] = useState("");
    const [confirmPassword ,setConfirmPassword ] = useState("");
    const [newPassword ,setNewPassword ] = useState("");

    const [errorConfirmPasswordMsg ,setConfirmPasswordErrorMsg ] = useState("");
    const [errorNewPasswordMsg ,setNewPasswordErrorMsg ] = useState("");
    const [errorPasswordMsg ,setErrorPasswordMsg ] = useState("");

    const common = useSelector(state => state.common);
    const userReducer = useSelector(state => state.user);
    const dispatch = useDispatch();



    const validator = (type,value) => {
     
        switch(type){
             case 'old_pass' :{
                setPassword(value)
                console.log(value.length < 8 || value.length > 16)
               if(!value){
                setErrorPasswordMsg("Please Enter Old Password");  
                   return false
                }
                else if(value.length < 8 || value.length > 16){

                    setErrorPasswordMsg("Old Password Must Be Of Greater Than 8 Characters And Less Than 16.");
                    //setErrorTrue(true);
                    return false
                    
                }
                setErrorPasswordMsg("");
                return true
            }
            case 'new_pass' :{
                setNewPassword(value)
               if(!value){
                setNewPasswordErrorMsg("Please Enter New Password");  
                   return false
                }
                else if(value.length < 8 || value.length > 16){

                    setNewPasswordErrorMsg("New Password Must Be Of Greater Than 8 Characters And Less Than 16.");
                 
                    return false
                    
                }
                setNewPasswordErrorMsg("");
                return true
            }
            case 'cnf_pass' :{
              setConfirmPassword(value)
               if(value.length == 0){
                setConfirmPasswordErrorMsg("Please Enter Confirm Password");  
                   return false
                }
                else if(value != newPassword){
                    setConfirmPasswordErrorMsg("Password And Confirm Password Must Be Same");
                    
                    return false
                }
                setConfirmPasswordErrorMsg("");
                
                return true
            }
            default :
                return true;
        }

    }

    const onSubmit = async() => {

        if(validator('old_pass',password) && validator('new_pass',newPassword) && validator('cnf_pass',confirmPassword)){
            let formdata = new FormData();
            formdata.append("user_id",userReducer.user_details.id);
            formdata.append("confirm_password",confirmPassword);
            formdata.append("new_password",newPassword);
            formdata.append("old_password",password);
            formdata.append("device_token",'asfafet4t43t4');

            dispatch(resetPassword(formdata))
            .then(response=> {
                console.log("resetPassword REsponse ---->>>>",response)
                if(!response.error){
                    showMessage(response.message,false)
                    AsyncStorage.getAllKeys().then(AsyncStorage.multiRemove);
                    dispatch({ type:types.LOGOUT_USER })
                }else{
                    showMessage(response.message)
                }
            })
            .catch(error => {
                showMessage('Something went wrong,Try Again')
            })
         }
    }



    return(
   
         <View style={styles.container}>
         
         <View style={styles.mainView}>
 
             <CustomTextInput 
                 labelText="Old Password"
                 placeholder="Enter Old Password"
                 isSecureText={true}
                 secureTextEntry={true}
                 value={password}
                 customTextInputStyle={errorPasswordMsg !== "" ?  {paddingBottom:2} : {paddingBottom:10}}
                 errorMsg={errorPasswordMsg}
                // onChangeText = {(value)=> {setPassword(value)}}
                onChangeText = {(value)=> {validator('old_pass',value)}}
             />
             <CustomTextInput 
                 labelText="New Password"
                 placeholder="Enter New Password"
                 customTextInputStyle={{width:"85%"}}
                 isSecureText={true}
                 secureTextEntry={true}
                 value={newPassword}
                 errorMsg={errorNewPasswordMsg}
                 customTextInputStyle={errorNewPasswordMsg !== "" ?  {paddingBottom:2} : {paddingBottom:10}}
                // onChangeText = {(value)=> {setNewPassword(value)}}
                onChangeText = {(value)=> {validator('new_pass',value)}}
                 />

                <CustomTextInput 
                 labelText="Confirm Password"
                 placeholder="Enter Confirm Password"
                 customTextInputStyle={{width:"85%"}}
                 isSecureText={true}
                 secureTextEntry={true}
                 value={confirmPassword}
                 errorMsg={errorConfirmPasswordMsg}
                 customTextInputStyle={errorConfirmPasswordMsg !== "" ?  {paddingBottom:2} : {paddingBottom:10}}
                // onChangeText = {(value)=> {setConfirmPassword(value)}}
                 onChangeText = {(value)=> {validator('cnf_pass',value)}}
                 />
 
               
                 <CustomButton btnText="UPDATE" 
                 onPress={() => onSubmit()}
                 customStyle={{marginLeft:10,marginRight:10,marginTop:20}}/>
 
 
         </View>
         { common.loading  && <ProgressBar/> }
     </View>
    )
}


const styles = StyleSheet.create({

    container:{
        flex:1,
        backgroundColor:"white"
    },
    mainView:{
        justifyContent:'center',
        alignItems:'center',
        margin:20,
        marginTop:40
    },
    dontHveAccStyle:{
        fontSize:15
    },
    signUPText:{
        color : COLORS.theme_orange
    }
})

export default ResetPassword;