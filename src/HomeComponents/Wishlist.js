
import React ,{useState, useEffect} from 'react';
import {View , Text , Image,FlatList , TouchableOpacity, StyleSheet} from 'react-native';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import WishlistComponent from '../CustomUI/WishlistComponent';
import  {getWishlistItems} from '../redux/actions/home';
import {showMessage} from '../Utils/ShowMessage';
import ApiUrl from '../Utils/ApiUrl';
import { useDispatch,useSelector } from 'react-redux';
import ProgressBar from '../Utils/ProgressBar';

const Wishlist = (props) => {
    const [wishlistItems, setWishlistItems] = useState([ ]);


    const common = useSelector(state => state.common);
    const userReducer = useSelector(state => state.user);
    const dispatch = useDispatch();


    function fetchWishlistItems(){
        let formData = new FormData();
        formData.append("user_id",userReducer.user_details.id);
        dispatch(getWishlistItems(formData))
        .then(response=> {
            if(!response.error){
               // var category = 
               console.log(response)
               setWishlistItems(response.result)
               if(response.result.length == 0) showMessage("No Items in Wishlist")

            }else{
                showMessage("No Items in Wishlist")
            }
        })
        .catch(error => {
            showMessage('Something went wrong,Try Again')
        })

    }



    useEffect(() => { 
        fetchWishlistItems()
    }, []);

    function onDeleteItem(item){
        console.log('item deleted---')
        var temp = wishlistItems.filter(i => i.id != item.id)
        setWishlistItems(temp)
        
    }


    const renderProductItem = (item) => {

        return(
                <TouchableOpacity onPress={() => props.navigation.navigate('DetailsPage',{name:item.itemdetails.name ,id:item.itemdetails.id})}>
                    {/* <ProductItem  {...props} item={item}/> */}
                    <WishlistComponent onDelete ={onDeleteItem}
                     {...props} item={item} />
                </TouchableOpacity>
              
          
       
        )
    }

    return(
        <KeyboardAwareScrollView 
        contentContainerStyle={{flexGrow:1}}
        >
        <View style={styles.conatiner}>

        {!common.loading  && wishlistItems.length == 0 
            ?
            <Image resizeMode='contain'
            source={require('../assets/no-item-in-wishlist.png')} 
            style={{alignSelf:"center",width:300,height:300}} />

            :   
            <FlatList 
            style={{marginTop:10}}
            showsVerticalScrollIndicator={false}
            data={wishlistItems}
            renderItem={({ item }) => renderProductItem(item)}
            keyExtractor={(item, index) => item.id } />
           }


       
        </View>
        { common.loading  && <ProgressBar/> }
        </KeyboardAwareScrollView>
    )
}

const styles = StyleSheet.create({
    conatiner:{
        flex:1,
       // backgroundColor:'white'
    }
})

export default Wishlist;