import React ,{useState,useEffect} from 'react';
import {View ,Text, Image, TouchableOpacity,StyleSheet} from 'react-native';
import ProgressBar from '../Utils/ProgressBar';
import ExpandableListView from 'react-native-expandable-listview';
import { useDispatch,useSelector } from 'react-redux';
import { COLORS } from '../Utils/Utils';
import  {getFaq} from '../redux/actions/home';
import {showMessage} from '../Utils/ShowMessage';


const CONTENT = [
    {
      "id": 1,
      "cellHeight": 50,
      "isExpanded": false,
      "categoryName": "What is grocery ? ",
      "subCategory": [
        {
          "id": 1,
          "name": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make",
          "innerCellHeight": 100
        }
      ]
    },
    {
      "id": 2,
      "cellHeight": 50,
      "isExpanded": false,
      "categoryName": "What kind of product you save?",
      "subCategory": [
        {
          "id": 2,
          "name": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make",
          "innerCellHeight": 100
        }
      ]
    },
    {
        "id": 2,
        "cellHeight": 50,
        "isExpanded": false,
        "categoryName": "Re creating an order from the history?",
        "subCategory": [
          {
            "id": 2,
            "name": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make",
            "innerCellHeight": 100
          }
        ]
      }
  ]
   

const Faq  = (props) => {
    const [lists, setLists] = useState([]);

    const dispatch = useDispatch();
    const userReducer = useSelector(state => state.user);
    const common = useSelector(state => state.common);
    function fetchFaqs(){
        dispatch(getFaq())
        .then(response=> {
            console.log(response)
            if(!response.error){
               var tempList = response.result
               var newList = []
               tempList.forEach((element,index) => {
                   newList.push( {
                    id: element.id,
                    cellHeight: 50,
                    isExpanded:false,
                    categoryName: element.title,
                    subCategory: [{id: index, name: element.content, innerCellHeight: 100}],
                  })
               });
                  setLists(newList)
            }else{
                showMessage(response.message)
            }
        })
        .catch(error => {
            showMessage('Something went wrong,Try Again')
        })

    }

    useEffect(() => { 
        fetchFaqs()
    }, []);


    function updateLayout  (listDataSource) {
        console.log(listDataSource);
         setLists(listDataSource)
      };
     
    function  handleInnerClick  (innerIndex, headerItem, headerIndex) {
        console.log(innerIndex);
      };

    return(
        <View style={styles.container}>
            <View style={{width: '100%',height:60,backgroundColor:COLORS.theme_green,justifyContent:'center'}}>
                <Text style={{color:'white',padding:10,fontWeight:'bold',fontSize:18}}>General</Text>
            </View>

          {
            lists.length > 0
            ?
            <ExpandableListView
            //renderInnerItemSeparator={false} // true or false, render separator between inner items
              //  renderHeaderSeparator={true} // true or false, render separator between headers
                headerContainerStyle={{backgroundColor:'white'}} // add your styles to all item container of your list
                headerLabelStyle={{color:'black'}} // add your styles to all item text of your list
           // customChevron={{}} // your custom image to the indicator
               chevronColor='black' // "white" or "black" select wich color of the default indicator
                itemContainerStyle={{backgroundColor:'white'}} // add your styles to all inner item containers of your list
               itemLabelStyle={{color:'grey',fontSize:14}} // add your styles to all inner item text of your list
           // headerImageIndicatorStyle={{}} // add your styles to the image indicator of your list
            data={lists}
            onInnerItemClick={( innerIndex, headerItem, headerIndex ) => handleInnerClick(innerIndex, headerItem, headerIndex)} // required
            onItemClick={( listDataSource ) => updateLayout(listDataSource)}

         />
         :null
          }
          { common.loading  && <ProgressBar/> }

        </View>
    )
}

const styles= StyleSheet.create({

    container:{
        flex:1,
        backgroundColor:'white', 
        padding:20
    },
    lahoreText:{
        fontSize:15,
        fontFamily:'roboto-bold',
        lineHeight:18
    },
    lahoreText1:{
        fontSize:13,
        fontFamily:'roboto-bold',
        lineHeight:18
    }
});

export default Faq;