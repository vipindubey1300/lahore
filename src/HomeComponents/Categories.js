import React ,{useState,useEffect}from 'react';
import {Text, StyleSheet,View, FlatList, SafeAreaView,Image} from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import CustomBanner from '../CustomUI/CustomBanner';
import CustomSearch from '../CustomUI/CustomSearch';
import CircularImage from '../CustomUI/CircularImage';
import ProductItem from '../CustomUI/ProductItem';

import ProgressBar from '../Utils/ProgressBar';

import { useDispatch,useSelector } from 'react-redux';

import  {getCategories} from '../redux/actions/home';
import {showMessage} from '../Utils/ShowMessage';
import ApiUrl from '../Utils/ApiUrl';
import { TouchableOpacity } from 'react-native-gesture-handler';



const Categories = (props) => {

    const [categories, setCategories] = useState([]);



    const common = useSelector(state => state.common);
    const userReducer = useSelector(state => state.user);
    const dispatch = useDispatch();


    function fetchCategories(){
       
        dispatch(getCategories())
        .then(response=> {
            if(!response.error){
                var category = response.details
                  setCategories(category)
                 

            }else{
                showMessage(response.message)
            }
        })
        .catch(error => {
            showMessage('Something went wrong,Try Again')
        })

    }

    const onSubmitLogin = async() =>{}


    useEffect(() => { 
        fetchCategories()
    }, []);

   
    const renderProductItem = (item) => {

        return(
            <TouchableOpacity onPress={() => props.navigation.navigate('ListingPage',{name:item.name ,id:item.id})}>
            <ProductItem  showCategories={1} item={item} />
            </TouchableOpacity>
        )
    }

    return(
        <SafeAreaView style={{backgroundColor:'white',flex:1}}>
        <KeyboardAwareScrollView style={styles.conatiner}>
            <View>
                <FlatList 
                style={{marginTop:10}}
                showsVerticalScrollIndicator={false}
                data={categories}
                renderItem={({ item }) => renderProductItem(item)}
                keyExtractor={(item, index) => item.id } />

            </View>
        </KeyboardAwareScrollView>
        { common.loading  && <ProgressBar/> }
        </SafeAreaView>
    )
}

const styles =  StyleSheet.create({

    conatiner :{
        flex:1,
      //  backgroundColor:'white',
       
    },
    textStyle:{
        fontFamily:'roboto-bold',
        fontSize:15,

    },
    ViewMoreText:{
        color:'grey',
        fontSize:13,
        fontFamily:'roboto-regular'
    }

    
})

export default Categories;