import React ,{useState,useEffect} from 'react';
import {View ,Text, Image, TouchableOpacity,StyleSheet} from 'react-native';
import ProgressBar from '../Utils/ProgressBar';
import ExpandableListView from 'react-native-expandable-listview';
import { useDispatch,useSelector } from 'react-redux';
import { COLORS } from '../Utils/Utils';
import  {getPrivacy} from '../redux/actions/home';
import {showMessage} from '../Utils/ShowMessage';
import { ScrollView } from 'react-native-gesture-handler';
import ProgressBarWebView from 'react-native-progress-webview';
import WebView from 'react-native-webview';


const Privacy  = (props) => {
    const [data, setData] = useState('');

    const dispatch = useDispatch();
    const userReducer = useSelector(state => state.user);
    const common = useSelector(state => state.common);

    function get(){
        var form = new FormData()
        form.append('slug','privacy-policy')
        dispatch(getPrivacy(form))
        .then(response=> {
            console.log(response.result.short_description)
            if(!response.error){
                setData(response.result.short_description)
            }else{
                showMessage(response.message)
            }
        })
        .catch(error => {
            showMessage('Something went wrong,Try Again')
        })

    }

    useEffect(() => { 
        get()
    }, []);


    function updateLayout  (listDataSource) {
        console.log(listDataSource);
         setLists(listDataSource)
      };
     
    function  handleInnerClick  (innerIndex, headerItem, headerIndex) {
        console.log(innerIndex);
      };

    return(
            
            <WebView  style={{flex:1}}
            source={{html: data}}/>
          

      
    )
}

const styles= StyleSheet.create({

    container:{
        flex:1,
        backgroundColor:'white', 
        padding:20
    },
    lahoreText:{
        fontSize:15,
        fontFamily:'roboto-bold',
        lineHeight:18
    },
    lahoreText1:{
        fontSize:13,
        fontFamily:'roboto-bold',
        lineHeight:18
    }
});

export default Privacy;