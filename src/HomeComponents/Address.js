
import React ,{useState, useEffect} from 'react';
import {View , Text , Image,FlatList , TouchableOpacity, StyleSheet} from 'react-native';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import AddressComponent from '../CustomUI/AddressComponent';
import  {getAddress} from '../redux/actions/home';
import {showMessage} from '../Utils/ShowMessage';
import ApiUrl from '../Utils/ApiUrl';
import { useDispatch,useSelector } from 'react-redux';
import ProgressBar from '../Utils/ProgressBar';
import CustomButton from '../CustomUI/CustomButton';
import { SafeAreaView } from 'react-native-safe-area-context';
const Address = (props) => {


    const [address, setAddress] = useState([]);


    const common = useSelector(state => state.common);
    const userReducer = useSelector(state => state.user);
    const dispatch = useDispatch();


    function fetchAddress(){
        let formData = new FormData();

        formData.append("user_id",userReducer.user_details.id);
        dispatch(getAddress(formData))
        .then(response=> {
            if(!response.error){
               console.log('fetchAddress --->>',response)
                 setAddress(response.result)
               if(response.result.length == 0) showMessage("No Address Found")

            }else{
                showMessage("No Address Found")
            }
        })
        .catch(error => {
            showMessage('Something went wrong,Try Again')
        })

    }



 

    useEffect(() =>{
        const unsubscribe = props.navigation.addListener('focus', () => {
            fetchAddress()
        });
          
      },[props.navigation])

    function onDeleteItem(item){
        console.log('item deleted---')
        var temp = address.filter(i => i.id != item.id)
        setAddress(temp)
    }

    function onEditItem(item){
        // console.log('item on editing---',item)
        props.navigation.navigate("EditAddress",{'item':item})
    }


    const renderAddress = (item) => {

        return(
                <TouchableOpacity>
                    {/* <ProductItem  {...props} item={item}/> */}
                    <AddressComponent onDelete ={onDeleteItem}
                    onEdit ={onEditItem}
                     {...props} item={item} />
                </TouchableOpacity>
        )
    }

    return(
        <SafeAreaView style={{flex:1}}>
        <KeyboardAwareScrollView 
        contentContainerStyle={{flexGrow:1}}
        >
        <View style={styles.conatiner}>
        <FlatList 
            style={{marginTop:10}}
            showsVerticalScrollIndicator={false}
            data={address}
            renderItem={({ item }) => renderAddress(item)}
            keyExtractor={(item, index) => item.id } />

            <CustomButton btnText="ADD ADDRESS" 
                 onPress={() => props.navigation.navigate('AddAddress')}
                 customStyle={{elevation:10}}
                />
        </View>
        { common.loading  && <ProgressBar/> }
        </KeyboardAwareScrollView>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    conatiner:{
        flex:1,
       // backgroundColor:'white'
    }
})

export default Address;