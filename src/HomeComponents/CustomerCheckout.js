import React ,{useState,useEffect}  from 'react';
import {View , Text , TouchableOpacity,Image, StyleSheet} from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import CustomAddress from '../CustomUI/CustomAddress';
import { FlatList } from 'react-native-gesture-handler';
import { Card } from 'react-native-elements';
import AntDesign from 'react-native-vector-icons/AntDesign';
import {COLORS} from '../Utils/Utils';
import CustomButton from '../CustomUI/CustomButton';

import  {getCheckoutData,placeOrder,getDeliveryData, orderSuccessApi} from '../redux/actions/home';
import {showMessage} from '../Utils/ShowMessage';
import ApiUrl from '../Utils/ApiUrl';
import { useDispatch,useSelector } from 'react-redux';
import ProgressBar from '../Utils/ProgressBar';
import { emptyCart } from '../redux/actions/cart_action';
import DateTimePickerModal from "react-native-modal-datetime-picker";
import CustomTextInput from '../CustomUI/CustomTextInput';



const PAYMENT =[
    {'id':1, 'name':'Credit Card/Debit Card','selected':false},
    {'id':0, 'name':'Cash on Delivery','selected':false},
]

const CheckoutCustomer = (props) => {
    const [customerType , setCustomerType] = useState('');

    const [addresses , setAddresses] = useState([]);
    const [shipping ,setShipping] = useState([]);
    const [total ,setTotal] = useState(0);
    const [cart ,setCart] = useState([]);
    const [grandTotal ,setGrandTotal] = useState(0);
    const [estimatedTax ,setEstimatedTax] = useState(0);
    const [shippingCost ,setShippingCost] = useState(0);
    const [deliveryData ,setDeliveryData] = useState([]);

    const [isDatePickerVisible, setDatePickerVisibility] = useState(false);
    const [date, setDate] = useState('');

    const [payments ,setPayments] = useState(PAYMENT);
    const [loyaltySelected ,setLoyaltySelected] = useState(false);
    const [loyaltyPoint ,setLoyaltyPoint] = useState(null);
    const [selectedAddress ,setSelectedAddress] = useState(null);
    const [selectedDelivery ,setSelectedDelivery] = useState(null);
    const [selectedShipping ,setSelectedShipping] = useState(null);
    const [selectedPayment ,setSelectedPayment] = useState(null);

    //input
    const [company,setCompany] = useState("");
    const [errorCompanyMsg ,setErrorCompanyMsg ] = useState("");
    const [cvr,setCvr] = useState("");
    const [errorCvrMsg ,setErrorCvrMsg ] = useState("");
    const [rekv,setRekv] = useState("");
    const [errorRekvMsg ,setErrorRekvMsg ] = useState("");


    const common = useSelector(state => state.common);
    const userReducer = useSelector(state => state.user);
    const dispatch = useDispatch();

    function isEmoji(str) {
        var ranges = [
            '(?:[\u2700-\u27bf]|(?:\ud83c[\udde6-\uddff]){2}|[\ud800-\udbff][\udc00-\udfff]|[\u0023-\u0039]\ufe0f?\u20e3|\u3299|\u3297|\u303d|\u3030|\u24c2|\ud83c[\udd70-\udd71]|\ud83c[\udd7e-\udd7f]|\ud83c\udd8e|\ud83c[\udd91-\udd9a]|\ud83c[\udde6-\uddff]|[\ud83c[\ude01-\ude02]|\ud83c\ude1a|\ud83c\ude2f|[\ud83c[\ude32-\ude3a]|[\ud83c[\ude50-\ude51]|\u203c|\u2049|[\u25aa-\u25ab]|\u25b6|\u25c0|[\u25fb-\u25fe]|\u00a9|\u00ae|\u2122|\u2139|\ud83c\udc04|[\u2600-\u26FF]|\u2b05|\u2b06|\u2b07|\u2b1b|\u2b1c|\u2b50|\u2b55|\u231a|\u231b|\u2328|\u23cf|[\u23e9-\u23f3]|[\u23f8-\u23fa]|\ud83c\udccf|\u2934|\u2935|[\u2190-\u21ff])' // U+1F680 to U+1F6FF
        ];
        if (str.match(ranges.join('|'))) {
            return true;
        } else {
            return false;
        }
    }


    function fetchCheckout(){
        let formData = new FormData();
        formData.append("user_id",userReducer.user_details.id);
        dispatch(getCheckoutData(formData))
        .then(response=> {
            console.log(JSON.stringify(response))
            if(!response.error){
               // var category = 
               setAddresses(response.address)
               setShipping(response.shipping)
               setCart(response.cart)
               setTotal(response.total)
               setGrandTotal(response.grand_total)
               setEstimatedTax(response.estimated_tax)
               setLoyaltyPoint(response.loyalty_point.loyalty_point)
            
            }else{
                showMessage(response.message)
            }
        })
        .catch(error => {
            showMessage('Something went wrong,Try Again')
        })

    }


    function fetchDelivery(){
        dispatch(getDeliveryData())
        .then(response=> {
            //console.log(JSON.stringify(response))
            if(!response.error){
              
               setDeliveryData(response.data)
               if(response.data.length > 0){
                   response.data[0].selected = true;
                   setSelectedDelivery(response.data[0])
               }
            
            }else{
                showMessage(response.message)
            }
        })
        .catch(error => {
            showMessage('Something went wrong,Try Again')
        })

    }

    function getTomorrow() {
        const tomorrow = new Date();
        tomorrow.setDate(tomorrow.getDate() + 1); // even 32 is acceptable
        return `${tomorrow.getFullYear()} - ${tomorrow.getMonth() + 1} - ${tomorrow.getDate()}`;
    }


    function initialize(){
        console.log(getTomorrow())
        var customertype = props.route.params.type
        setCustomerType(customertype)
    }

    function resetData(){
       
        setSelectedPayment(null)
        setSelectedShipping(null)
        setSelectedAddress(null)
        setLoyaltySelected(false)
        setSelectedDelivery(null)
       // setPayments(PAYMENT)

       let copyPayment = [...payments];
        copyPayment.forEach((element,index) => {
            element.selected =  false;
        })

        setPayments(copyPayment);
        console.log('Resert00000-----',payments)
    }


    useEffect(() =>{
        const unsubscribe = props.navigation.addListener('focus', () => {
            fetchCheckout()
            fetchDelivery()
            initialize()
            resetData()
        }); 
        const unsubscribeTwo = props.navigation.addListener('blur', () => {
            resetData()
            console.log('Screen Unfocues')
        }); 
        
      },[props.navigation])

    const renderItem = (item,index) => {

        return <CustomAddress item={item} index={index}
            onSelectAddress={(id, index) => onSelectAddress(id,index)} 
        />
    }


    let onSelectShipping = (id,index) => {

        let copyShipping = [...shipping];
        copyShipping.forEach((element,index) => {
            element.selected =  false;
            if(element.id == id){
                console.log(element.selected);
                element.selected = true;
                setSelectedShipping(element)
                setShippingCost(element.price)
            }
        })

        setShipping(copyShipping);
    }

    const renderShipping = (item,index) => {

        return (
            <View style={{flexDirection:'row',justifyContent:'space-between',alignItems:'center',marginTop:20,marginBottom:20}}>
                        <Text style={styles.headingText}>{item.name}(DDK {item.price} )</Text>
                        <TouchableOpacity onPress={()=>onSelectShipping(item.id,index)} style={{marginRight:10}}>
                            {item.selected
                            ?
                            <AntDesign  name="checkcircle" color={COLORS.theme_green} size={20}  />
                            :
                            <AntDesign  name="checkcircle" color={"#a9a9a9"} size={20}  />
                            }
                        </TouchableOpacity>
                    </View>
        )
    }

    let onSelectAddress = (id,index) => {

        let copyAddress = [...addresses];
        copyAddress.forEach((element,index) => {
            element.selected =  false;
            if(element.id == id){
                console.log(element.selected);
                element.selected = true;
                setSelectedAddress(element)
            }
        })

        setAddresses(copyAddress);
    }


    const renderPayment= (item,index) => {
       
        return(
            <View style={{flexDirection:'row',justifyContent:'space-between',alignItems:'center',marginTop:20,marginBottom:20}}>
            <Text style={styles.headingText}>{item.name}</Text>
            <TouchableOpacity onPress={()=>onSelectPayment(item.id,index)}  style={{marginRight:10}}>
            {item.selected
                ?
                <AntDesign  name="checkcircle" color={COLORS.theme_green} size={20}  />
                :
                <AntDesign  name="checkcircle" color={"#a9a9a9"} size={20}  />
                }
            </TouchableOpacity>
        </View>
        )

    }


    let onSelectDelivery = (id,index) => {

        let copyPayment = [...deliveryData];
        copyPayment.forEach((element,index) => {
            element.selected =  false;
            if(element.id == id){
                console.log(element.selected);
                element.selected = true;
                setSelectedDelivery(element)
            }
        })

        setDeliveryData(copyPayment);
    }

    const renderDelivery= (item,index) => {
        return(
            <View style={{flexDirection:'row',justifyContent:'space-between',
            alignItems:'center',paddingTop:20,paddingBottom:20,backgroundColor:index == 0 ? '#ffcccb' : null}}>
                <View>
                <Text style={styles.headingText}>{item.timezone} 
                 <Text 
                 style={[styles.headingText,{color:'grey'}]}>({item.start_time} - {item.end_time})
                 </Text></Text>
                 <View style={{height:10}}/>
                 <Text style={styles.headingText}>DDK {item.price}</Text>
                </View>
            <TouchableOpacity onPress={()=>onSelectDelivery(item.id,index)}  style={{marginRight:10}}>
            {item.selected
                ?
                <AntDesign  name="checkcircle" color={COLORS.theme_green} size={20}  />
                :
                <AntDesign  name="checkcircle" color={"#a9a9a9"} size={20}  />
                }
            </TouchableOpacity>
        </View>
        )

    }

    let onSelectPayment = (id,index) => {

        let copyPayment = [...payments];
        copyPayment.forEach((element,index) => {
            element.selected =  false;
            if(element.id == id){
                console.log(element.selected);
                element.selected = true;
                setSelectedPayment(element)
            }
        })

        setPayments(copyPayment);
    }

    function isValid(){
        if(selectedAddress == null) {
            if(addresses.length == 0){
                showMessage("Add Address First")
                return false
            }
            else {
                 showMessage("Select Address First")
                 return false
            }
        }
        if(customerType == 'public'){
            if(company == '') {
                showMessage("Enter Company Name")
                setErrorCompanyMsg("Enter Company Name")
                return false
            }
            else if(isEmoji(company)) {
                showMessage("Enter Valid Company Name")
                setErrorCompanyMsg("Enter Valid Company Name")
                return false
            }
            else if(cvr == '') {
                showMessage("Enter CVR No.")
                setErrorCvrMsg("Enter CVR No.")
                return false
            }
            else if(isEmoji(cvr)) {
                showMessage("Enter Valid CVR No.")
                setErrorCvrMsg("Enter CVR No.")
                return false
            }
            else if(rekv == '') {
                showMessage("Enter Rekv No.")
                setErrorRekvMsg("Enter Rekv No.")
                return false
            }
            else if(isEmoji(rekv)) {
                showMessage("Enter Valid Rekv No.")
                setErrorRekvMsg("Enter Valid Rekv No.")
                return false
            }
        }
         if(!selectedPayment ) {
            showMessage("Select Payment method First")
            return false
        }
        // else if(date == '') {
        //     showMessage("Select Delivery Date First")
        //     return false
        // }
         if(!selectedDelivery) {
            showMessage("Select Delivery Time First")
            return false
        }
        // else if(selectedShipping == null) {
        //     showMessage("Select Shipping First")
        //     return false
        // }
        
        
        return true
    }

    function orderSucces(id){
   
        let formData = new FormData();
        formData.append("user_id",userReducer.user_details.id);
        formData.append("order_no",id);
        console.log(formData)
        dispatch(orderSuccessApi(formData))
        .then(response=> {
            if(!response.error){
                props.navigation.replace("OrderPlaced")
            }else{
                showMessage(response.message)
            }
        })
        .catch(error => {
            showMessage('Something went wrong,Try Again')
        })
}

    function next(){
        
        if(isValid()){
            console.log('-----',selectedPayment)
            let formData = new FormData();
            formData.append("user_id",userReducer.user_details.id);
            formData.append("payment_method",selectedPayment.id);
            formData.append("payable_amount",grandTotal);
            formData.append("grand_total",
                parseInt(total) + 
                parseInt(selectedAddress ? selectedAddress.delivery_charges : '0') + 
                parseInt((selectedDelivery ? selectedDelivery.price : 0)) -
                parseInt(loyaltySelected ? loyaltyPoint : 0)
                );
            formData.append("address_id",selectedAddress.id);
           // formData.append("shipping_id",selectedShipping.id);
            formData.append("delivery_charges",selectedAddress.delivery_charges);
            formData.append("extra_charge",selectedDelivery.price);
            formData.append("date",getTomorrow());
            formData.append("time",selectedDelivery.start_time + '-' + selectedDelivery.end_time);
            // formData.append("loyalty_points",loyaltySelected ? 0 : loyaltyPoint);
            if(loyaltySelected) formData.append("loyalty_points", 0 )
          

            formData.append("company",company);
            formData.append("cvr",cvr);
            formData.append("rekv",rekv);

            console.log('formData----',formData)

            dispatch(placeOrder(formData))
            .then(response=> {
                console.log(response)
                if(!response.error){

                    dispatch(emptyCart({}))
                    resetData()
                    if(selectedPayment.id == 1) {
                        //means your pay
                        var amount = response.amount
                        var order_no = response.order_no
                        var user_id = userReducer.user_details.id

                        var url = ApiUrl.base_url+ ApiUrl.payment + 
                                '?user_id='+ user_id +
                                '&amount='+ amount +
                                '&order_no='+ order_no 
                        props.navigation.replace("Payment",{'url':url,'order_no':order_no})
                    }
                    else{
                        //cash on delivery
                        var order_no = response.order_no
                        orderSucces(order_no)
                    }
                   
                }else{
                    showMessage(response.message)
                }
            })
            .catch(error => {
                console.log(error.message)
                showMessage('Something went wrong,Try Again')
            })
     
        }
    }


    function  convertDate(date) {
        var yyyy = date.getFullYear().toString();
        var mm = (date.getMonth()+1).toString();
        var dd  = date.getDate().toString();
    
        var mmChars = mm.split('');
        var ddChars = dd.split('');
    
        return yyyy + '-' + (mmChars[1]?mm:"0"+mmChars[0]) + '-' + (ddChars[1]?dd:"0"+ddChars[0]);
      }

      

    const handleConfirm = (date) => { 
        console.warn("A date has been picked: ", date);
        setDate(convertDate(date))
       
        hideDatePicker();
      };

      const hideDatePicker = () => {
        setDatePickerVisibility(false);
      };


      const showDatePicker = () => {
        setDatePickerVisibility(true);
      };

      const validator = (type,value) => {
        switch(type){
             case 'company' :{
               setCompany(value)
               if(!value){
                   setErrorCompanyMsg("Please Enter Company Name");
                   return false
                }
                setErrorCompanyMsg("");
                return true
            }
            case 'cvr' :{
                setCvr(value)
                if(!value){
                    setErrorCvrMsg("Please Enter CVR No.");
                    return false
                 }
                 setErrorCvrMsg("");
                 return true
             }
             case 'rekv' :{
                setRekv(value)
                if(!value){
                    setErrorRekvMsg("Please Enter Rekv No.");
                    return false
                 }
                 setErrorRekvMsg("");
                 return true
             }
            default :
                return true;
        }

    }

    return(
        <>
        <KeyboardAwareScrollView>
        <DateTimePickerModal
        isVisible={isDatePickerVisible}
        mode="date"
        minimumDate={new Date()}
        onConfirm={handleConfirm}
        onCancel={hideDatePicker}
      />

            <View style={styles.container}>
                {
                    addresses.length > 0
                    ? 
                    <View>
                     <Text style={styles.addressTitle}>My Addresses</Text>
                    <FlatList
                    horizontal
                    showsHorizontalScrollIndicator={false}
                    data={addresses}
                    renderItem={({ item,index }) =>  renderItem(item,index)}
                    keyExtractor={(item, index) => item.id }
                    />
                    </View>
                    : 
                    <CustomButton btnText="ADD ADDRESS" 
                    onPress={() => props.navigation.navigate('AddAddress')}
                    customStyle={{elevation:10}}
                   />
                }

                {
                    customerType == 'public' ?
                    <View>
                     <Text style={[styles.addressTitle,{marginTop:20}]}>Company Details</Text>

                        <CustomTextInput 
                            labelText="Company"
                            placeholder="Company"
                            value={company}
                            customTextInputStyle={errorCompanyMsg !== "" ?  {paddingBottom:2} : {paddingBottom:10}}
                            errorMsg={errorCompanyMsg}
                            onChangeText = {(value)=> {validator("company",value)}}
                        />

                         <CustomTextInput 
                            labelText="CVR No."
                            placeholder="CVR No."
                            value={cvr}
                            customTextInputStyle={errorCvrMsg !== "" ?  {paddingBottom:2} : {paddingBottom:10}}
                            errorMsg={errorCvrMsg}
                            onChangeText = {(value)=> {validator("cvr",value)}}
                        />

                       <CustomTextInput 
                            labelText="Rekv No."
                            placeholder="Rekv No."
                            value={rekv}
                            customTextInputStyle={errorRekvMsg !== "" ?  {paddingBottom:2} : {paddingBottom:10}}
                            errorMsg={errorRekvMsg}
                            onChangeText = {(value)=> {validator("rekv",value)}}
                        />
                    </View>
                    : null
                }
               
                <Text style={[styles.addressTitle,{marginTop:20}]}>Payment Method</Text>
                <Card
                containerStyle={{paddingTop:0,borderRadius:5,paddingBottom:0,margin:0,paddingLeft:0,paddingRight:0,elevation:10}}>
              
                <FlatList
                showsVerticalScrollIndicator={false}
                data={payments}
                ItemSeparatorComponent={()=>  <View style={styles.viewLine} />}
                renderItem={({ item,index }) =>  renderPayment(item,index)}
                keyExtractor={(item, index) => item.id }
                />
                </Card>





                <Text style={[styles.addressTitle,{marginTop:20}]}>Delivery Time</Text>
                <Card
                containerStyle={{paddingTop:0,borderRadius:5,paddingBottom:0,margin:0,paddingLeft:0,paddingRight:0,elevation:10}}>
              
                <FlatList
                showsVerticalScrollIndicator={false}
                data={deliveryData}
                ItemSeparatorComponent={()=>  <View style={styles.viewLine} />}
                renderItem={({ item,index }) =>  renderDelivery(item,index)}
                keyExtractor={(item, index) => item.id }
                />
                </Card>




                {/* <Text style={[styles.addressTitle,{marginTop:20}]}>Shipping Method</Text>
                <Card
                containerStyle={{paddingTop:0,borderRadius:5,paddingBottom:0,margin:0,paddingLeft:0,paddingRight:0,elevation:10}}>

                    <FlatList
                    showsVerticalScrollIndicator={false}
                    data={shipping}
                    ItemSeparatorComponent={()=>  <View style={styles.viewLine} />}
                    renderItem={({ item,index }) =>  renderShipping(item,index)}
                    keyExtractor={(item, index) => item.id }
                    />
                 
                
                </Card> */}
                <Text style={[styles.addressTitle,{fontSize:20,marginTop:20,color:'black',fontFamily:'roboto-bold'}]}>Order Summary</Text>
                <Card
                containerStyle={{paddingTop:0,borderRadius:5,paddingBottom:0,margin:0,paddingLeft:20,paddingRight:20,elevation:10}}>
                    <View style={styles.viewOrderSummary}>
                        <Text style={styles.orderProductText}>Product</Text>
                        <Text style={styles.orderProductText}>Total</Text>
                    </View>
                    {
                        cart.map(element => (
                            <View style={[styles.viewOrderSummary,{marginTop:0,marginBottom:10}]}>
                         <Text style={styles.orderProductItem}>{element.cartitem.name} X {element.quantity}</Text>
                        <Text style={styles.orderProductItem}>DDK {element.cartitem.price}</Text>
                         </View>

                        ))
                    }


                <View style={[styles.viewLine]} />
                    <View style={styles.viewOrderSummary}>
                        <Text style={styles.orderProductItem}>Total</Text>
                        <Text style={styles.orderProductItem}>DDK {total}</Text>
                    </View> 
                
                    <View style={[styles.viewLine]} />
                    <View style={styles.viewOrderSummary}>
                        <Text style={styles.orderProductItem}>Extra Charges</Text>
                        <Text style={styles.orderProductItem}>DDK {selectedDelivery ? selectedDelivery.price : '0'}</Text>
                    </View>
                    
                    <View style={[styles.viewLine]} />
                    <View style={styles.viewOrderSummary}>
                        <Text style={styles.orderProductItem}>Delivery Fees</Text>
                        <Text style={styles.orderProductItem}>DDK {selectedAddress ? selectedAddress.delivery_charges : '0'}</Text>
                    </View>

                    <View style={[styles.viewLine]} />
                    <View style={styles.viewOrderSummary}>
                        <Text style={styles.orderProductItem}>VAT (25%)</Text>
                        <Text style={styles.orderProductItem}>DDK {(parseInt(total))/4}</Text>
                    </View>

                    <View style={[styles.viewLine]} />
                    <View style={styles.viewOrderSummary}>
                        <Text style={styles.orderProductText}>Subtotal incl. VAT</Text>
                        <Text style={styles.orderProductText}>DDK {parseInt(total) +
                         parseInt(selectedAddress ? selectedAddress.delivery_charges : '0') + 
                         parseInt((selectedDelivery ? selectedDelivery.price : 0)) -
                         parseInt(loyaltySelected ? loyaltyPoint : 0)

                         }</Text>
                    </View>
                   
                </Card>

              {
                  loyaltyPoint ?
                  <View>
                  <View style={{height:10}}/>
                  <Card
                  containerStyle={{padding:11,borderRadius:5,elevation:10,margin:1,}}>
                      <View style={{flexDirection:'row',
                  alignItems:'center',justifyContent:'space-between'}}>
                      <Text>Use Loyalty Point ({loyaltyPoint})</Text>
                      {loyaltySelected
                              ?
                              <TouchableOpacity onPress={()=>setLoyaltySelected(!loyaltySelected)}>
                                  <AntDesign  name="checkcircle" color={COLORS.theme_green} size={20}  />
                              </TouchableOpacity>
                              :
                              <TouchableOpacity onPress={()=>setLoyaltySelected(!loyaltySelected)}>
                                  <AntDesign  name="checkcircle" color={"#a9a9a9"} size={20}  />  
                            </TouchableOpacity>
                              
                      }
                  </View>
                  </Card>
                  </View>
                  : null
              }

                <View style={{flex:1, justifyContent:'flex-end',alignItems:'center',marginTop:20}}>
                    <CustomButton btnText="CONFIRM ORDER" 
                            onPress={() => next()}
                            customStyle={{elevation:10}}
                            />
                </View>
                
            </View>
          
            
        </KeyboardAwareScrollView>
        { common.loading  && <ProgressBar/> }
        </>
    )
}

const styles  = StyleSheet.create({
    container:{
        flex:1,
        padding:20
    },
    addressTitle:{
        fontFamily:'roboto-medium',
        color:'grey',
        fontSize:15,
        marginBottom:15
    },
    viewLine:{
        width:'100%',
        height:1,
        backgroundColor:"grey"
    },
    headingText:{
        fontSize:14,
        color:'black',
        marginLeft:20,
        marginRight:10,
    },
    orderProductText:{
        fontFamily:'roboto-bold',
        fontSize:15,
        color:'black',
        textAlign:"left"
    },
    orderProductItem:{
        fontFamily:'roboto-regular',
        fontSize:15,
        color:'black'
    },
    viewOrderSummary :{
        flexDirection:'row',
        justifyContent:"space-between",
        alignItems:'center',
        marginLeft:20,
        marginRight:20,
        marginTop:20,
        marginBottom:20
    }
})



export default CheckoutCustomer;