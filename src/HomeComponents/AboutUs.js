import React ,{useState,useEffect} from 'react';
import {View ,Text, Image, TouchableOpacity,StyleSheet,Dimensions} from 'react-native';
import ProgressBar from '../Utils/ProgressBar';

import { useDispatch,useSelector } from 'react-redux';
import { COLORS } from '../Utils/Utils';
import  {getAboutUs} from '../redux/actions/home';
import {showMessage} from '../Utils/ShowMessage';
import HTML from 'react-native-render-html';


const AboutUs  = (props) => {

    const [data, setData] = useState('');

    const dispatch = useDispatch();
    const userReducer = useSelector(state => state.user);

    function fetchAboutUs(){
        dispatch(getAboutUs())
        .then(response=> {
            console.log(response)
            if(!response.error){
               // var category = 
                  setData(response.result.short_description)
            }else{
                showMessage(response.message)
            }
        })
        .catch(error => {
            showMessage('Something went wrong,Try Again')
        })

    }

    useEffect(() => { 
        fetchAboutUs()
    }, []);

    return(
        <View style={styles.container}>
            <Image source={require('../assets/logo2.png')} style={{marginTop:10,marginBottom:30,alignSelf:"center",width:'80%',height:100}} />
            <HTML html={data} imagesMaxWidth={Dimensions.get('window').width} />
        </View>
    )
}

const styles= StyleSheet.create({

    container:{
        flex:1,
        backgroundColor:'white', 
        padding:20
    },
    lahoreText:{
        fontSize:15,
        fontFamily:'roboto-bold',
        lineHeight:18
    },
    lahoreText1:{
        fontSize:13,
        fontFamily:'roboto-bold',
        lineHeight:18
    }
});

export default AboutUs;