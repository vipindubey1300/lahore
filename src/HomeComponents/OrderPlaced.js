import React ,{useState,useEffect} from 'react';
import {View ,Text, Image, TouchableOpacity,StyleSheet,Dimensions} from 'react-native';
import ProgressBar from '../Utils/ProgressBar';

import { useDispatch,useSelector } from 'react-redux';
import { COLORS } from '../Utils/Utils';
import  {getAboutUs} from '../redux/actions/home';
import {showMessage} from '../Utils/ShowMessage';
import HTML from 'react-native-render-html';
import CustomButton from '../CustomUI/CustomButton';


const OrderPlaced  = (props) => {

    const [data, setData] = useState('');

    const dispatch = useDispatch();
    const userReducer = useSelector(state => state.user);


    useEffect(() => { 
       
    }, []);

    return(
        <View style={styles.container}>
            <View style={styles.topContainer}>
            <Image source={require('../assets/tick.png')} style={{width:100,height:100,alignSelf:'center'}} />
            <Text style={{color:'white',fontSize:22,fontWeight:'bold',lineHeight:40}}>Order Placed</Text>
            <Text style={{color:'white',lineHeight:40}}>Your Order Has Been Placed Successfully</Text>
            </View>
            <View style={styles.bottomContainer}>
                <CustomButton btnText="CONTINUE SHOPPING" 
                onPress={() => props.navigation.navigate("Home")}
                customStyle={{elevation:10}}
                />
            </View>
        </View>
    )
}

const styles= StyleSheet.create({

    container:{
        flex:1,
        backgroundColor:'white', 
       
    },
    topContainer:{
        flex:7,
        backgroundColor:"#FD7F02",
        justifyContent:'center',
        alignItems:'center',
        width:'100%',
        borderBottomLeftRadius:60,
        borderBottomRightRadius:60
    },
    bottomContainer:{
        flex:3,
        justifyContent:'flex-end',
        padding:20
      
    }
});

export default OrderPlaced;