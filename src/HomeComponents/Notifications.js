import React ,{useState , useEffect} from 'react';
import {View , Text, TouchableOacity, Image, StyleSheet ,FlatList,TouchableOpacity} from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { Card } from 'react-native-elements';
import AntDesign from 'react-native-vector-icons/AntDesign';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'
import { COLORS } from '../Utils/Utils';
import  {getMyNotifications, getMyOrders} from '../redux/actions/home';
import {showMessage} from '../Utils/ShowMessage';
import { useDispatch,useSelector } from 'react-redux';
import ApiUrl from '../Utils/ApiUrl';
import  {updateCart} from '../redux/actions/home';
import  {cancelOrder} from '../redux/actions/home';
import ProgressBar from '../Utils/ProgressBar';


const Notifications = (props) => {

    const [notificationHistory , setNotificationHistory] = useState([]);


    function renderItem (item)  {

        return(
            <TouchableOpacity style={styles.notificationContainer}>
    
                <View style={styles.notiLeft}>
                <Image 
                source={require('../assets/bell.png')}
                style={styles.icon} 
                />
                </View>
    
                <View style={styles.notiRight}>
                    <View style={{flexDirection:'row',
                    alignItems:'center',justifyContent:'space-between',margin:3}}>
                        <Text style={{fontWeight:'700'}}>Order No : {item.order_number}</Text>
                        <Text style={{fontWeight:'700'}}>{item.created_at.substring(11,16)}</Text>
                    </View>
                    <Text style={styles.notiText}> {item.name} </Text>
                </View>
           
            </TouchableOpacity>
        )
      }

    const common = useSelector(state => state.common);
    const userReducer = useSelector(state => state.user);
    const dispatch = useDispatch();


    function fetchNotifications(){
        let formData = new FormData();

        formData.append("user_id",userReducer.user_details.id);
        dispatch(getMyNotifications(formData))
        .then(response=> {
            console.log(JSON.stringify(response))
            if(!response.error){
               // var category = 
               setNotificationHistory(response.data)
            }else{
                showMessage(response.message)
            }
        })
        .catch(error => {
            showMessage('Something went wrong,Try Again')
        })

    }

    useEffect(() => { 
         fetchNotifications()
    }, []);


    return(
        <>
        <KeyboardAwareScrollView
            contentContainerStyle={{flexGrow:1}}
        >
            <View style={styles.container}>

            {!common.loading  && notificationHistory.length == 0 
            ?
            <Image resizeMode='contain'
            source={require('../assets/notification.png')} 
            style={{alignSelf:"center",width:300,height:300}} />

            :   <FlatList 
            data={notificationHistory}
            renderItem={({item}) => 
                renderItem(item)}
            />
           }
              
                
            </View>
        </KeyboardAwareScrollView>
        { common.loading  && <ProgressBar/> }
        </>
    )


}

const styles  = StyleSheet.create({
    container : {
        flex:1,
        backgroundColor:'white',
        paddingTop:20
    },
    boldText:{
        fontFamily:'roboto-bold',
        fontSize:14,
        color:"black"
    },
    labelText:{
        fontWeight:'500',
        fontSize:16,
        lineHeight:28,margin:15,
        
        
      },
      notificationContainer:{
          width:'95%',
          alignSelf:'center',
          backgroundColor:'white',
          borderRadius:10,
          borderColor:'grey',
          borderWidth:1,
          paddingHorizontal:10,
          paddingVertical:13,
          marginHorizontal:8,
          marginVertical:7,
          flexDirection:'row',
          alignItems:'center'
      },
      notiLeft:{
          flex:1.7,justifyContent:'center',alignItems:'center'
      },
      icon:{
          height:30,width:30,
      },
      notiRight:{
          flex:8.2
      },
      notiText:{
          color:'black'
      }
     
})
export default Notifications;