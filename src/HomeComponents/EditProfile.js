import React ,{useState , useEffect } from 'react';
import {View , Text , Image , TouchableOpacity,StyleSheet, SafeAreaView} from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import CustomTextInput from '../CustomUI/CustomTextInput';
import CustomButton from '../CustomUI/CustomButton';
import { COLORS } from '../Utils/Utils';

import  {getProfile,editProfile} from '../redux/actions/user_action';
import {showMessage} from '../Utils/ShowMessage';
import ApiUrl from '../Utils/ApiUrl';
import { useDispatch,useSelector } from 'react-redux';
import ProgressBar from '../Utils/ProgressBar';
import AsyncStorage from '@react-native-community/async-storage';
import { UPDATE_USER } from '../redux/types';

const EditProfile = (props) => {

    const [email,setEmail] = useState("");
    const [password ,setPassword ] = useState("");
    const [phone ,setPhone] = useState("");
    const [username ,setUsername ] = useState("");
    const [userimage ,setUserimage ] = useState("");

    const [errorTrue ,setErrorTrue] = useState(false);
    const [errorUserNameMsg ,setErrorUserNameMsg ] = useState("");
    const [errorEmailMsg ,setErrorEmailMsg ] = useState("");
    const [errorPhoneMsg ,setErrorPhonesg ] = useState("");
    const [errorPasswordMsg ,setErrorPasswordMsg ] = useState("");

    const common = useSelector(state => state.common);
    const userReducer = useSelector(state => state.user);
    const dispatch = useDispatch();

    const validator = (type,value) => {

        console.log("type",type);

        switch(type){

            case 'user_name':

                setUsername(value)
                if(!value){
                    setErrorUserNameMsg("Please Enter User Name");
                    setErrorTrue(true);
                   
                    return false
                }
                setErrorUserNameMsg("");
                setErrorTrue(false);
                
                return true
            
             case 'email' :{

              
                setEmail(value)
               if(!value){
                    setErrorEmailMsg("Please Enter Email");
                    setErrorTrue(true);
                   return false

                }
                else if(!(/[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/.test(
                value))){
                    setErrorEmailMsg("Please Enter Valid Email");
                    setErrorTrue(true);
                    return false
                }
                setErrorEmailMsg("");
                setErrorTrue(false);
                return true
            }
           
            case 'mobile' :{

                setPhone(value)
               if(!value){
                setErrorPhonesg("Please Enter Mobile Number");
                setErrorTrue(true);
                   return false

               }
               else if(value.length < 7 || value.length >15){
                setErrorPhonesg("Mobile Number Should Be 7-15 Digits");
                setErrorTrue(true);
                return false
              }
              else if(value == '0000000'){
                setErrorPhonesg("Mobile Number Should Be Valid");
                setErrorTrue(true);
                return false
            }

              else if(value == '00000000'){
                setErrorPhonesg("Mobile Number Should Be Valid");
                setErrorTrue(true);
                return false
                
            }
            else if(value == '000000000'){
                setErrorPhonesg("Mobile Number Should Be Valid");
                setErrorTrue(true);
                return false
                
            }
            else if(value == '0000000000'){
                setErrorPhonesg("Mobile Number Should Be Valid");
                setErrorTrue(true);
                return false
                
            }
            else if(value == '00000000000'){
                setErrorPhonesg("Mobile Number Should Be Valid");
                setErrorTrue(true);
                return false
                
            }
            else if(value == '000000000000'){
                setErrorPhonesg("Mobile Number Should Be Valid");
                setErrorTrue(true);
                return false
                
            }

               setErrorPhonesg("");
               setErrorTrue(false);
               return true
            }
           
            default :
                return true;
        }

    }

    function fetchProfile(){
        dispatch(getProfile(userReducer.user_details.id))
        .then(response=> {
            console.log(response)
            if(!response.error){
                setEmail(response.result.email)
                setUsername(response.result.name)
                setPhone(response.result.mobile)
                setUserimage(response.result.user_image)
                setPassword(response.result.txtpassword)
            }else{
                showMessage(response.message)
            }
        })
        .catch(error => {
            showMessage('Something went wrong,Try Again')
        })
    }

    const edit = async() =>{
        if(validator('user_name',username) && validator('email',email)  && validator('mobile',phone)){

            var formData = new FormData()
            formData.append('user_id',userReducer.user_details.id)
            formData.append('name',username)
            formData.append('contact_number',phone)
            console.log(formData)
       // formData.append('profile_img',phone)
            dispatch(editProfile(formData))
            .then(response=> {
                console.log(response)
                if(!response.error){
                    AsyncStorage.setItem('name',username)
                    dispatch({ type:UPDATE_USER,  user:{'name':username} })
                props.navigation.navigate("Home")
                }else{
                    showMessage(response.message)
                }
            })
            .catch(error => {
                showMessage('Something went wrong,Try Again')
            })
     }
    }

    useEffect(() => { 
        fetchProfile()
    }, []);

 

    return(
        <SafeAreaView style={{backgroundColor:"white",flex:1}}>
        
        <KeyboardAwareScrollView >
            <View style={styles.container}>
                <TouchableOpacity style={styles.circularImage} >
                    <Image source={{uri:ApiUrl.image_base_url + userimage}} style={styles.imgStyle} resizeMode="contain"/>
                </TouchableOpacity>        

                <CustomTextInput 
                labelText="User Name"
                placeholder="Enter User Name"
                isSecureText={false}
                value={username}
                //errorTrue={errorTrue}
                errorMsg={errorUserNameMsg}
                customTextInputStyle={errorUserNameMsg !== "" ?  {paddingBottom:2} : {paddingBottom:10}}
                onChangeText = {(value)=> {validator('user_name',value)}}
            />
            <CustomTextInput 
                labelText="E-mail"
                placeholder="Enter Email"
                isSecureText={false}
                value={email}
                editable={false}
                //errorTrue={errorTrue}
                customTextInputStyle={errorEmailMsg !== "" ?  {paddingBottom:2} : {paddingBottom:10}}
                errorMsg={errorEmailMsg}
                onChangeText = {(value)=> {validator('email',value)}}
            />
            <CustomTextInput 
                labelText="Phone Number"
                placeholder="Enter Phone Number"
                isSecureText={false}
                value={phone}
                keyboardType="phone-pad"
                //errorTrue={errorTrue}
                customTextInputStyle={errorPhoneMsg !== "" ?  {paddingBottom:2} : {paddingBottom:10}}
                errorMsg={errorPhoneMsg}
                onChangeText = {(value)=> {validator('mobile',value)}}
            />

            <CustomTextInput 
                labelText="Password"
                editable={false}
                placeholder="Enter Password"
                customTextInputStyle={{width:"85%"}}
                isSecureText={true}
                secureTextEntry={true}
                value={password}
                //errorTrue={errorTrue}
                errorMsg={errorPasswordMsg}
                customTextInputStyle={errorPasswordMsg !== "" ?  {paddingBottom:2} : {paddingBottom:10}}
                onChangeText = {(value)=> {validator('password',value)}}
                />

                <CustomButton 
                onPress={() => edit()}
                btnText="UPDATE" 
                customStyle={{marginLeft:10,marginRight:10,marginTop:20}} />



            </View>
        </KeyboardAwareScrollView>
        { common.loading  && <ProgressBar/> }
            
        </SafeAreaView>
    )


}

const styles = StyleSheet.create({

    container:{
        flex:1,
        padding:20,
        paddingTop:30,
        backgroundColor:'white',
    },
    imgStyle:{
        width:80,
        height:80,
        borderRadius:80/2,
        alignSelf:"center",
        // borderColor:COLORS.desp_grey,
        // borderWidth:0.5
    },
    circularImage:{
       
        width:90,
        height:90,
        alignSelf:"center",
        alignItems:'center',
       justifyContent:"center",
       borderRadius:180/2,
       borderColor:COLORS.desp_grey,
       borderWidth:0.5,
       overflow:'hidden'
    },
})

export default EditProfile;