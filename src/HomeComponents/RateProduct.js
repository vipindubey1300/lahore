import React ,{useState,useEffect} from 'react';
import {View ,Text, Image, TouchableOpacity,StyleSheet, Dimensions, TextInput} from 'react-native';
import ProgressBar from '../Utils/ProgressBar';
import ExpandableListView from 'react-native-expandable-listview';
import { useDispatch,useSelector } from 'react-redux';
import { COLORS } from '../Utils/Utils';
import  {rateProduct} from '../redux/actions/home';
import CustomButton from '../CustomUI/CustomButton';
import { showMessage } from '../Utils/ShowMessage';
import { Rating, AirbnbRating } from 'react-native-ratings';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import InputScrollView from 'react-native-input-scroll-view';
import { Keyboard } from 'react-native';
import { TouchableWithoutFeedback } from 'react-native-gesture-handler';


const RateProduct  = (props) => {
    

    const dispatch = useDispatch();
    const userReducer = useSelector(state => state.user);
    const common = useSelector(state => state.common);

    const [rating ,setRating ] = useState(2.5);
    const [comment ,setComment ] = useState('');


    function rate(){
        Keyboard.dismiss()
        if(comment.toString().trim().length == 0) showMessage("Enter review first")
        else{
            var formData = new FormData()
        formData.append('user_id',userReducer.user_details.id)
        formData.append('item_id',props.route.params.id)
        formData.append('rate',rating)
        formData.append('review',comment)
     console.log(formData)

        dispatch((rateProduct(formData)))
        .then(response=> {
            console.log(response)
            if(!response.error){
              showMessage('Rated Successfully.',false)
              props.navigation.pop()
            }else{
                showMessage(response.meaasge)
            }
        })
        .catch(error => {
            showMessage('Something went wrong,Try Again')
        })

        }
    }

    useEffect(() => { 
    }, []);



   function ratingCompleted(rating) {
        console.log("Rating is: " + rating)
        setRating(rating)
      }
       
    return(
        <InputScrollView keyboardOffset={400} style={styles.container}>
         <View style={{height:20}}></View>
         <View style={{height:'100%'}} >
         <View style={{margin:10}}>
            <Text style={styles.heading}>Ratings</Text>
            <Rating
                type='custom'
                ratingColor='green'
                ratingBackgroundColor='#c8c7c8'
                ratingCount={5}
                tintColor={'#fff'}
                startingValue={5}
                fractions={1}
                imageSize={25}
                onFinishRating={ratingCompleted}
                style={{ paddingVertical: 10 ,alignSelf:'flex-start'}}
            />
            </View>
            <View style={{margin:10}}/>

            <View style={{width:Dimensions.get('window').width , height:22,backgroundColor:'lightgrey'}}/>


            <View style={{margin:10}}>
            <Text style={styles.heading}>Review</Text>
            <TextInput
                style={{
                    borderWidth: 1,  // size/width of the border
                    borderColor: 'lightgrey',  // color of the border
                    paddingLeft: 10,
                    height: 130,
                    textAlignVertical:'top'
                }}
                placeholder="Write here.."
                onChangeText={e => {
                    setComment(e)
                }}
                value={comment}
                multiline={true}
                />
            </View>
            <View style={{margin:10}}/>
          
          <CustomButton btnText="RATE" 
                 onPress={() =>{rate()}}
                 customStyle={{elevation:10,width:'80%',alignSelf:'center',marginBottom:30}}
         />
         
          </View>
          { common.loading  && <ProgressBar/> }

        </InputScrollView>
    )
}

const styles= StyleSheet.create({

    container:{
        flex:1,
        backgroundColor:'white', 
    },
    lahoreText:{
        fontSize:15,
        fontFamily:'roboto-bold',
        lineHeight:18
    },
    lahoreText1:{
        fontSize:13,
        fontFamily:'roboto-bold',
        lineHeight:18
    },
    heading:{
        fontSize:18,
        fontWeight:'bold',
        margin:10
    }
});

export default RateProduct;