
import React ,{useState, useEffect} from 'react';
import {View , Text , Image,FlatList , 
    TouchableOpacity, StyleSheet, SafeAreaView,Alert} from 'react-native';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import ProductItem from '../CustomUI/ProductItem';
import ProductItemCartOrWishlist from '../CustomUI/ProductItemCartOrWishlist';
import CustomButton from '../CustomUI/CustomButton';

import  {getCartItems} from '../redux/actions/home';
import {showMessage} from '../Utils/ShowMessage';
import ApiUrl from '../Utils/ApiUrl';
import { useDispatch,useSelector } from 'react-redux';
import ProgressBar from '../Utils/ProgressBar';
import { removeCartItem } from '../redux/actions/cart_action';


const Cart = (props) => {


    const [cartItems, setCartItems] = useState([]);


    const common = useSelector(state => state.common);
    const userReducer = useSelector(state => state.user);
    const dispatch = useDispatch();


    function fetchCartItems(){
        let formData = new FormData();
        formData.append("single",2);
        formData.append("user_id",userReducer.user_details.id);
        dispatch(getCartItems(formData))
        .then(response=> {
            console.log(response)
            if(!response.error){
               // var category = 
                  setCartItems(response.result)
                 

            }else{
                showMessage(response.message)
            }
        })
        .catch(error => {
            showMessage('Something went wrong,Try Again')
        })

    }

    useEffect(() => { 
        fetchCartItems()
    }, []);


    function onDeleteItem(item){
        console.log('item deleted---')
        var temp = cartItems.filter(i => i.id != item.id)
        setCartItems(temp)
        console.log(temp)
        dispatch(removeCartItem({'id':item.item_id}))
        
        
    }

    const renderProductItem = (item) => {

        return(
                <TouchableOpacity onPress={() => props.navigation.navigate('DetailsPage',{name:item.cartitem.name ,id:item.cartitem.id})}>
                    {/* <ProductItem  {...props} item={item}/> */}
                    <ProductItemCartOrWishlist onDelete ={onDeleteItem}
                     {...props} item={item} showWishList={1}/>
                </TouchableOpacity>
              
          
       
        )
    }

    return(
        <SafeAreaView style={{backgroundColor:'white',flex:1}}>
        <KeyboardAwareScrollView 
        contentContainerStyle={{flexGrow:1}}
        >
        <View style={styles.conatiner}>

        {!common.loading  && cartItems.length == 0 
            ?
            <Image resizeMode='contain'
            source={require('../assets/cart-empty.png')} 
            style={{alignSelf:"center",width:300,height:300}} />

            :   
            <FlatList 
            style={{marginTop:10}}
            showsVerticalScrollIndicator={false}
            data={cartItems}
            renderItem={({ item }) => renderProductItem(item)}
            keyExtractor={(item, index) => item.id } />
           }
      
        </View>
        {
            cartItems.length == 0 ?
            null :
            <View style={{flex:1, justifyContent:'flex-end',alignItems:'center'}}>
        <CustomButton btnText="NEXT" 
                 onPress={() =>{
                     if(cartItems.length > 0)  props.navigation.navigate('CustomerType')
                     else showMessage("No Items in Cart")
                 }}
                 customStyle={{elevation:10}}
                />
        </View>

        }
        
        </KeyboardAwareScrollView>
        { common.loading  && <ProgressBar/> }
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    conatiner:{
        flex:1,
       // backgroundColor:'white'
     
    }
})

export default Cart;