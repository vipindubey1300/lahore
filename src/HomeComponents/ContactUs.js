import React ,{useState, useEffect} from 'react';
import {View , Text , TouchableOpacity, SafeAreaView,StyleSheet } from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import CustomTextInput from '../CustomUI/CustomTextInput';
import CustomButton from '../CustomUI/CustomButton';
import ProgressBar from '../Utils/ProgressBar';
import { useDispatch,useSelector } from 'react-redux';
import  {contactUs} from '../redux/actions/home';
import {showMessage} from '../Utils/ShowMessage';


const ContactUs = (props) => {

    const [title,setTitle] = useState("");
    const [errorTitlelMsg ,setErrorTitleMsg ] = useState("");

    const [message , setMessage] = useState("");
    const [errorMsg , setErrorMsg] = useState("");

    const common = useSelector(state => state.common);
    const userReducer = useSelector(state => state.user);
    const dispatch = useDispatch();

    const validator = (type,value) => {
        console.log("type",type);
        switch(type){
             case 'title' :{
                setTitle(value)
               if(!value){
                setErrorTitleMsg("Please Enter title");
                   return false
                }
                setErrorTitleMsg("");
                return true
            }
            case 'message' :{
                setMessage(value)
               if(!value){
                setErrorMsg("Please Enter Message");
                   return false
                }
                setErrorMsg("");
                return true
            }
            
           
            default :
                return true;
        }

    }


    const onSubmit = async() => {

        if(validator('title',title) && validator('message',message)){
            let formdata = new FormData();
            formdata.append("user_id",userReducer.user_details.id);
            formdata.append("title",title);
            formdata.append("message",message);
           
            dispatch(contactUs(formdata))
            .then(response=> {
                console.log("contactUs REsponse ---->>>>",response)
                if(!response.error){
                    showMessage('Query Submitted Successfully.',false)
                    setMessage('')
                    setTitle('')
                }else{
                    showMessage(response.message)
                }

            })
            .catch(error => {
                showMessage('Something went wrong,Try Again')
            })
         }
        }

    return(
        <SafeAreaView style={{backgroundColor:'white',flex:1}}>
            <KeyboardAwareScrollView
            contentContainerStyle={{flexGrow:1}}
            >
                <View style={styles.container}>
                    <CustomTextInput
                      labelText="Title"
                      placeholder="Enter Title"
                      isSecureText={false}
                      value={title}
                      customTextInputStyle={errorTitlelMsg !== "" ?  {paddingBottom:2} : {paddingBottom:10}}
                      errorMsg={errorTitlelMsg}
                      onChangeText = {(value)=> {validator("title",value)}}
                    />

                    <CustomTextInput
                      labelText="Message"
                      placeholder="Enter Message"
                      isSecureText={false}
                      multiline={true}
                      value={message}
                      customTextInputStyle={errorMsg !== "" ?  {paddingBottom:2,height:120,} : {paddingBottom:10,height:120}}
                      errorMsg={errorMsg}
                      onChangeText = {(value)=> {validator("message",value)}}
                    />
                    <CustomButton btnText="SUBMIT" 
                        onPress={() => onSubmit()}
                        customStyle={{marginLeft:10,marginRight:10,marginTop:120}}/>


                </View>
            </KeyboardAwareScrollView>
            { common.loading  && <ProgressBar/> }
        </SafeAreaView>
       
    )

}

const styles =  StyleSheet.create({

    container:{
        flex:1,
        padding:20
    }
})

export default ContactUs;