import React ,{useState,useEffect}from 'react';
import {Text, StyleSheet,View, FlatList, SafeAreaView,Image, Dimensions} from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import CustomBanner from '../CustomUI/CustomBanner';
import CustomSearch from '../CustomUI/CustomSearch';
import CircularImage from '../CustomUI/CircularImage';
import ProductItem from '../CustomUI/ProductItem';
import { TouchableOpacity } from 'react-native-gesture-handler';
import ProgressBar from '../Utils/ProgressBar';

import { useDispatch,useSelector } from 'react-redux';

import  {getCartItems, getHomeProducts, updateCartData} from '../redux/actions/home';
import user_reducer from '../redux/reducers/user_reducer';
import {showMessage} from '../Utils/ShowMessage';
import ApiUrl from '../Utils/ApiUrl';
import { addCartItem, updateCartCount } from '../redux/actions/cart_action';
import { COLORS } from '../Utils/Utils';


const Home = (props) => {

    const [categories, setCategories] = useState([]);
    const [banners, setBanner] = useState([]);
    const [topProducts, setTopProducts] = useState([]);


    const common = useSelector(state => state.common);
    const userReducer = useSelector(state => state.user);
    const dispatch = useDispatch();


    function fetchHomeData(){
       
        dispatch(getHomeProducts(userReducer.user_details.id))
        .then(response=> {
            console.log("Home REsponse ---->>>>")
            if(!response.error){
                var itemsInCart = response.Items_in_cart
                var notificationCount = response.notification_count
                var banner = response.banner
                var popular = response.popular.data
                var category = response.category.data

                setCategories(category)
                setTopProducts(popular)
                setBanner(banner)
            }else{
                showMessage(response.message)
            }

        })
        .catch(error => {
            showMessage('Something went wrong,Try Again')
        })

    }

    const onSubmitLogin = async() =>{}

    function fetchCartCount(){
        let formData = new FormData();
        formData.append("single",2);
        formData.append("user_id",userReducer.user_details.id);
        dispatch(getCartItems(formData))
        .then(response=> {
           // console.log('response of cart count---',JSON.stringify(response))
            if(!response.error){
                let items = []
                //response.result.map(e => items.push({'id':e.cartitem.id}))
                response.result.map(e =>  dispatch(addCartItem({'id':e.cartitem.id})))
                //console.log(items)
               
            }else{
                showMessage(response.message)
                
            }
        })
        .catch(error => {
            console.log(error.message)
            showMessage('Something went wrong,Try Again')
        })

    }

    useEffect(() => { 
        fetchHomeData()
        fetchCartCount()
    }, []);

    const renderCategoriesItem = (item) => {

        return(
            <TouchableOpacity onPress={() => props.navigation.navigate('ListingPage',{name:item.name ,id:item.id})} >
            <View style={{flexDirection:"row",justifyContent:"center",alignItems:"center"}}>
                <CircularImage imagePath={ApiUrl.image_base_url + item.icon_image} typeName={item.name} customStyles={{backgroundColor:'white'}} />
            </View>
            </TouchableOpacity>

        )

    }

    const renderProductItem = (item) => {

        return(
                <TouchableOpacity onPress={() => props.navigation.navigate('DetailsPage',{name:item.name ,id:item.id})}>
                    <ProductItem  {...props} item={item}/>
                </TouchableOpacity>
       
        )
    }

    return(
        <SafeAreaView style={{backgroundColor:'white',flex:1}}>
        <KeyboardAwareScrollView >
            <View>
                <CustomBanner bannerItems={banners}/>
                {/* <View style={{margin:10}}> */}
                        <TouchableOpacity 
                        style={{
                            width:Dimensions.get('window').width * 0.96,
                            backgroundColor:COLORS.search_color,
                            height:50,
                            borderRadius:25,
                            flexDirection:'row',
                            margin:10,alignSelf:'center'
                           
                        }} onPress={()=> {props.navigation.navigate("Search")}}>
                        <View  style ={styles.textInputStyle}>
                            <Text>Search here ...</Text>
                        </View>

                        <View style={styles.roundView}>
                            <Image source={require('../assets/search.png')} style={{width:30,height:30,alignSelf:'center'}} />
                        </View>

                        </TouchableOpacity>
                    <View style={{flexDirection:"row",marginTop:20,marginLeft:10,marginRight:10,marginBottom:10,justifyContent:"space-between",alignItems:"center"}}>
                        <Text style={styles.textStyle}>Categories</Text>
                        <TouchableOpacity onPress={() => props.navigation.push('Categories')}>
                        <Text style={styles.ViewMoreText}>View All</Text>
                        </TouchableOpacity>
                    </View>
                    <FlatList 
                    showsHorizontalScrollIndicator={false}
                    horizontal
                    data={categories}
                    renderItem={({ item }) => renderCategoriesItem(item)}
                    keyExtractor={(item, index) => item.id } />
                    <View style={{flexDirection:"row",marginTop:20,marginLeft:10,marginRight:10,marginBottom:10,justifyContent:"space-between",alignItems:"center"}}>
                        <Text style={styles.textStyle}>Top Products</Text>
                        <TouchableOpacity onPress={() => {props.navigation.navigate('AllProducts')}}>
                        <Text style={styles.ViewMoreText}>View All</Text>
                        </TouchableOpacity>
                    </View>
                    <FlatList 
                    style={{marginTop:10}}
                    showsVerticalScrollIndicator={false}
                    data={topProducts}
                    renderItem={({ item }) => renderProductItem(item)}
                    keyExtractor={(item, index) => item.id } />

                  
                {/* </View> */}
            </View>
        </KeyboardAwareScrollView>
        { common.loading  && <ProgressBar/> }
        </SafeAreaView>
    )
}

const styles =  StyleSheet.create({

    conatiner :{
        flex:1,
      //  backgroundColor:'white',
       
    },
    textStyle:{
        fontFamily:'roboto-bold',
        fontSize:17,

    },
    ViewMoreText:{
        color:'grey',
        fontSize:13,
        fontFamily:'roboto-regular'
    },
    textInputStyle:{
        marginLeft:10,
         flex:7,
        fontSize:15,
        justifyContent:'center',paddingHorizontal:10
    },
    roundView:{
        backgroundColor:COLORS.theme_orange,
      justifyContent:'center',
      alignItems:'center',
        borderRadius:25,
        flex:2.5
    }

    
})

export default Home;