import React ,{useState , useEffect,useRef } from 'react';
import {View , Text ,Image , TouchableOpacity, 
    StyleSheet, Dimensions, ColorPropType,FlatList} from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { Card } from 'react-native-elements';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import { SafeAreaView } from 'react-native-safe-area-context';
import ProgressBar from '../Utils/ProgressBar';

import { useDispatch,useSelector } from 'react-redux';

import  {getItemDetails,addToCart,addToWishlist} from '../redux/actions/home';
import user_reducer from '../redux/reducers/user_reducer';
import {showMessage} from '../Utils/ShowMessage';
import ApiUrl from '../Utils/ApiUrl';
import QuantityComponent from '../CustomUI/QuantityComponent';
import { addCartItem, updateCartCount } from '../redux/actions/cart_action';
import { Rating, AirbnbRating } from 'react-native-ratings';
import Stars from 'react-native-stars';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';


const DetailsPage = (props) => {
    const [details, setDetails] = useState('');
    const [isWishlist, setWishlist] = useState(false);
    const [isCart, setCart] = useState(false);
    const [avatar, setAvatar] = useState('');
    const [averageRating, setAverageRating] = useState(0.0);
    const [reviews, setReviews] = useState([]);
    const [quantity, setQuantity] = useState(1);

    const quantityComp = useRef();


    const common = useSelector(state => state.common);
    const userReducer = useSelector(state => state.user);
    const cartReducer = useSelector(state => state.cart);

    const dispatch = useDispatch();

    const fetchDetails = async(id) =>{
        dispatch(getItemDetails(id,userReducer.user_details.id))
        .then(response=> {
            if(!response.error){
                console.log(response)
                setDetails(response.result[0])
                setWishlist(response.is_whislist == 0 ? false : true)
                setReviews(response.reviews)
                setAvatar(response.image)
                if(response.reviews.length > 0){
                    var temp = 0.0
                    response.reviews.map(e =>{
                        temp = temp + parseFloat(e.rate)
                    })
                    setAverageRating((temp/response.reviews.length))
                }
            }else  showMessage(response.message)
        })
        .catch(error => {
            showMessage('Something went wrong,Try Again')
        })
    }

    const addCart = async(id) =>{
      if(details != ''){
        let formData = new FormData();
        formData.append("item_id",props.route.params.id);
        formData.append("user_id",userReducer.user_details.id);
        formData.append("quantity",quantityComp.current.getQuantity());
        formData.append("amount",details.price);
        formData.append("single",2);
        formData.append("category_id",details.cate_id);

        console.log(formData)
        dispatch(addToCart(formData))
        .then(response=> {
            if(!response.error){
                console.log(response)
                let item = {'id': props.route.params.id}
                dispatch(addCartItem(item))
                showMessage(response.message,false)
            }else  showMessage(response.message)
        })
        .catch(error => {
            showMessage('Something went wrong,Try Again')
        })
      }
    }

    const addWishlist = async() =>{
        if(details != ''){
           
          let formData = new FormData();
          formData.append("item_id",props.route.params.id);
          formData.append("user_id",userReducer.user_details.id);
          //formData.append("status",1); //1 add 2 delete
          formData.append("status",isWishlist ? 2 : 1);
          console.log(formData)
          dispatch(addToWishlist(formData))
          .then(response=> {
              if(!response.error){
                  console.log(response)
                  showMessage(response.message,false)
                  isWishlist ? setWishlist(false) : setWishlist(true)
                  
              }else  showMessage(response.message)
          })
          .catch(error => {
              showMessage('Something went wrong,Try Again')
          })
        }
      }
    
    useEffect(() => { 
        props.navigation.setOptions({ title: props.route.params.name.length > 17 ?
             (props.route.params.name.substring(0,16)+'..') : props.route.params.name })
        fetchDetails(props.route.params.id)
        setQuantity(quantityComp.current.getQuantity())
    }, []);

    function onQuantityChange(value){
        console.log('----',value)
        setQuantity(value)
    }


    function renderItem (item)  {

        return(
            <View style={{width:Dimensions.get('window').width * 0.97 ,margin:10}}>
            <View style={{flexDirection:'row',width:'100%',justifyContent:'space-between'}}>

            <View style={{flexDirection:'row'}}>
                <View style={{width:45,height:45,borderRedius:50}}>
                <Image source={{uri:avatar}} style={{ width: '100%', height: '100%'}} />
                </View>


              <View style={{marginHorizontal:10,justifyContent:'space-evenly'}}>
                <Text style={{fontWeight:'bold'}}>{item.name}</Text>
               
                <Stars
                default={2.5}
                count={5}
                display={item.rate}
                disabled
                half={true}
                spacing={2.3}
                starSize={50}

                fullStar={<Icon name={'star'} style={{color:'green', textShadowColor: 'grey',
                textShadowOffset: {width: 1, height: 1},
                textShadowRadius: 2,}}/>}

                emptyStar={<Icon name={'star-outline'} style={{color:'grey'}}/>}

                halfStar={<Icon name={'star-half'} style={{color:'green', textShadowColor: 'grey',
                textShadowOffset: {width: 1, height: 1},
                textShadowRadius: 2,}}/>}
               />
               
              </View>
              </View>


              <View style={{marginHorizontal:15}}>
                <Text style={{color:'grey'}}>{item.created_at}</Text>
              </View>
  
            </View>
  
            <View style={{marginVertical:7}}>
              <Text style={{width:'98%'}}>{item.review} </Text>
            </View>
  
        </View>
        )
      }
    
    return(
        <SafeAreaView style={{flex:1,backgroundColor:'white'}}>
        <KeyboardAwareScrollView
        contentContainerStyle={{flexGrow:1}}
        >
        <View  style={styles.container}>
           <Card 
            containerStyle={styles.cardView}>
                <FontAwesome name="heart" onPress={()=> addWishlist()}
                 size={25} color={isWishlist ? "red" : 'grey'} style={{alignSelf:"flex-end"}} /> 
                <Image source={{uri:ApiUrl.image_base_url + details.item_image}} style={{width:200, height:200,alignSelf:'center'}} resizeMode="contain" />
            </Card>
            <View style={{flexDirection:'row',justifyContent:"space-between",alignItems:"center",paddingLeft:20,paddingRight:20,paddingTop:15,paddingBottom:5}}>
                <Text style={styles.textStyle}>{details.name}</Text>
                          <Text style={styles.textStyle}>DDK {!(quantity * details.price) ? 0 : (quantity * details.price)}</Text>
            </View>
            <Card
            containerStyle={{paddingLeft:20,paddingRight:20,paddingLeft:10,paddingBottom:10}}>
                <View style={{flexDirection:"row",justifyContent:'space-between',alignItems:'center'}}>
                    <QuantityComponent onChange={onQuantityChange}
                     isHorizontal ={true} ref={quantityComp}/>

                    <TouchableOpacity  onPress={()=>addCart()}
                    style={styles.addBtn}>
                        <Text style={styles.addBtnText}>Add</Text>
                    </TouchableOpacity>
                </View>
            </Card>
            <Text style={styles.textDescription}>Description</Text>
            <Text style={[styles.textDescription,{fontFamily:'roboto-regular',color:'#696969',marginTop:10,marginBottom:20}]}>{details.about}.</Text>


            <View style={{height:20}}/>
             <View style={{alignSelf:'flex-start',marginHorizontal:10,marginTop:-10}}>
                       <Rating
                        type='custom'
                        isDisabled={true}
                        ratingColor='green'
                        ratingBackgroundColor='lightgrey'
                        ratingCount={5}
                         readonly
                         tintColor='#fff'
                        startingValue={averageRating}
                        imageSize={25}
                        starContainerStyle={{ratingBackgroundColor:'red',borderRedius:20}}
                        style={{ratingBackgroundColor:'red',color:'white',borderRedius:20}}
                      /> 
                     
                        {/* <Stars
                        count={5}
                        display={averageRating}
                        disabled
                        half={true}
                        starSize={50}
                        fullStar={<Icon name={'star'} style={{color:'green'}}/>}
                        emptyStar={<Icon name={'star-outline'} style={{color:'grey'}}/>}
                        halfStar={<Icon name={'star-half'} style={{color:'grey'}}/>}
                        /> */}
                 
                </View>
            <View style={{margin:10}}>
            <Text style={{fontSize:14,}}>Ratings & Reviews Summary</Text>
            <View style={{flexDirection:'row',alignItems:'center'}}>
                <Text style={{fontSize:17,color:'green',fontWeight:'bold'}}>{parseFloat(averageRating).toFixed(1)}</Text>
                <Text style={{fontSize:16,color:'gray'}}> ({reviews.length} reviews)</Text>
            </View>
            </View>


            <FlatList 
            data={reviews}
            renderItem={({item}) => 
                renderItem(item)}
            />



        </View>
        </KeyboardAwareScrollView>
        { common.loading  && <ProgressBar/> }
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container:{
        flex:1,
       // backgroundColor:'white',
    },
    cardView:{
        padding:10,
        paddingLeft:15,
        paddingRight:15,
        paddingBottom:30,
        borderRadius:10,
        elevation:5,
        justifyContent:'space-between',
        width:Dimensions.get('window').width * 0.9,
    },
    textStyle:{
        fontFamily:'roboto-bold',
        fontSize:15,
        color:'black'
    },
    addBtn:{
        backgroundColor:'orange',
        padding:10,
        paddingLeft:40,
        paddingRight:40,
        borderRadius:10
    },
    addBtnText:{
        fontSize:15,
        color:'white',
        fontFamily:'roboto-bold',
    },
    quantityText:{
        fontFamily:'roboto-bold',
        fontSize:20,
        color:'black'
    },
    textDescription:{
        fontSize:14,
        fontFamily:"roboto-medium",
        marginTop:15,
        paddingLeft:20,

    }

})
export default DetailsPage;