import React ,{useEffect}from 'react';
import {ImageBackground,Image} from 'react-native'


const Splash = (props) => {

    return(
        <React.Fragment>
            <ImageBackground source={require('./assets/splash.png')}  style={{flex:1,alignItems:'center',justifyContent:'center'}} >
            <Image source={require('./assets/logo2.png')} 
            resizeMode='contain'
            style={{width:"70%",height:140,marginBottom:100,     }} />
            </ImageBackground>
        </React.Fragment>
    )

}

export default Splash;